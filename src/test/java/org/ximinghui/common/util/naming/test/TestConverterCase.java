/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util.naming.test;

import org.junit.jupiter.api.Test;
import org.ximinghui.common.util.exception.UnrecognizedNamingPatternException;
import org.ximinghui.common.util.naming.Convention;
import org.ximinghui.common.util.naming.Converter;

import static org.junit.jupiter.api.Assertions.*;

/**
 * 命名转换器测试用例
 */
class TestConverterCase extends Converter {

    /**
     * 命名转换方法测试
     */
    @Test
    void convertCase() {
        // 连字符命名转换测试
        hyphenConvertCase();
        // 下划线命名转换测试
        underscoreConvertCase();
        // 空格命名转换测试
        spaceConvertCase();
        // 单双单词命名转换测试
        singleAndDoubleWordsConversionCase();
        // 英文单词命名转换测试
        englishWordsConversionCase();
        // 数字和英文混合命名转换测试
        mixedDigitAndEnglishConversionCase();
        // 中英文混合命名转换测试
        mixedChineseAndEnglishConversionCase();
    }

    /**
     * 连字符命名转换测试
     */
    void hyphenConvertCase() {
        assertEquals("my-common-util", convert("my-common-util", Convention.KEBAB_CASE));
        assertEquals("MY-COMMON-UTIL", convert("my-common-util", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("myCommonUtil", convert("my-common-util", Convention.CAMEL_CASE));
        assertEquals("MyCommonUtil", convert("my-common-util", Convention.PASCAL_CASE));
        assertEquals("My-Common-Util", convert("my-common-util", Convention.HTTP_HEADER_CASE));
        assertEquals("my_common_util", convert("my-common-util", Convention.SNAKE_CASE));
        assertEquals("MY_COMMON_UTIL", convert("my-common-util", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("my_Common_Util", convert("my-common-util", Convention.CAMEL_SNAKE_CASE));
        assertEquals("My_Common_Util", convert("my-common-util", Convention.PASCAL_SNAKE_CASE));
        assertEquals("mycommonutil", convert("my-common-util", Convention.FLAT_CASE));
        assertEquals("MYCOMMONUTIL", convert("my-common-util", Convention.UPPER_FLAT_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("my-common-util", Convention.SENTENCE_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("my-common-util", Convention.TITLE_CASE));
        assertEquals("My Common Util", convert("my-common-util", Convention.START_CASE));
        assertEquals("my-common-util", convert("My-Common-Util", Convention.KEBAB_CASE));
        assertEquals("MY-COMMON-UTIL", convert("My-Common-Util", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("myCommonUtil", convert("My-Common-Util", Convention.CAMEL_CASE));
        assertEquals("MyCommonUtil", convert("My-Common-Util", Convention.PASCAL_CASE));
        assertEquals("My-Common-Util", convert("My-Common-Util", Convention.HTTP_HEADER_CASE));
        assertEquals("my_common_util", convert("My-Common-Util", Convention.SNAKE_CASE));
        assertEquals("MY_COMMON_UTIL", convert("My-Common-Util", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("my_Common_Util", convert("My-Common-Util", Convention.CAMEL_SNAKE_CASE));
        assertEquals("My_Common_Util", convert("My-Common-Util", Convention.PASCAL_SNAKE_CASE));
        assertEquals("mycommonutil", convert("My-Common-Util", Convention.FLAT_CASE));
        assertEquals("MYCOMMONUTIL", convert("My-Common-Util", Convention.UPPER_FLAT_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("My-Common-Util", Convention.SENTENCE_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("My-Common-Util", Convention.TITLE_CASE));
        assertEquals("My Common Util", convert("My-Common-Util", Convention.START_CASE));
        assertEquals("my-common-util", convert("MY-COMMON-UTIL", Convention.KEBAB_CASE));
        assertEquals("MY-COMMON-UTIL", convert("MY-COMMON-UTIL", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("myCommonUtil", convert("MY-COMMON-UTIL", Convention.CAMEL_CASE));
        assertEquals("MyCommonUtil", convert("MY-COMMON-UTIL", Convention.PASCAL_CASE));
        assertEquals("My-Common-Util", convert("MY-COMMON-UTIL", Convention.HTTP_HEADER_CASE));
        assertEquals("my_common_util", convert("MY-COMMON-UTIL", Convention.SNAKE_CASE));
        assertEquals("MY_COMMON_UTIL", convert("MY-COMMON-UTIL", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("my_Common_Util", convert("MY-COMMON-UTIL", Convention.CAMEL_SNAKE_CASE));
        assertEquals("My_Common_Util", convert("MY-COMMON-UTIL", Convention.PASCAL_SNAKE_CASE));
        assertEquals("mycommonutil", convert("MY-COMMON-UTIL", Convention.FLAT_CASE));
        assertEquals("MYCOMMONUTIL", convert("MY-COMMON-UTIL", Convention.UPPER_FLAT_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("MY-COMMON-UTIL", Convention.SENTENCE_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("MY-COMMON-UTIL", Convention.TITLE_CASE));
        assertEquals("My Common Util", convert("MY-COMMON-UTIL", Convention.START_CASE));
    }

    /**
     * 下划线命名转换测试
     */
    void underscoreConvertCase() {
        assertEquals("my-common-util", convert("my_common_util", Convention.KEBAB_CASE));
        assertEquals("MY-COMMON-UTIL", convert("my_common_util", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("myCommonUtil", convert("my_common_util", Convention.CAMEL_CASE));
        assertEquals("MyCommonUtil", convert("my_common_util", Convention.PASCAL_CASE));
        assertEquals("My-Common-Util", convert("my_common_util", Convention.HTTP_HEADER_CASE));
        assertEquals("my_common_util", convert("my_common_util", Convention.SNAKE_CASE));
        assertEquals("MY_COMMON_UTIL", convert("my_common_util", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("my_Common_Util", convert("my_common_util", Convention.CAMEL_SNAKE_CASE));
        assertEquals("My_Common_Util", convert("my_common_util", Convention.PASCAL_SNAKE_CASE));
        assertEquals("mycommonutil", convert("my_common_util", Convention.FLAT_CASE));
        assertEquals("MYCOMMONUTIL", convert("my_common_util", Convention.UPPER_FLAT_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("my_common_util", Convention.SENTENCE_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("my_common_util", Convention.TITLE_CASE));
        assertEquals("My Common Util", convert("my_common_util", Convention.START_CASE));
        assertEquals("my-common-util", convert("My_Common_Util", Convention.KEBAB_CASE));
        assertEquals("MY-COMMON-UTIL", convert("My_Common_Util", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("myCommonUtil", convert("My_Common_Util", Convention.CAMEL_CASE));
        assertEquals("MyCommonUtil", convert("My_Common_Util", Convention.PASCAL_CASE));
        assertEquals("My-Common-Util", convert("My_Common_Util", Convention.HTTP_HEADER_CASE));
        assertEquals("my_common_util", convert("My_Common_Util", Convention.SNAKE_CASE));
        assertEquals("MY_COMMON_UTIL", convert("My_Common_Util", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("my_Common_Util", convert("My_Common_Util", Convention.CAMEL_SNAKE_CASE));
        assertEquals("My_Common_Util", convert("My_Common_Util", Convention.PASCAL_SNAKE_CASE));
        assertEquals("mycommonutil", convert("My_Common_Util", Convention.FLAT_CASE));
        assertEquals("MYCOMMONUTIL", convert("My_Common_Util", Convention.UPPER_FLAT_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("My_Common_Util", Convention.SENTENCE_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("My_Common_Util", Convention.TITLE_CASE));
        assertEquals("My Common Util", convert("My_Common_Util", Convention.START_CASE));
        assertEquals("my-common-util", convert("MY_COMMON_UTIL", Convention.KEBAB_CASE));
        assertEquals("MY-COMMON-UTIL", convert("MY_COMMON_UTIL", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("myCommonUtil", convert("MY_COMMON_UTIL", Convention.CAMEL_CASE));
        assertEquals("MyCommonUtil", convert("MY_COMMON_UTIL", Convention.PASCAL_CASE));
        assertEquals("My-Common-Util", convert("MY_COMMON_UTIL", Convention.HTTP_HEADER_CASE));
        assertEquals("my_common_util", convert("MY_COMMON_UTIL", Convention.SNAKE_CASE));
        assertEquals("MY_COMMON_UTIL", convert("MY_COMMON_UTIL", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("my_Common_Util", convert("MY_COMMON_UTIL", Convention.CAMEL_SNAKE_CASE));
        assertEquals("My_Common_Util", convert("MY_COMMON_UTIL", Convention.PASCAL_SNAKE_CASE));
        assertEquals("mycommonutil", convert("MY_COMMON_UTIL", Convention.FLAT_CASE));
        assertEquals("MYCOMMONUTIL", convert("MY_COMMON_UTIL", Convention.UPPER_FLAT_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("MY_COMMON_UTIL", Convention.SENTENCE_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("MY_COMMON_UTIL", Convention.TITLE_CASE));
        assertEquals("My Common Util", convert("MY_COMMON_UTIL", Convention.START_CASE));
    }

    /**
     * 空格命名转换测试
     */
    void spaceConvertCase() {
        assertEquals("my-common-util", convert("my common util", Convention.KEBAB_CASE));
        assertEquals("MY-COMMON-UTIL", convert("my common util", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("myCommonUtil", convert("my common util", Convention.CAMEL_CASE));
        assertEquals("MyCommonUtil", convert("my common util", Convention.PASCAL_CASE));
        assertEquals("My-Common-Util", convert("my common util", Convention.HTTP_HEADER_CASE));
        assertEquals("my_common_util", convert("my common util", Convention.SNAKE_CASE));
        assertEquals("MY_COMMON_UTIL", convert("my common util", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("my_Common_Util", convert("my common util", Convention.CAMEL_SNAKE_CASE));
        assertEquals("My_Common_Util", convert("my common util", Convention.PASCAL_SNAKE_CASE));
        assertEquals("mycommonutil", convert("my common util", Convention.FLAT_CASE));
        assertEquals("MYCOMMONUTIL", convert("my common util", Convention.UPPER_FLAT_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("my common util", Convention.SENTENCE_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("my common util", Convention.TITLE_CASE));
        assertEquals("My Common Util", convert("my common util", Convention.START_CASE));
        assertEquals("my-common-util", convert("My Common Util", Convention.KEBAB_CASE));
        assertEquals("MY-COMMON-UTIL", convert("My Common Util", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("myCommonUtil", convert("My Common Util", Convention.CAMEL_CASE));
        assertEquals("MyCommonUtil", convert("My Common Util", Convention.PASCAL_CASE));
        assertEquals("My-Common-Util", convert("My Common Util", Convention.HTTP_HEADER_CASE));
        assertEquals("my_common_util", convert("My Common Util", Convention.SNAKE_CASE));
        assertEquals("MY_COMMON_UTIL", convert("My Common Util", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("my_Common_Util", convert("My Common Util", Convention.CAMEL_SNAKE_CASE));
        assertEquals("My_Common_Util", convert("My Common Util", Convention.PASCAL_SNAKE_CASE));
        assertEquals("mycommonutil", convert("My Common Util", Convention.FLAT_CASE));
        assertEquals("MYCOMMONUTIL", convert("My Common Util", Convention.UPPER_FLAT_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("My Common Util", Convention.SENTENCE_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("My Common Util", Convention.TITLE_CASE));
        assertEquals("My Common Util", convert("My Common Util", Convention.START_CASE));
        assertEquals("my-common-util", convert("MY COMMON UTIL", Convention.KEBAB_CASE));
        assertEquals("MY-COMMON-UTIL", convert("MY COMMON UTIL", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("myCommonUtil", convert("MY COMMON UTIL", Convention.CAMEL_CASE));
        assertEquals("MyCommonUtil", convert("MY COMMON UTIL", Convention.PASCAL_CASE));
        assertEquals("My-Common-Util", convert("MY COMMON UTIL", Convention.HTTP_HEADER_CASE));
        assertEquals("my_common_util", convert("MY COMMON UTIL", Convention.SNAKE_CASE));
        assertEquals("MY_COMMON_UTIL", convert("MY COMMON UTIL", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("my_Common_Util", convert("MY COMMON UTIL", Convention.CAMEL_SNAKE_CASE));
        assertEquals("My_Common_Util", convert("MY COMMON UTIL", Convention.PASCAL_SNAKE_CASE));
        assertEquals("mycommonutil", convert("MY COMMON UTIL", Convention.FLAT_CASE));
        assertEquals("MYCOMMONUTIL", convert("MY COMMON UTIL", Convention.UPPER_FLAT_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("MY COMMON UTIL", Convention.SENTENCE_CASE));
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("MY COMMON UTIL", Convention.TITLE_CASE));
        assertEquals("My Common Util", convert("MY COMMON UTIL", Convention.START_CASE));
    }

    /**
     * 单双单词命名转换测试
     */
    void singleAndDoubleWordsConversionCase() {
        // 单双单词命名转换START_CASE命名方法测试
        singleAndDoubleWordsConvertToStartCaseCase();
        // 单双单词命名转换KEBAB_CASE命名方法测试
        singleAndDoubleWordsConvertToKebabCaseCase();
        // 单双单词命名转换SCREAMING_KEBAB_CASE命名方法测试
        singleAndDoubleWordsConvertToScreamingKebabCaseCase();
        // 单双单词命名转换CAMEL_CASE命名方法测试
        singleAndDoubleWordsConvertToCamelCaseCase();
        // 单双单词命名转换PASCAL_CASE命名方法测试
        singleAndDoubleWordsConvertToPascalCaseCase();
        // 单双单词命名转换HTTP_HEADER_CASE命名方法测试
        singleAndDoubleWordsConvertToHttpHeaderCaseCase();
        // 单双单词命名转换SNAKE_CASE命名方法测试
        singleAndDoubleWordsConvertToSnakeCaseCase();
        // 单双单词命名转换SCREAMING_SNAKE_CASE命名方法测试
        singleAndDoubleWordsConvertToScreamingSnakeCaseCase();
        // 单双单词命名转换CAMEL_SNAKE_CASE命名方法测试
        singleAndDoubleWordsConvertToCamelSnakeCaseCase();
        // 单双单词命名转换PASCAL_SNAKE_CASE命名方法测试
        singleAndDoubleWordsConvertToPascalSnakeCaseCase();
        // 单双单词命名转换FLAT_CASE命名方法测试
        singleAndDoubleWordsConvertToFlatCaseCase();
        // 单双单词命名转换UPPER_FLAT_CASE命名方法测试
        singleAndDoubleWordsConvertToUpperFlatCaseCase();
        // 单双单词命名转换SENTENCE_CASE命名方法测试
        singleAndDoubleWordsConvertToSentenceCaseCase();
        // 单双单词命名转换TITLE_CASE命名方法测试
        singleAndDoubleWordsConvertToTitleCaseCase();
    }

    /**
     * 单双单词命名转换{@link Convention#START_CASE START_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToStartCaseCase() {
        assertEquals("A", convert("a", Convention.START_CASE));
        assertEquals("A", convert("A", Convention.START_CASE));
        assertEquals("Io", convert("io", Convention.START_CASE));
        assertEquals("Io", convert("Io", Convention.START_CASE));
        assertEquals("Io", convert("IO", Convention.START_CASE));
        assertEquals("Url", convert("url", Convention.START_CASE));
        assertEquals("Url", convert("Url", Convention.START_CASE));
        assertEquals("Url", convert("URL", Convention.START_CASE));
        assertEquals("Url Name", convert("urlName", Convention.START_CASE));
        assertEquals("Url Name", convert("UrlName", Convention.START_CASE));
        assertEquals("Url Name", convert("URLName", Convention.START_CASE));
        assertEquals("Url A", convert("urlA", Convention.START_CASE));
        assertEquals("Url A", convert("UrlA", Convention.START_CASE));
        assertEquals("Url2uri", convert("url2uri", Convention.START_CASE));
        assertEquals("Url2uri", convert("URL2URI", Convention.START_CASE));
        assertEquals("Log4j", convert("log4j", Convention.START_CASE));
        assertEquals("Log4j", convert("LOG4J", Convention.START_CASE));
        assertEquals("I18n", convert("i18n", Convention.START_CASE));
        assertEquals("I18ns", convert("i18ns", Convention.START_CASE));
        assertEquals("I18ns", convert("I18NS", Convention.START_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#KEBAB_CASE KEBAB_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToKebabCaseCase() {
        assertEquals("a", convert("a", Convention.KEBAB_CASE));
        assertEquals("a", convert("A", Convention.KEBAB_CASE));
        assertEquals("io", convert("io", Convention.KEBAB_CASE));
        assertEquals("io", convert("Io", Convention.KEBAB_CASE));
        assertEquals("io", convert("IO", Convention.KEBAB_CASE));
        assertEquals("url", convert("url", Convention.KEBAB_CASE));
        assertEquals("url", convert("Url", Convention.KEBAB_CASE));
        assertEquals("url", convert("URL", Convention.KEBAB_CASE));
        assertEquals("url-name", convert("urlName", Convention.KEBAB_CASE));
        assertEquals("url-name", convert("UrlName", Convention.KEBAB_CASE));
        assertEquals("url-name", convert("URLName", Convention.KEBAB_CASE));
        assertEquals("url-a", convert("urlA", Convention.KEBAB_CASE));
        assertEquals("url-a", convert("UrlA", Convention.KEBAB_CASE));
        assertEquals("url2uri", convert("url2uri", Convention.KEBAB_CASE));
        assertEquals("url2uri", convert("URL2URI", Convention.KEBAB_CASE));
        assertEquals("log4j", convert("log4j", Convention.KEBAB_CASE));
        assertEquals("log4j", convert("LOG4J", Convention.KEBAB_CASE));
        assertEquals("i18n", convert("i18n", Convention.KEBAB_CASE));
        assertEquals("i18ns", convert("i18ns", Convention.KEBAB_CASE));
        assertEquals("i18ns", convert("I18NS", Convention.KEBAB_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#SCREAMING_KEBAB_CASE SCREAMING_KEBAB_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToScreamingKebabCaseCase() {
        assertEquals("A", convert("a", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("A", convert("A", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("IO", convert("io", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("IO", convert("Io", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("IO", convert("IO", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL", convert("url", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL", convert("Url", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL", convert("URL", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-NAME", convert("urlName", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-NAME", convert("UrlName", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-NAME", convert("URLName", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-A", convert("urlA", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-A", convert("UrlA", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL2URI", convert("url2uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL2URI", convert("URL2URI", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("LOG4J", convert("log4j", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("LOG4J", convert("LOG4J", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("I18N", convert("i18n", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("I18NS", convert("i18ns", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("I18NS", convert("I18NS", Convention.SCREAMING_KEBAB_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#CAMEL_CASE CAMEL_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToCamelCaseCase() {
        assertEquals("a", convert("a", Convention.CAMEL_CASE));
        assertEquals("a", convert("A", Convention.CAMEL_CASE));
        assertEquals("io", convert("io", Convention.CAMEL_CASE));
        assertEquals("io", convert("Io", Convention.CAMEL_CASE));
        assertEquals("io", convert("IO", Convention.CAMEL_CASE));
        assertEquals("url", convert("url", Convention.CAMEL_CASE));
        assertEquals("url", convert("Url", Convention.CAMEL_CASE));
        assertEquals("url", convert("URL", Convention.CAMEL_CASE));
        assertEquals("urlName", convert("urlName", Convention.CAMEL_CASE));
        assertEquals("urlName", convert("UrlName", Convention.CAMEL_CASE));
        assertEquals("urlName", convert("URLName", Convention.CAMEL_CASE));
        assertEquals("urlA", convert("urlA", Convention.CAMEL_CASE));
        assertEquals("urlA", convert("UrlA", Convention.CAMEL_CASE));
        assertEquals("url2uri", convert("url2uri", Convention.CAMEL_CASE));
        assertEquals("url2uri", convert("URL2URI", Convention.CAMEL_CASE));
        assertEquals("log4j", convert("log4j", Convention.CAMEL_CASE));
        assertEquals("log4j", convert("LOG4J", Convention.CAMEL_CASE));
        assertEquals("i18n", convert("i18n", Convention.CAMEL_CASE));
        assertEquals("i18ns", convert("i18ns", Convention.CAMEL_CASE));
        assertEquals("i18ns", convert("I18NS", Convention.CAMEL_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#PASCAL_CASE PASCAL_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToPascalCaseCase() {
        assertEquals("A", convert("a", Convention.PASCAL_CASE));
        assertEquals("A", convert("A", Convention.PASCAL_CASE));
        assertEquals("Io", convert("io", Convention.PASCAL_CASE));
        assertEquals("Io", convert("Io", Convention.PASCAL_CASE));
        assertEquals("Io", convert("IO", Convention.PASCAL_CASE));
        assertEquals("Url", convert("url", Convention.PASCAL_CASE));
        assertEquals("Url", convert("Url", Convention.PASCAL_CASE));
        assertEquals("Url", convert("URL", Convention.PASCAL_CASE));
        assertEquals("UrlName", convert("urlName", Convention.PASCAL_CASE));
        assertEquals("UrlName", convert("UrlName", Convention.PASCAL_CASE));
        assertEquals("UrlName", convert("URLName", Convention.PASCAL_CASE));
        assertEquals("UrlA", convert("urlA", Convention.PASCAL_CASE));
        assertEquals("UrlA", convert("UrlA", Convention.PASCAL_CASE));
        assertEquals("Url2uri", convert("url2uri", Convention.PASCAL_CASE));
        assertEquals("Url2uri", convert("URL2URI", Convention.PASCAL_CASE));
        assertEquals("Log4j", convert("log4j", Convention.PASCAL_CASE));
        assertEquals("Log4j", convert("LOG4J", Convention.PASCAL_CASE));
        assertEquals("I18n", convert("i18n", Convention.PASCAL_CASE));
        assertEquals("I18ns", convert("i18ns", Convention.PASCAL_CASE));
        assertEquals("I18ns", convert("I18NS", Convention.PASCAL_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#HTTP_HEADER_CASE HTTP_HEADER_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToHttpHeaderCaseCase() {
        assertEquals("A", convert("a", Convention.HTTP_HEADER_CASE));
        assertEquals("A", convert("A", Convention.HTTP_HEADER_CASE));
        assertEquals("Io", convert("io", Convention.HTTP_HEADER_CASE));
        assertEquals("Io", convert("Io", Convention.HTTP_HEADER_CASE));
        assertEquals("Io", convert("IO", Convention.HTTP_HEADER_CASE));
        assertEquals("Url", convert("url", Convention.HTTP_HEADER_CASE));
        assertEquals("Url", convert("Url", Convention.HTTP_HEADER_CASE));
        assertEquals("Url", convert("URL", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-Name", convert("urlName", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-Name", convert("UrlName", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-Name", convert("URLName", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-A", convert("urlA", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-A", convert("UrlA", Convention.HTTP_HEADER_CASE));
        assertEquals("Url2uri", convert("url2uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url2uri", convert("URL2URI", Convention.HTTP_HEADER_CASE));
        assertEquals("Log4j", convert("log4j", Convention.HTTP_HEADER_CASE));
        assertEquals("Log4j", convert("LOG4J", Convention.HTTP_HEADER_CASE));
        assertEquals("I18n", convert("i18n", Convention.HTTP_HEADER_CASE));
        assertEquals("I18ns", convert("i18ns", Convention.HTTP_HEADER_CASE));
        assertEquals("I18ns", convert("I18NS", Convention.HTTP_HEADER_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#SNAKE_CASE SNAKE_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToSnakeCaseCase() {
        assertEquals("a", convert("a", Convention.SNAKE_CASE));
        assertEquals("a", convert("A", Convention.SNAKE_CASE));
        assertEquals("io", convert("io", Convention.SNAKE_CASE));
        assertEquals("io", convert("Io", Convention.SNAKE_CASE));
        assertEquals("io", convert("IO", Convention.SNAKE_CASE));
        assertEquals("url", convert("url", Convention.SNAKE_CASE));
        assertEquals("url", convert("Url", Convention.SNAKE_CASE));
        assertEquals("url", convert("URL", Convention.SNAKE_CASE));
        assertEquals("url_name", convert("urlName", Convention.SNAKE_CASE));
        assertEquals("url_name", convert("UrlName", Convention.SNAKE_CASE));
        assertEquals("url_name", convert("URLName", Convention.SNAKE_CASE));
        assertEquals("url_a", convert("urlA", Convention.SNAKE_CASE));
        assertEquals("url_a", convert("UrlA", Convention.SNAKE_CASE));
        assertEquals("url2uri", convert("url2uri", Convention.SNAKE_CASE));
        assertEquals("url2uri", convert("URL2URI", Convention.SNAKE_CASE));
        assertEquals("log4j", convert("log4j", Convention.SNAKE_CASE));
        assertEquals("log4j", convert("LOG4J", Convention.SNAKE_CASE));
        assertEquals("i18n", convert("i18n", Convention.SNAKE_CASE));
        assertEquals("i18ns", convert("i18ns", Convention.SNAKE_CASE));
        assertEquals("i18ns", convert("I18NS", Convention.SNAKE_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#SCREAMING_SNAKE_CASE SCREAMING_SNAKE_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToScreamingSnakeCaseCase() {
        assertEquals("A", convert("a", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("A", convert("A", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("IO", convert("io", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("IO", convert("Io", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("IO", convert("IO", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL", convert("url", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL", convert("Url", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL", convert("URL", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_NAME", convert("urlName", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_NAME", convert("UrlName", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_NAME", convert("URLName", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_A", convert("urlA", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_A", convert("UrlA", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL2URI", convert("url2uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL2URI", convert("URL2URI", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("LOG4J", convert("log4j", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("LOG4J", convert("LOG4J", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("I18N", convert("i18n", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("I18NS", convert("i18ns", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("I18NS", convert("I18NS", Convention.SCREAMING_SNAKE_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#CAMEL_SNAKE_CASE CAMEL_SNAKE_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToCamelSnakeCaseCase() {
        assertEquals("a", convert("a", Convention.CAMEL_SNAKE_CASE));
        assertEquals("a", convert("A", Convention.CAMEL_SNAKE_CASE));
        assertEquals("io", convert("io", Convention.CAMEL_SNAKE_CASE));
        assertEquals("io", convert("Io", Convention.CAMEL_SNAKE_CASE));
        assertEquals("io", convert("IO", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url", convert("url", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url", convert("Url", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url", convert("URL", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_Name", convert("urlName", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_Name", convert("UrlName", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_Name", convert("URLName", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_A", convert("urlA", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_A", convert("UrlA", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url2uri", convert("url2uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url2uri", convert("URL2URI", Convention.CAMEL_SNAKE_CASE));
        assertEquals("log4j", convert("log4j", Convention.CAMEL_SNAKE_CASE));
        assertEquals("log4j", convert("LOG4J", Convention.CAMEL_SNAKE_CASE));
        assertEquals("i18n", convert("i18n", Convention.CAMEL_SNAKE_CASE));
        assertEquals("i18ns", convert("i18ns", Convention.CAMEL_SNAKE_CASE));
        assertEquals("i18ns", convert("I18NS", Convention.CAMEL_SNAKE_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#PASCAL_SNAKE_CASE PASCAL_SNAKE_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToPascalSnakeCaseCase() {
        assertEquals("A", convert("a", Convention.PASCAL_SNAKE_CASE));
        assertEquals("A", convert("A", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Io", convert("io", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Io", convert("Io", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Io", convert("IO", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url", convert("url", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url", convert("Url", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url", convert("URL", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_Name", convert("urlName", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_Name", convert("UrlName", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_Name", convert("URLName", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_A", convert("urlA", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_A", convert("UrlA", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url2uri", convert("url2uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url2uri", convert("URL2URI", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Log4j", convert("log4j", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Log4j", convert("LOG4J", Convention.PASCAL_SNAKE_CASE));
        assertEquals("I18n", convert("i18n", Convention.PASCAL_SNAKE_CASE));
        assertEquals("I18ns", convert("i18ns", Convention.PASCAL_SNAKE_CASE));
        assertEquals("I18ns", convert("I18NS", Convention.PASCAL_SNAKE_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#FLAT_CASE FLAT_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToFlatCaseCase() {
        assertEquals("a", convert("a", Convention.FLAT_CASE));
        assertEquals("a", convert("A", Convention.FLAT_CASE));
        assertEquals("io", convert("io", Convention.FLAT_CASE));
        assertEquals("io", convert("Io", Convention.FLAT_CASE));
        assertEquals("io", convert("IO", Convention.FLAT_CASE));
        assertEquals("url", convert("url", Convention.FLAT_CASE));
        assertEquals("url", convert("Url", Convention.FLAT_CASE));
        assertEquals("url", convert("URL", Convention.FLAT_CASE));
        assertEquals("urlname", convert("urlName", Convention.FLAT_CASE));
        assertEquals("urlname", convert("UrlName", Convention.FLAT_CASE));
        assertEquals("urlname", convert("URLName", Convention.FLAT_CASE));
        assertEquals("urla", convert("urlA", Convention.FLAT_CASE));
        assertEquals("urla", convert("UrlA", Convention.FLAT_CASE));
        assertEquals("url2uri", convert("url2uri", Convention.FLAT_CASE));
        assertEquals("url2uri", convert("URL2URI", Convention.FLAT_CASE));
        assertEquals("log4j", convert("log4j", Convention.FLAT_CASE));
        assertEquals("log4j", convert("LOG4J", Convention.FLAT_CASE));
        assertEquals("i18n", convert("i18n", Convention.FLAT_CASE));
        assertEquals("i18ns", convert("i18ns", Convention.FLAT_CASE));
        assertEquals("i18ns", convert("I18NS", Convention.FLAT_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#UPPER_FLAT_CASE UPPER_FLAT_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToUpperFlatCaseCase() {
        assertEquals("A", convert("a", Convention.UPPER_FLAT_CASE));
        assertEquals("A", convert("A", Convention.UPPER_FLAT_CASE));
        assertEquals("IO", convert("io", Convention.UPPER_FLAT_CASE));
        assertEquals("IO", convert("Io", Convention.UPPER_FLAT_CASE));
        assertEquals("IO", convert("IO", Convention.UPPER_FLAT_CASE));
        assertEquals("URL", convert("url", Convention.UPPER_FLAT_CASE));
        assertEquals("URL", convert("Url", Convention.UPPER_FLAT_CASE));
        assertEquals("URL", convert("URL", Convention.UPPER_FLAT_CASE));
        assertEquals("URLNAME", convert("urlName", Convention.UPPER_FLAT_CASE));
        assertEquals("URLNAME", convert("UrlName", Convention.UPPER_FLAT_CASE));
        assertEquals("URLNAME", convert("URLName", Convention.UPPER_FLAT_CASE));
        assertEquals("URLA", convert("urlA", Convention.UPPER_FLAT_CASE));
        assertEquals("URLA", convert("UrlA", Convention.UPPER_FLAT_CASE));
        assertEquals("URL2URI", convert("url2uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL2URI", convert("URL2URI", Convention.UPPER_FLAT_CASE));
        assertEquals("LOG4J", convert("log4j", Convention.UPPER_FLAT_CASE));
        assertEquals("LOG4J", convert("LOG4J", Convention.UPPER_FLAT_CASE));
        assertEquals("I18N", convert("i18n", Convention.UPPER_FLAT_CASE));
        assertEquals("I18NS", convert("i18ns", Convention.UPPER_FLAT_CASE));
        assertEquals("I18NS", convert("I18NS", Convention.UPPER_FLAT_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#SENTENCE_CASE SENTENCE_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToSentenceCaseCase() {
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("暂未实现", Convention.SENTENCE_CASE));
    }

    /**
     * 单双单词命名转换{@link Convention#TITLE_CASE TITLE_CASE}命名方法测试
     */
    void singleAndDoubleWordsConvertToTitleCaseCase() {
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("暂未实现", Convention.TITLE_CASE));
    }

    /**
     * 英文单词命名转换测试
     */
    void englishWordsConversionCase() {
        // 英文单词命名转换START_CASE命名方法测试
        englishWordsConvertToStartCaseCase();
        // 英文单词命名转换KEBAB_CASE命名方法测试
        englishWordsConvertToKebabCaseCase();
        // 英文单词命名转换SCREAMING_KEBAB_CASE命名方法测试
        englishWordsConvertToScreamingKebabCaseCase();
        // 英文单词命名转换CAMEL_CASE命名方法测试
        englishWordsConvertToCamelCaseCase();
        // 英文单词命名转换PASCAL_CASE命名方法测试
        englishWordsConvertToPascalCaseCase();
        // 英文单词命名转换HTTP_HEADER_CASE命名方法测试
        englishWordsConvertToHttpHeaderCaseCase();
        // 英文单词命名转换SNAKE_CASE命名方法测试
        englishWordsConvertToSnakeCaseCase();
        // 英文单词命名转换SCREAMING_SNAKE_CASE命名方法测试
        englishWordsConvertToScreamingSnakeCaseCase();
        // 英文单词命名转换CAMEL_SNAKE_CASE命名方法测试
        englishWordsConvertToCamelSnakeCaseCase();
        // 英文单词命名转换PASCAL_SNAKE_CASE命名方法测试
        englishWordsConvertToPascalSnakeCaseCase();
        // 英文单词命名转换FLAT_CASE命名方法测试
        englishWordsConvertToFlatCaseCase();
        // 英文单词命名转换UPPER_FLAT_CASE命名方法测试
        englishWordsConvertToUpperFlatCaseCase();
        // 英文单词命名转换SENTENCE_CASE命名方法测试
        englishWordsConvertToSentenceCaseCase();
        // 英文单词命名转换TITLE_CASE命名方法测试
        englishWordsConvertToTitleCaseCase();
    }

    /**
     * 英文单词命名转换{@link Convention#START_CASE START_CASE}命名方法测试
     */
    void englishWordsConvertToStartCaseCase() {
        assertEquals("A Url Name", convert("aUrlName", Convention.START_CASE));
        assertEquals("A Url Name", convert("aURLName", Convention.START_CASE));
        assertEquals("A Url Name Description", convert("aUrlNameDescription", Convention.START_CASE));
        assertEquals("A Url Name Description", convert("aURLNameDescription", Convention.START_CASE));
        assertEquals("This Is A Url Name Description", convert("thisIsAUrlNameDescription", Convention.START_CASE));
        assertEquals("This Is Aurl Name Description", convert("thisIsAURLNameDescription", Convention.START_CASE));
        assertEquals("A Url A", convert("aUrlA", Convention.START_CASE));
        assertEquals("A Url B", convert("aUrlB", Convention.START_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#KEBAB_CASE KEBAB_CASE}命名方法测试
     */
    void englishWordsConvertToKebabCaseCase() {
        assertEquals("a-url-name", convert("aUrlName", Convention.KEBAB_CASE));
        assertEquals("a-url-name", convert("aURLName", Convention.KEBAB_CASE));
        assertEquals("a-url-name-description", convert("aUrlNameDescription", Convention.KEBAB_CASE));
        assertEquals("a-url-name-description", convert("aURLNameDescription", Convention.KEBAB_CASE));
        assertEquals("this-is-a-url-name-description", convert("thisIsAUrlNameDescription", Convention.KEBAB_CASE));
        assertEquals("this-is-aurl-name-description", convert("thisIsAURLNameDescription", Convention.KEBAB_CASE));
        assertEquals("a-url-a", convert("aUrlA", Convention.KEBAB_CASE));
        assertEquals("a-url-b", convert("aUrlB", Convention.KEBAB_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#SCREAMING_KEBAB_CASE SCREAMING_KEBAB_CASE}命名方法测试
     */
    void englishWordsConvertToScreamingKebabCaseCase() {
        assertEquals("A-URL-NAME", convert("aUrlName", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("A-URL-NAME", convert("aURLName", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("A-URL-NAME-DESCRIPTION", convert("aUrlNameDescription", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("A-URL-NAME-DESCRIPTION", convert("aURLNameDescription", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("THIS-IS-A-URL-NAME-DESCRIPTION", convert("thisIsAUrlNameDescription", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("THIS-IS-AURL-NAME-DESCRIPTION", convert("thisIsAURLNameDescription", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("A-URL-A", convert("aUrlA", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("A-URL-B", convert("aUrlB", Convention.SCREAMING_KEBAB_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#CAMEL_CASE CAMEL_CASE}命名方法测试
     */
    void englishWordsConvertToCamelCaseCase() {
        assertEquals("aUrlName", convert("aUrlName", Convention.CAMEL_CASE));
        assertEquals("aUrlName", convert("aURLName", Convention.CAMEL_CASE));
        assertEquals("aUrlNameDescription", convert("aUrlNameDescription", Convention.CAMEL_CASE));
        assertEquals("aUrlNameDescription", convert("aURLNameDescription", Convention.CAMEL_CASE));
        assertEquals("thisIsAUrlNameDescription", convert("thisIsAUrlNameDescription", Convention.CAMEL_CASE));
        assertEquals("thisIsAurlNameDescription", convert("thisIsAURLNameDescription", Convention.CAMEL_CASE));
        assertEquals("aUrlA", convert("aUrlA", Convention.CAMEL_CASE));
        assertEquals("aUrlB", convert("aUrlB", Convention.CAMEL_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#PASCAL_CASE PASCAL_CASE}命名方法测试
     */
    void englishWordsConvertToPascalCaseCase() {
        assertEquals("AUrlName", convert("aUrlName", Convention.PASCAL_CASE));
        assertEquals("AUrlName", convert("aURLName", Convention.PASCAL_CASE));
        assertEquals("AUrlNameDescription", convert("aUrlNameDescription", Convention.PASCAL_CASE));
        assertEquals("AUrlNameDescription", convert("aURLNameDescription", Convention.PASCAL_CASE));
        assertEquals("ThisIsAUrlNameDescription", convert("thisIsAUrlNameDescription", Convention.PASCAL_CASE));
        assertEquals("ThisIsAurlNameDescription", convert("thisIsAURLNameDescription", Convention.PASCAL_CASE));
        assertEquals("AUrlA", convert("aUrlA", Convention.PASCAL_CASE));
        assertEquals("AUrlB", convert("aUrlB", Convention.PASCAL_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#HTTP_HEADER_CASE HTTP_HEADER_CASE}命名方法测试
     */
    void englishWordsConvertToHttpHeaderCaseCase() {
        assertEquals("A-Url-Name", convert("aUrlName", Convention.HTTP_HEADER_CASE));
        assertEquals("A-Url-Name", convert("aURLName", Convention.HTTP_HEADER_CASE));
        assertEquals("A-Url-Name-Description", convert("aUrlNameDescription", Convention.HTTP_HEADER_CASE));
        assertEquals("A-Url-Name-Description", convert("aURLNameDescription", Convention.HTTP_HEADER_CASE));
        assertEquals("This-Is-A-Url-Name-Description", convert("thisIsAUrlNameDescription", Convention.HTTP_HEADER_CASE));
        assertEquals("This-Is-Aurl-Name-Description", convert("thisIsAURLNameDescription", Convention.HTTP_HEADER_CASE));
        assertEquals("A-Url-A", convert("aUrlA", Convention.HTTP_HEADER_CASE));
        assertEquals("A-Url-B", convert("aUrlB", Convention.HTTP_HEADER_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#SNAKE_CASE SNAKE_CASE}命名方法测试
     */
    void englishWordsConvertToSnakeCaseCase() {
        assertEquals("a_url_name", convert("aUrlName", Convention.SNAKE_CASE));
        assertEquals("a_url_name", convert("aURLName", Convention.SNAKE_CASE));
        assertEquals("a_url_name_description", convert("aUrlNameDescription", Convention.SNAKE_CASE));
        assertEquals("a_url_name_description", convert("aURLNameDescription", Convention.SNAKE_CASE));
        assertEquals("this_is_a_url_name_description", convert("thisIsAUrlNameDescription", Convention.SNAKE_CASE));
        assertEquals("this_is_aurl_name_description", convert("thisIsAURLNameDescription", Convention.SNAKE_CASE));
        assertEquals("a_url_a", convert("aUrlA", Convention.SNAKE_CASE));
        assertEquals("a_url_b", convert("aUrlB", Convention.SNAKE_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#SCREAMING_SNAKE_CASE SCREAMING_SNAKE_CASE}命名方法测试
     */
    void englishWordsConvertToScreamingSnakeCaseCase() {
        assertEquals("A_URL_NAME", convert("aUrlName", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("A_URL_NAME", convert("aURLName", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("A_URL_NAME_DESCRIPTION", convert("aUrlNameDescription", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("A_URL_NAME_DESCRIPTION", convert("aURLNameDescription", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("THIS_IS_A_URL_NAME_DESCRIPTION", convert("thisIsAUrlNameDescription", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("THIS_IS_AURL_NAME_DESCRIPTION", convert("thisIsAURLNameDescription", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("A_URL_A", convert("aUrlA", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("A_URL_B", convert("aUrlB", Convention.SCREAMING_SNAKE_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#CAMEL_SNAKE_CASE CAMEL_SNAKE_CASE}命名方法测试
     */
    void englishWordsConvertToCamelSnakeCaseCase() {
        assertEquals("a_Url_Name", convert("aUrlName", Convention.CAMEL_SNAKE_CASE));
        assertEquals("a_Url_Name", convert("aURLName", Convention.CAMEL_SNAKE_CASE));
        assertEquals("a_Url_Name_Description", convert("aUrlNameDescription", Convention.CAMEL_SNAKE_CASE));
        assertEquals("a_Url_Name_Description", convert("aURLNameDescription", Convention.CAMEL_SNAKE_CASE));
        assertEquals("this_Is_A_Url_Name_Description", convert("thisIsAUrlNameDescription", Convention.CAMEL_SNAKE_CASE));
        assertEquals("this_Is_Aurl_Name_Description", convert("thisIsAURLNameDescription", Convention.CAMEL_SNAKE_CASE));
        assertEquals("a_Url_A", convert("aUrlA", Convention.CAMEL_SNAKE_CASE));
        assertEquals("a_Url_B", convert("aUrlB", Convention.CAMEL_SNAKE_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#PASCAL_SNAKE_CASE PASCAL_SNAKE_CASE}命名方法测试
     */
    void englishWordsConvertToPascalSnakeCaseCase() {
        assertEquals("A_Url_Name", convert("aUrlName", Convention.PASCAL_SNAKE_CASE));
        assertEquals("A_Url_Name", convert("aURLName", Convention.PASCAL_SNAKE_CASE));
        assertEquals("A_Url_Name_Description", convert("aUrlNameDescription", Convention.PASCAL_SNAKE_CASE));
        assertEquals("A_Url_Name_Description", convert("aURLNameDescription", Convention.PASCAL_SNAKE_CASE));
        assertEquals("This_Is_A_Url_Name_Description", convert("thisIsAUrlNameDescription", Convention.PASCAL_SNAKE_CASE));
        assertEquals("This_Is_Aurl_Name_Description", convert("thisIsAURLNameDescription", Convention.PASCAL_SNAKE_CASE));
        assertEquals("A_Url_A", convert("aUrlA", Convention.PASCAL_SNAKE_CASE));
        assertEquals("A_Url_B", convert("aUrlB", Convention.PASCAL_SNAKE_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#FLAT_CASE FLAT_CASE}命名方法测试
     */
    void englishWordsConvertToFlatCaseCase() {
        assertEquals("aurlname", convert("aUrlName", Convention.FLAT_CASE));
        assertEquals("aurlname", convert("aURLName", Convention.FLAT_CASE));
        assertEquals("aurlnamedescription", convert("aUrlNameDescription", Convention.FLAT_CASE));
        assertEquals("aurlnamedescription", convert("aURLNameDescription", Convention.FLAT_CASE));
        assertEquals("thisisaurlnamedescription", convert("thisIsAUrlNameDescription", Convention.FLAT_CASE));
        assertEquals("thisisaurlnamedescription", convert("thisIsAURLNameDescription", Convention.FLAT_CASE));
        assertEquals("aurla", convert("aUrlA", Convention.FLAT_CASE));
        assertEquals("aurlb", convert("aUrlB", Convention.FLAT_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#UPPER_FLAT_CASE UPPER_FLAT_CASE}命名方法测试
     */
    void englishWordsConvertToUpperFlatCaseCase() {
        assertEquals("AURLNAME", convert("aUrlName", Convention.UPPER_FLAT_CASE));
        assertEquals("AURLNAME", convert("aURLName", Convention.UPPER_FLAT_CASE));
        assertEquals("AURLNAMEDESCRIPTION", convert("aUrlNameDescription", Convention.UPPER_FLAT_CASE));
        assertEquals("AURLNAMEDESCRIPTION", convert("aURLNameDescription", Convention.UPPER_FLAT_CASE));
        assertEquals("THISISAURLNAMEDESCRIPTION", convert("thisIsAUrlNameDescription", Convention.UPPER_FLAT_CASE));
        assertEquals("THISISAURLNAMEDESCRIPTION", convert("thisIsAURLNameDescription", Convention.UPPER_FLAT_CASE));
        assertEquals("AURLA", convert("aUrlA", Convention.UPPER_FLAT_CASE));
        assertEquals("AURLB", convert("aUrlB", Convention.UPPER_FLAT_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#SENTENCE_CASE SENTENCE_CASE}命名方法测试
     */
    void englishWordsConvertToSentenceCaseCase() {
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("暂未实现", Convention.SENTENCE_CASE));
    }

    /**
     * 英文单词命名转换{@link Convention#TITLE_CASE TITLE_CASE}命名方法测试
     */
    void englishWordsConvertToTitleCaseCase() {
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("暂未实现", Convention.TITLE_CASE));
    }

    /**
     * 数字和英文混合命名转换测试
     */
    void mixedDigitAndEnglishConversionCase() {
        // 数字和英文混合命名转换START_CASE命名方法测试
        mixedDigitAndEnglishConvertToStartCaseCase();
        // 数字和英文混合命名转换KEBAB_CASE命名方法测试
        mixedDigitAndEnglishConvertToKebabCaseCase();
        // 数字和英文混合命名转换SCREAMING_KEBAB_CASE命名方法测试
        mixedDigitAndEnglishConvertToScreamingKebabCaseCase();
        // 数字和英文混合命名转换CAMEL_CASE命名方法测试
        mixedDigitAndEnglishConvertToCamelCaseCase();
        // 数字和英文混合命名转换PASCAL_CASE命名方法测试
        mixedDigitAndEnglishConvertToPascalCaseCase();
        // 数字和英文混合命名转换HTTP_HEADER_CASE命名方法测试
        mixedDigitAndEnglishConvertToHttpHeaderCaseCase();
        // 数字和英文混合命名转换SNAKE_CASE命名方法测试
        mixedDigitAndEnglishConvertToSnakeCaseCase();
        // 数字和英文混合命名转换SCREAMING_SNAKE_CASE命名方法测试
        mixedDigitAndEnglishConvertToScreamingSnakeCaseCase();
        // 数字和英文混合命名转换CAMEL_SNAKE_CASE命名方法测试
        mixedDigitAndEnglishConvertToCamelSnakeCaseCase();
        // 数字和英文混合命名转换PASCAL_SNAKE_CASE命名方法测试
        mixedDigitAndEnglishConvertToPascalSnakeCaseCase();
        // 数字和英文混合命名转换FLAT_CASE命名方法测试
        mixedDigitAndEnglishConvertToFlatCaseCase();
        // 数字和英文混合命名转换UPPER_FLAT_CASE命名方法测试
        mixedDigitAndEnglishConvertToUpperFlatCaseCase();
        // 数字和英文混合命名转换SENTENCE_CASE命名方法测试
        mixedDigitAndEnglishConvertToSentenceCaseCase();
        // 数字和英文混合命名转换TITLE_CASE命名方法测试
        mixedDigitAndEnglishConvertToTitleCaseCase();
    }

    /**
     * 数字和英文混合命名转换{@link Convention#START_CASE START_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToStartCaseCase() {
        assertEquals("Url 2 Uri", convert("url2Uri", Convention.START_CASE));
        assertEquals("Url 2 Url", convert("url2URL", Convention.START_CASE));
        assertEquals("Url 2 Uri", convert("Url2uri", Convention.START_CASE));
        assertEquals("Url 2 Uri", convert("Url2Uri", Convention.START_CASE));
        assertEquals("Url 2 Uri", convert("Url2URI", Convention.START_CASE));
        assertEquals("Url 2 Uri", convert("URL2uri", Convention.START_CASE));
        assertEquals("Url 2 Uri", convert("URL2Uri", Convention.START_CASE));
        assertEquals("I 18 N", convert("I18n", Convention.START_CASE));
        assertEquals("I 18 N", convert("i18N", Convention.START_CASE));
        assertEquals("I 18 Ns", convert("i18Ns", Convention.START_CASE));
        assertEquals("I 18 Ns", convert("i18NS", Convention.START_CASE));
        assertEquals("I 18 Ns", convert("I18ns", Convention.START_CASE));
        assertEquals("I 18 Ns", convert("I18Ns", Convention.START_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#KEBAB_CASE KEBAB_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToKebabCaseCase() {
        assertEquals("url-2-uri", convert("url2Uri", Convention.KEBAB_CASE));
        assertEquals("url-2-url", convert("url2URL", Convention.KEBAB_CASE));
        assertEquals("url-2-uri", convert("Url2uri", Convention.KEBAB_CASE));
        assertEquals("url-2-uri", convert("Url2Uri", Convention.KEBAB_CASE));
        assertEquals("url-2-uri", convert("Url2URI", Convention.KEBAB_CASE));
        assertEquals("url-2-uri", convert("URL2uri", Convention.KEBAB_CASE));
        assertEquals("url-2-uri", convert("URL2Uri", Convention.KEBAB_CASE));
        assertEquals("i-18-n", convert("I18n", Convention.KEBAB_CASE));
        assertEquals("i-18-n", convert("i18N", Convention.KEBAB_CASE));
        assertEquals("i-18-ns", convert("i18Ns", Convention.KEBAB_CASE));
        assertEquals("i-18-ns", convert("i18NS", Convention.KEBAB_CASE));
        assertEquals("i-18-ns", convert("I18ns", Convention.KEBAB_CASE));
        assertEquals("i-18-ns", convert("I18Ns", Convention.KEBAB_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#SCREAMING_KEBAB_CASE SCREAMING_KEBAB_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToScreamingKebabCaseCase() {
        assertEquals("URL-2-URI", convert("url2Uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-2-URL", convert("url2URL", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-2-URI", convert("Url2uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-2-URI", convert("Url2Uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-2-URI", convert("Url2URI", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-2-URI", convert("URL2uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-2-URI", convert("URL2Uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("I-18-N", convert("I18n", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("I-18-N", convert("i18N", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("I-18-NS", convert("i18Ns", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("I-18-NS", convert("i18NS", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("I-18-NS", convert("I18ns", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("I-18-NS", convert("I18Ns", Convention.SCREAMING_KEBAB_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#CAMEL_CASE CAMEL_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToCamelCaseCase() {
        assertEquals("url2Uri", convert("url2Uri", Convention.CAMEL_CASE));
        assertEquals("url2Url", convert("url2URL", Convention.CAMEL_CASE));
        assertEquals("url2Uri", convert("Url2uri", Convention.CAMEL_CASE));
        assertEquals("url2Uri", convert("Url2Uri", Convention.CAMEL_CASE));
        assertEquals("url2Uri", convert("Url2URI", Convention.CAMEL_CASE));
        assertEquals("url2Uri", convert("URL2uri", Convention.CAMEL_CASE));
        assertEquals("url2Uri", convert("URL2Uri", Convention.CAMEL_CASE));
        assertEquals("i18N", convert("I18n", Convention.CAMEL_CASE));
        assertEquals("i18N", convert("i18N", Convention.CAMEL_CASE));
        assertEquals("i18Ns", convert("i18Ns", Convention.CAMEL_CASE));
        assertEquals("i18Ns", convert("i18NS", Convention.CAMEL_CASE));
        assertEquals("i18Ns", convert("I18ns", Convention.CAMEL_CASE));
        assertEquals("i18Ns", convert("I18Ns", Convention.CAMEL_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#PASCAL_CASE PASCAL_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToPascalCaseCase() {
        assertEquals("Url2Uri", convert("url2Uri", Convention.PASCAL_CASE));
        assertEquals("Url2Url", convert("url2URL", Convention.PASCAL_CASE));
        assertEquals("Url2Uri", convert("Url2uri", Convention.PASCAL_CASE));
        assertEquals("Url2Uri", convert("Url2Uri", Convention.PASCAL_CASE));
        assertEquals("Url2Uri", convert("Url2URI", Convention.PASCAL_CASE));
        assertEquals("Url2Uri", convert("URL2uri", Convention.PASCAL_CASE));
        assertEquals("Url2Uri", convert("URL2Uri", Convention.PASCAL_CASE));
        assertEquals("I18N", convert("I18n", Convention.PASCAL_CASE));
        assertEquals("I18N", convert("i18N", Convention.PASCAL_CASE));
        assertEquals("I18Ns", convert("i18Ns", Convention.PASCAL_CASE));
        assertEquals("I18Ns", convert("i18NS", Convention.PASCAL_CASE));
        assertEquals("I18Ns", convert("I18ns", Convention.PASCAL_CASE));
        assertEquals("I18Ns", convert("I18Ns", Convention.PASCAL_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#HTTP_HEADER_CASE HTTP_HEADER_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToHttpHeaderCaseCase() {
        assertEquals("Url-2-Uri", convert("url2Uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-2-Url", convert("url2URL", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-2-Uri", convert("Url2uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-2-Uri", convert("Url2Uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-2-Uri", convert("Url2URI", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-2-Uri", convert("URL2uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-2-Uri", convert("URL2Uri", Convention.HTTP_HEADER_CASE));
        assertEquals("I-18-N", convert("I18n", Convention.HTTP_HEADER_CASE));
        assertEquals("I-18-N", convert("i18N", Convention.HTTP_HEADER_CASE));
        assertEquals("I-18-Ns", convert("i18Ns", Convention.HTTP_HEADER_CASE));
        assertEquals("I-18-Ns", convert("i18NS", Convention.HTTP_HEADER_CASE));
        assertEquals("I-18-Ns", convert("I18ns", Convention.HTTP_HEADER_CASE));
        assertEquals("I-18-Ns", convert("I18Ns", Convention.HTTP_HEADER_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#SNAKE_CASE SNAKE_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToSnakeCaseCase() {
        assertEquals("url_2_uri", convert("url2Uri", Convention.SNAKE_CASE));
        assertEquals("url_2_url", convert("url2URL", Convention.SNAKE_CASE));
        assertEquals("url_2_uri", convert("Url2uri", Convention.SNAKE_CASE));
        assertEquals("url_2_uri", convert("Url2Uri", Convention.SNAKE_CASE));
        assertEquals("url_2_uri", convert("Url2URI", Convention.SNAKE_CASE));
        assertEquals("url_2_uri", convert("URL2uri", Convention.SNAKE_CASE));
        assertEquals("url_2_uri", convert("URL2Uri", Convention.SNAKE_CASE));
        assertEquals("i_18_n", convert("I18n", Convention.SNAKE_CASE));
        assertEquals("i_18_n", convert("i18N", Convention.SNAKE_CASE));
        assertEquals("i_18_ns", convert("i18Ns", Convention.SNAKE_CASE));
        assertEquals("i_18_ns", convert("i18NS", Convention.SNAKE_CASE));
        assertEquals("i_18_ns", convert("I18ns", Convention.SNAKE_CASE));
        assertEquals("i_18_ns", convert("I18Ns", Convention.SNAKE_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#SCREAMING_SNAKE_CASE SCREAMING_SNAKE_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToScreamingSnakeCaseCase() {
        assertEquals("URL_2_URI", convert("url2Uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_2_URL", convert("url2URL", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_2_URI", convert("Url2uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_2_URI", convert("Url2Uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_2_URI", convert("Url2URI", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_2_URI", convert("URL2uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_2_URI", convert("URL2Uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("I_18_N", convert("I18n", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("I_18_N", convert("i18N", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("I_18_NS", convert("i18Ns", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("I_18_NS", convert("i18NS", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("I_18_NS", convert("I18ns", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("I_18_NS", convert("I18Ns", Convention.SCREAMING_SNAKE_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#CAMEL_SNAKE_CASE CAMEL_SNAKE_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToCamelSnakeCaseCase() {
        assertEquals("url_2_Uri", convert("url2Uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_2_Url", convert("url2URL", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_2_Uri", convert("Url2uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_2_Uri", convert("Url2Uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_2_Uri", convert("Url2URI", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_2_Uri", convert("URL2uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_2_Uri", convert("URL2Uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("i_18_N", convert("I18n", Convention.CAMEL_SNAKE_CASE));
        assertEquals("i_18_N", convert("i18N", Convention.CAMEL_SNAKE_CASE));
        assertEquals("i_18_Ns", convert("i18Ns", Convention.CAMEL_SNAKE_CASE));
        assertEquals("i_18_Ns", convert("i18NS", Convention.CAMEL_SNAKE_CASE));
        assertEquals("i_18_Ns", convert("I18ns", Convention.CAMEL_SNAKE_CASE));
        assertEquals("i_18_Ns", convert("I18Ns", Convention.CAMEL_SNAKE_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#PASCAL_SNAKE_CASE PASCAL_SNAKE_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToPascalSnakeCaseCase() {
        assertEquals("Url_2_Uri", convert("url2Uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_2_Url", convert("url2URL", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_2_Uri", convert("Url2uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_2_Uri", convert("Url2Uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_2_Uri", convert("Url2URI", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_2_Uri", convert("URL2uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_2_Uri", convert("URL2Uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("I_18_N", convert("I18n", Convention.PASCAL_SNAKE_CASE));
        assertEquals("I_18_N", convert("i18N", Convention.PASCAL_SNAKE_CASE));
        assertEquals("I_18_Ns", convert("i18Ns", Convention.PASCAL_SNAKE_CASE));
        assertEquals("I_18_Ns", convert("i18NS", Convention.PASCAL_SNAKE_CASE));
        assertEquals("I_18_Ns", convert("I18ns", Convention.PASCAL_SNAKE_CASE));
        assertEquals("I_18_Ns", convert("I18Ns", Convention.PASCAL_SNAKE_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#FLAT_CASE FLAT_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToFlatCaseCase() {
        assertEquals("url2uri", convert("url2Uri", Convention.FLAT_CASE));
        assertEquals("url2url", convert("url2URL", Convention.FLAT_CASE));
        assertEquals("url2uri", convert("Url2uri", Convention.FLAT_CASE));
        assertEquals("url2uri", convert("Url2Uri", Convention.FLAT_CASE));
        assertEquals("url2uri", convert("Url2URI", Convention.FLAT_CASE));
        assertEquals("url2uri", convert("URL2uri", Convention.FLAT_CASE));
        assertEquals("url2uri", convert("URL2Uri", Convention.FLAT_CASE));
        assertEquals("i18n", convert("I18n", Convention.FLAT_CASE));
        assertEquals("i18n", convert("i18N", Convention.FLAT_CASE));
        assertEquals("i18ns", convert("i18Ns", Convention.FLAT_CASE));
        assertEquals("i18ns", convert("i18NS", Convention.FLAT_CASE));
        assertEquals("i18ns", convert("I18ns", Convention.FLAT_CASE));
        assertEquals("i18ns", convert("I18Ns", Convention.FLAT_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#UPPER_FLAT_CASE UPPER_FLAT_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToUpperFlatCaseCase() {
        assertEquals("URL2URI", convert("url2Uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL2URL", convert("url2URL", Convention.UPPER_FLAT_CASE));
        assertEquals("URL2URI", convert("Url2uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL2URI", convert("Url2Uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL2URI", convert("Url2URI", Convention.UPPER_FLAT_CASE));
        assertEquals("URL2URI", convert("URL2uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL2URI", convert("URL2Uri", Convention.UPPER_FLAT_CASE));
        assertEquals("I18N", convert("I18n", Convention.UPPER_FLAT_CASE));
        assertEquals("I18N", convert("i18N", Convention.UPPER_FLAT_CASE));
        assertEquals("I18NS", convert("i18Ns", Convention.UPPER_FLAT_CASE));
        assertEquals("I18NS", convert("i18NS", Convention.UPPER_FLAT_CASE));
        assertEquals("I18NS", convert("I18ns", Convention.UPPER_FLAT_CASE));
        assertEquals("I18NS", convert("I18Ns", Convention.UPPER_FLAT_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#SENTENCE_CASE SENTENCE_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToSentenceCaseCase() {
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("暂未实现", Convention.SENTENCE_CASE));
    }

    /**
     * 数字和英文混合命名转换{@link Convention#TITLE_CASE TITLE_CASE}命名方法测试
     */
    void mixedDigitAndEnglishConvertToTitleCaseCase() {
        assertThrows(UnrecognizedNamingPatternException.class, () -> convert("暂未实现", Convention.TITLE_CASE));
    }

    /**
     * 中英文混合命名转换测试
     */
    void mixedChineseAndEnglishConversionCase() {
        // 中英文混合命名转换START_CASE命名方法测试
        mixedChineseAndEnglishConvertToStartCaseCase();
        // 中英文混合命名转换KEBAB_CASE命名方法测试
        mixedChineseAndEnglishConvertToKebabCaseCase();
        // 中英文混合命名转换SCREAMING_KEBAB_CASE命名方法测试
        mixedChineseAndEnglishConvertToScreamingKebabCaseCase();
        // 中英文混合命名转换CAMEL_CASE命名方法测试
        mixedChineseAndEnglishConvertToCamelCaseCase();
        // 中英文混合命名转换PASCAL_CASE命名方法测试
        mixedChineseAndEnglishConvertToPascalCaseCase();
        // 中英文混合命名转换HTTP_HEADER_CASE命名方法测试
        mixedChineseAndEnglishConvertToHttpHeaderCaseCase();
        // 中英文混合命名转换SNAKE_CASE命名方法测试
        mixedChineseAndEnglishConvertToSnakeCaseCase();
        // 中英文混合命名转换SCREAMING_SNAKE_CASE命名方法测试
        mixedChineseAndEnglishConvertToScreamingSnakeCaseCase();
        // 中英文混合命名转换CAMEL_SNAKE_CASE命名方法测试
        mixedChineseAndEnglishConvertToCamelSnakeCaseCase();
        // 中英文混合命名转换PASCAL_SNAKE_CASE命名方法测试
        mixedChineseAndEnglishConvertToPascalSnakeCaseCase();
        // 中英文混合命名转换FLAT_CASE命名方法测试
        mixedChineseAndEnglishConvertToFlatCaseCase();
        // 中英文混合命名转换UPPER_FLAT_CASE命名方法测试
        mixedChineseAndEnglishConvertToUpperFlatCaseCase();
    }

    /**
     * 中英文混合命名转换{@link Convention#START_CASE START_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToStartCaseCase() {
        assertEquals("My Url 值", convert("myUrl值", Convention.START_CASE));
        assertEquals("My Url 地址", convert("myUrl地址", Convention.START_CASE));
        assertEquals("My Url 值", convert("myURL值", Convention.START_CASE));
        assertEquals("Url 转 Uri", convert("url转Uri", Convention.START_CASE));
        assertEquals("Url 转 Url", convert("url转URL", Convention.START_CASE));
        assertEquals("Url 转 Uri", convert("Url转uri", Convention.START_CASE));
        assertEquals("Url 转 Uri", convert("Url转Uri", Convention.START_CASE));
        assertEquals("Url 转 Uri", convert("Url转URI", Convention.START_CASE));
        assertEquals("Url 转 Uri", convert("URL转uri", Convention.START_CASE));
        assertEquals("Url 转 Uri", convert("URL转Uri", Convention.START_CASE));
        assertEquals("Url 转为 Uri", convert("url转为Uri", Convention.START_CASE));
        assertEquals("Url 转为 Url", convert("url转为URL", Convention.START_CASE));
        assertEquals("Url 转为 Uri", convert("Url转为uri", Convention.START_CASE));
        assertEquals("Url 转为 Uri", convert("Url转为Uri", Convention.START_CASE));
        assertEquals("Url 转为 Uri", convert("Url转为URI", Convention.START_CASE));
        assertEquals("Url 转为 Uri", convert("URL转为uri", Convention.START_CASE));
        assertEquals("Url 转为 Uri", convert("URL转为Uri", Convention.START_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#KEBAB_CASE KEBAB_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToKebabCaseCase() {
        assertEquals("my-url-值", convert("myUrl值", Convention.KEBAB_CASE));
        assertEquals("my-url-地址", convert("myUrl地址", Convention.KEBAB_CASE));
        assertEquals("my-url-值", convert("myURL值", Convention.KEBAB_CASE));
        assertEquals("url-转-uri", convert("url转Uri", Convention.KEBAB_CASE));
        assertEquals("url-转-url", convert("url转URL", Convention.KEBAB_CASE));
        assertEquals("url-转-uri", convert("Url转uri", Convention.KEBAB_CASE));
        assertEquals("url-转-uri", convert("Url转Uri", Convention.KEBAB_CASE));
        assertEquals("url-转-uri", convert("Url转URI", Convention.KEBAB_CASE));
        assertEquals("url-转-uri", convert("URL转uri", Convention.KEBAB_CASE));
        assertEquals("url-转-uri", convert("URL转Uri", Convention.KEBAB_CASE));
        assertEquals("url-转为-uri", convert("url转为Uri", Convention.KEBAB_CASE));
        assertEquals("url-转为-url", convert("url转为URL", Convention.KEBAB_CASE));
        assertEquals("url-转为-uri", convert("Url转为uri", Convention.KEBAB_CASE));
        assertEquals("url-转为-uri", convert("Url转为Uri", Convention.KEBAB_CASE));
        assertEquals("url-转为-uri", convert("Url转为URI", Convention.KEBAB_CASE));
        assertEquals("url-转为-uri", convert("URL转为uri", Convention.KEBAB_CASE));
        assertEquals("url-转为-uri", convert("URL转为Uri", Convention.KEBAB_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#SCREAMING_KEBAB_CASE SCREAMING_KEBAB_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToScreamingKebabCaseCase() {
        assertEquals("MY-URL-值", convert("myUrl值", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("MY-URL-地址", convert("myUrl地址", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("MY-URL-值", convert("myURL值", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转-URI", convert("url转Uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转-URL", convert("url转URL", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转-URI", convert("Url转uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转-URI", convert("Url转Uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转-URI", convert("Url转URI", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转-URI", convert("URL转uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转-URI", convert("URL转Uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转为-URI", convert("url转为Uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转为-URL", convert("url转为URL", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转为-URI", convert("Url转为uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转为-URI", convert("Url转为Uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转为-URI", convert("Url转为URI", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转为-URI", convert("URL转为uri", Convention.SCREAMING_KEBAB_CASE));
        assertEquals("URL-转为-URI", convert("URL转为Uri", Convention.SCREAMING_KEBAB_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#CAMEL_CASE CAMEL_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToCamelCaseCase() {
        assertEquals("myUrl值", convert("myUrl值", Convention.CAMEL_CASE));
        assertEquals("myUrl地址", convert("myUrl地址", Convention.CAMEL_CASE));
        assertEquals("myUrl值", convert("myURL值", Convention.CAMEL_CASE));
        assertEquals("url转Uri", convert("url转Uri", Convention.CAMEL_CASE));
        assertEquals("url转Url", convert("url转URL", Convention.CAMEL_CASE));
        assertEquals("url转Uri", convert("Url转uri", Convention.CAMEL_CASE));
        assertEquals("url转Uri", convert("Url转Uri", Convention.CAMEL_CASE));
        assertEquals("url转Uri", convert("Url转URI", Convention.CAMEL_CASE));
        assertEquals("url转Uri", convert("URL转uri", Convention.CAMEL_CASE));
        assertEquals("url转Uri", convert("URL转Uri", Convention.CAMEL_CASE));
        assertEquals("url转为Uri", convert("url转为Uri", Convention.CAMEL_CASE));
        assertEquals("url转为Url", convert("url转为URL", Convention.CAMEL_CASE));
        assertEquals("url转为Uri", convert("Url转为uri", Convention.CAMEL_CASE));
        assertEquals("url转为Uri", convert("Url转为Uri", Convention.CAMEL_CASE));
        assertEquals("url转为Uri", convert("Url转为URI", Convention.CAMEL_CASE));
        assertEquals("url转为Uri", convert("URL转为uri", Convention.CAMEL_CASE));
        assertEquals("url转为Uri", convert("URL转为Uri", Convention.CAMEL_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#PASCAL_CASE PASCAL_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToPascalCaseCase() {
        assertEquals("MyUrl值", convert("myUrl值", Convention.PASCAL_CASE));
        assertEquals("MyUrl地址", convert("myUrl地址", Convention.PASCAL_CASE));
        assertEquals("MyUrl值", convert("myURL值", Convention.PASCAL_CASE));
        assertEquals("Url转Uri", convert("url转Uri", Convention.PASCAL_CASE));
        assertEquals("Url转Url", convert("url转URL", Convention.PASCAL_CASE));
        assertEquals("Url转Uri", convert("Url转uri", Convention.PASCAL_CASE));
        assertEquals("Url转Uri", convert("Url转Uri", Convention.PASCAL_CASE));
        assertEquals("Url转Uri", convert("Url转URI", Convention.PASCAL_CASE));
        assertEquals("Url转Uri", convert("URL转uri", Convention.PASCAL_CASE));
        assertEquals("Url转Uri", convert("URL转Uri", Convention.PASCAL_CASE));
        assertEquals("Url转为Uri", convert("url转为Uri", Convention.PASCAL_CASE));
        assertEquals("Url转为Url", convert("url转为URL", Convention.PASCAL_CASE));
        assertEquals("Url转为Uri", convert("Url转为uri", Convention.PASCAL_CASE));
        assertEquals("Url转为Uri", convert("Url转为Uri", Convention.PASCAL_CASE));
        assertEquals("Url转为Uri", convert("Url转为URI", Convention.PASCAL_CASE));
        assertEquals("Url转为Uri", convert("URL转为uri", Convention.PASCAL_CASE));
        assertEquals("Url转为Uri", convert("URL转为Uri", Convention.PASCAL_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#HTTP_HEADER_CASE HTTP_HEADER_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToHttpHeaderCaseCase() {
        assertEquals("My-Url-值", convert("myUrl值", Convention.HTTP_HEADER_CASE));
        assertEquals("My-Url-地址", convert("myUrl地址", Convention.HTTP_HEADER_CASE));
        assertEquals("My-Url-值", convert("myURL值", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转-Uri", convert("url转Uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转-Url", convert("url转URL", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转-Uri", convert("Url转uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转-Uri", convert("Url转Uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转-Uri", convert("Url转URI", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转-Uri", convert("URL转uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转-Uri", convert("URL转Uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转为-Uri", convert("url转为Uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转为-Url", convert("url转为URL", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转为-Uri", convert("Url转为uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转为-Uri", convert("Url转为Uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转为-Uri", convert("Url转为URI", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转为-Uri", convert("URL转为uri", Convention.HTTP_HEADER_CASE));
        assertEquals("Url-转为-Uri", convert("URL转为Uri", Convention.HTTP_HEADER_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#SNAKE_CASE SNAKE_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToSnakeCaseCase() {
        assertEquals("my_url_值", convert("myUrl值", Convention.SNAKE_CASE));
        assertEquals("my_url_地址", convert("myUrl地址", Convention.SNAKE_CASE));
        assertEquals("my_url_值", convert("myURL值", Convention.SNAKE_CASE));
        assertEquals("url_转_uri", convert("url转Uri", Convention.SNAKE_CASE));
        assertEquals("url_转_url", convert("url转URL", Convention.SNAKE_CASE));
        assertEquals("url_转_uri", convert("Url转uri", Convention.SNAKE_CASE));
        assertEquals("url_转_uri", convert("Url转Uri", Convention.SNAKE_CASE));
        assertEquals("url_转_uri", convert("Url转URI", Convention.SNAKE_CASE));
        assertEquals("url_转_uri", convert("URL转uri", Convention.SNAKE_CASE));
        assertEquals("url_转_uri", convert("URL转Uri", Convention.SNAKE_CASE));
        assertEquals("url_转为_uri", convert("url转为Uri", Convention.SNAKE_CASE));
        assertEquals("url_转为_url", convert("url转为URL", Convention.SNAKE_CASE));
        assertEquals("url_转为_uri", convert("Url转为uri", Convention.SNAKE_CASE));
        assertEquals("url_转为_uri", convert("Url转为Uri", Convention.SNAKE_CASE));
        assertEquals("url_转为_uri", convert("Url转为URI", Convention.SNAKE_CASE));
        assertEquals("url_转为_uri", convert("URL转为uri", Convention.SNAKE_CASE));
        assertEquals("url_转为_uri", convert("URL转为Uri", Convention.SNAKE_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#SCREAMING_SNAKE_CASE SCREAMING_SNAKE_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToScreamingSnakeCaseCase() {
        assertEquals("MY_URL_值", convert("myUrl值", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("MY_URL_地址", convert("myUrl地址", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("MY_URL_值", convert("myURL值", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转_URI", convert("url转Uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转_URL", convert("url转URL", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转_URI", convert("Url转uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转_URI", convert("Url转Uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转_URI", convert("Url转URI", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转_URI", convert("URL转uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转_URI", convert("URL转Uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转为_URI", convert("url转为Uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转为_URL", convert("url转为URL", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转为_URI", convert("Url转为uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转为_URI", convert("Url转为Uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转为_URI", convert("Url转为URI", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转为_URI", convert("URL转为uri", Convention.SCREAMING_SNAKE_CASE));
        assertEquals("URL_转为_URI", convert("URL转为Uri", Convention.SCREAMING_SNAKE_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#CAMEL_SNAKE_CASE CAMEL_SNAKE_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToCamelSnakeCaseCase() {
        assertEquals("my_Url_值", convert("myUrl值", Convention.CAMEL_SNAKE_CASE));
        assertEquals("my_Url_地址", convert("myUrl地址", Convention.CAMEL_SNAKE_CASE));
        assertEquals("my_Url_值", convert("myURL值", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转_Uri", convert("url转Uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转_Url", convert("url转URL", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转_Uri", convert("Url转uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转_Uri", convert("Url转Uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转_Uri", convert("Url转URI", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转_Uri", convert("URL转uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转_Uri", convert("URL转Uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转为_Uri", convert("url转为Uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转为_Url", convert("url转为URL", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转为_Uri", convert("Url转为uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转为_Uri", convert("Url转为Uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转为_Uri", convert("Url转为URI", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转为_Uri", convert("URL转为uri", Convention.CAMEL_SNAKE_CASE));
        assertEquals("url_转为_Uri", convert("URL转为Uri", Convention.CAMEL_SNAKE_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#PASCAL_SNAKE_CASE PASCAL_SNAKE_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToPascalSnakeCaseCase() {
        assertEquals("My_Url_值", convert("myUrl值", Convention.PASCAL_SNAKE_CASE));
        assertEquals("My_Url_地址", convert("myUrl地址", Convention.PASCAL_SNAKE_CASE));
        assertEquals("My_Url_值", convert("myURL值", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转_Uri", convert("url转Uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转_Url", convert("url转URL", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转_Uri", convert("Url转uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转_Uri", convert("Url转Uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转_Uri", convert("Url转URI", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转_Uri", convert("URL转uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转_Uri", convert("URL转Uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转为_Uri", convert("url转为Uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转为_Url", convert("url转为URL", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转为_Uri", convert("Url转为uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转为_Uri", convert("Url转为Uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转为_Uri", convert("Url转为URI", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转为_Uri", convert("URL转为uri", Convention.PASCAL_SNAKE_CASE));
        assertEquals("Url_转为_Uri", convert("URL转为Uri", Convention.PASCAL_SNAKE_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#FLAT_CASE FLAT_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToFlatCaseCase() {
        assertEquals("myurl值", convert("myUrl值", Convention.FLAT_CASE));
        assertEquals("myurl地址", convert("myUrl地址", Convention.FLAT_CASE));
        assertEquals("myurl值", convert("myURL值", Convention.FLAT_CASE));
        assertEquals("url转uri", convert("url转Uri", Convention.FLAT_CASE));
        assertEquals("url转url", convert("url转URL", Convention.FLAT_CASE));
        assertEquals("url转uri", convert("Url转uri", Convention.FLAT_CASE));
        assertEquals("url转uri", convert("Url转Uri", Convention.FLAT_CASE));
        assertEquals("url转uri", convert("Url转URI", Convention.FLAT_CASE));
        assertEquals("url转uri", convert("URL转uri", Convention.FLAT_CASE));
        assertEquals("url转uri", convert("URL转Uri", Convention.FLAT_CASE));
        assertEquals("url转为uri", convert("url转为Uri", Convention.FLAT_CASE));
        assertEquals("url转为url", convert("url转为URL", Convention.FLAT_CASE));
        assertEquals("url转为uri", convert("Url转为uri", Convention.FLAT_CASE));
        assertEquals("url转为uri", convert("Url转为Uri", Convention.FLAT_CASE));
        assertEquals("url转为uri", convert("Url转为URI", Convention.FLAT_CASE));
        assertEquals("url转为uri", convert("URL转为uri", Convention.FLAT_CASE));
        assertEquals("url转为uri", convert("URL转为Uri", Convention.FLAT_CASE));
    }

    /**
     * 中英文混合命名转换{@link Convention#UPPER_FLAT_CASE UPPER_FLAT_CASE}命名方法测试
     */
    void mixedChineseAndEnglishConvertToUpperFlatCaseCase() {
        assertEquals("MYURL值", convert("myUrl值", Convention.UPPER_FLAT_CASE));
        assertEquals("MYURL地址", convert("myUrl地址", Convention.UPPER_FLAT_CASE));
        assertEquals("MYURL值", convert("myURL值", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转URI", convert("url转Uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转URL", convert("url转URL", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转URI", convert("Url转uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转URI", convert("Url转Uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转URI", convert("Url转URI", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转URI", convert("URL转uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转URI", convert("URL转Uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转为URI", convert("url转为Uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转为URL", convert("url转为URL", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转为URI", convert("Url转为uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转为URI", convert("Url转为Uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转为URI", convert("Url转为URI", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转为URI", convert("URL转为uri", Convention.UPPER_FLAT_CASE));
        assertEquals("URL转为URI", convert("URL转为Uri", Convention.UPPER_FLAT_CASE));
    }

    /**
     * 命名成分分解方法测试
     */
    @Test
    void decomposeCaseCase() {
        // 空内容命名成分分解方法测试
        blankDecomposeCase();
        // 空格分隔的命名成分分解方法测试
        spaceDecomposeCase();
        // 下划线分隔的命名成分分解方法测试
        underscoreDecomposeCase();
        // 连字符分隔的命名成分分解方法测试
        hyphenDecomposeCase();
        // 驼峰命名成分分解方法测试
        camelCaseDecomposeCase();
    }

    /**
     * 空内容命名成分分解方法测试
     */
    void blankDecomposeCase() {
        assertArrayEquals(new String[0], decompose(null));
        assertArrayEquals(new String[0], decompose(""));
        assertArrayEquals(new String[0], decompose(" "));
        assertArrayEquals(new String[0], decompose("  "));
    }

    /**
     * 空格分隔的命名成分分解方法测试
     */
    void spaceDecomposeCase() {
        assertArrayEquals(new String[]{"my", "common", "util"}, decompose("my common util"));
        assertArrayEquals(new String[]{"my", "common", "util"}, decompose("  my common util   "));
        assertArrayEquals(new String[]{"моя", "общая", "утилита"}, decompose("моя общая утилита"));
        assertArrayEquals(new String[]{"моя", "общая", "утилита"}, decompose("  моя общая утилита   "));
        assertArrayEquals(new String[]{"海", "内存", "知己"}, decompose("海 内存 知己"));
        assertArrayEquals(new String[]{"海", "内存", "知己"}, decompose("  海 内存 知己   "));
        assertArrayEquals(new String[]{"mon", "utilité", "commune"}, decompose("mon utilité commune"));
        assertArrayEquals(new String[]{"mon", "utilité", "commune"}, decompose("  mon utilité commune   "));
        assertArrayEquals(new String[]{"فائدتي", "المشتركة"}, decompose("فائدتي المشتركة"));
        assertArrayEquals(new String[]{"فائدتي", "المشتركة"}, decompose("  فائدتي المشتركة   "));
        assertArrayEquals(new String[]{"mi", "utilidad", "común"}, decompose("mi utilidad común"));
        assertArrayEquals(new String[]{"mi", "utilidad", "común"}, decompose("  mi utilidad común   "));
    }

    /**
     * 下划线分隔的命名成分分解方法测试
     */
    void underscoreDecomposeCase() {
        assertArrayEquals(new String[]{"my", "common", "util"}, decompose("my_common_util"));
        assertArrayEquals(new String[]{"моя", "общая", "утилита"}, decompose("моя_общая_утилита"));
        assertArrayEquals(new String[]{"海", "内存", "知己"}, decompose("海_内存_知己"));
        assertArrayEquals(new String[]{"mon", "utilité", "commune"}, decompose("mon_utilité_commune"));
        assertArrayEquals(new String[]{"فائدتي", "المشتركة"}, decompose("فائدتي_المشتركة"));
        assertArrayEquals(new String[]{"mi", "utilidad", "común"}, decompose("mi_utilidad_común"));
    }

    /**
     * 连字符分隔的命名成分分解方法测试
     */
    void hyphenDecomposeCase() {
        assertArrayEquals(new String[]{"my", "common", "util"}, decompose("my-common-util"));
        assertArrayEquals(new String[]{"моя", "общая", "утилита"}, decompose("моя-общая-утилита"));
        assertArrayEquals(new String[]{"海", "内存", "知己"}, decompose("海-内存-知己"));
        assertArrayEquals(new String[]{"mon", "utilité", "commune"}, decompose("mon-utilité-commune"));
        assertArrayEquals(new String[]{"فائدتي", "المشتركة"}, decompose("فائدتي-المشتركة"));
        assertArrayEquals(new String[]{"mi", "utilidad", "común"}, decompose("mi-utilidad-común"));
    }

    /**
     * 驼峰命名成分分解方法测试
     */
    void camelCaseDecomposeCase() {
        // 测试当命名为驼峰时调用“Converter.decompose(String)”方法或“Converter.decomposeCamelCase(String)”方法结果一致
        assertArrayEquals(decomposeCamelCase("myCommonUtil"), decompose("myCommonUtil"));
        assertArrayEquals(decomposeCamelCase("мояОбщаяУтилита"), decompose("мояОбщаяУтилита"));
        assertArrayEquals(decomposeCamelCase("海内存知己"), decompose("海内存知己"));
        assertArrayEquals(decomposeCamelCase("monUtilitéCommune"), decompose("monUtilitéCommune"));
        assertArrayEquals(decomposeCamelCase("فائدتيالمشتركة"), decompose("فائدتيالمشتركة"));
        assertArrayEquals(decomposeCamelCase("miUtilidadComún"), decompose("miUtilidadComún"));
        // 测试“Converter.decompose(String)”分解驼峰命名准确性，下面的decomposeCamelCaseCase()方法会进行更全面的驼峰命名分解测试
        assertArrayEquals(new String[]{"my", "Common", "Util"}, decompose("myCommonUtil"));
        // TODO: 俄语驼峰命名分解暂不支持，后续请完善
        // assertArrayEquals(new String[]{"моя", "общая", "утилита"}, decompose("мояОбщаяУтилита"));
        assertArrayEquals(new String[]{"海内存知己"}, decompose("海内存知己"));
        // TODO: 法语驼峰命名分解暂不支持，后续请完善
        // assertArrayEquals(new String[]{"mon", "utilité", "commune"}, decompose("monUtilitéCommune"));
        assertArrayEquals(new String[]{"فائدتيالمشتركة"}, decompose("فائدتيالمشتركة"));
        // TODO: 西班牙语驼峰命名分解暂不支持，后续请完善
        // assertArrayEquals(new String[]{"mi", "utilidad", "común"}, decompose("miUtilidadComún"));
    }

    /////////////////////////////////////////////////////////
    //                 以下为非公共方法测试部分                 //
    /////////////////////////////////////////////////////////

    /**
     * 驼峰命名成分分解方法测试
     */
    @Test
    void decomposeCamelCaseCase() {
        // 单双英文单词命名驼峰成分分解方法测试
        singleAndDoubleEnglishWordsDecomposeCamelCaseCase();
        // 英文单词命名驼峰成分分解方法测试
        englishWordsDecomposeCamelCaseCase();
        // 数字和英文混合命名驼峰成分分解方法测试
        mixedDigitAndEnglishDecomposeCamelCaseCase();
        // 中英文混合命名驼峰成分分解方法测试
        mixedChineseAndEnglishDecomposeCamelCaseCase();
    }

    /**
     * 单双单词命名驼峰成分分解方法测试
     */
    void singleAndDoubleEnglishWordsDecomposeCamelCaseCase() {
        assertArrayEquals(new String[]{"a"}, decomposeCamelCase("a"));
        assertArrayEquals(new String[]{"A"}, decomposeCamelCase("A"));
        assertArrayEquals(new String[]{"io"}, decomposeCamelCase("io"));
        assertArrayEquals(new String[]{"Io"}, decomposeCamelCase("Io"));
        assertArrayEquals(new String[]{"IO"}, decomposeCamelCase("IO"));
        assertArrayEquals(new String[]{"url"}, decomposeCamelCase("url"));
        assertArrayEquals(new String[]{"Url"}, decomposeCamelCase("Url"));
        assertArrayEquals(new String[]{"URL"}, decomposeCamelCase("URL"));
        assertArrayEquals(new String[]{"url", "Name"}, decomposeCamelCase("urlName"));
        assertArrayEquals(new String[]{"Url", "Name"}, decomposeCamelCase("UrlName"));
        assertArrayEquals(new String[]{"URL", "Name"}, decomposeCamelCase("URLName"));
        assertArrayEquals(new String[]{"url", "A"}, decomposeCamelCase("urlA"));
        assertArrayEquals(new String[]{"Url", "A"}, decomposeCamelCase("UrlA"));
        assertArrayEquals(new String[]{"url2uri"}, decomposeCamelCase("url2uri"));
        assertArrayEquals(new String[]{"URL2URI"}, decomposeCamelCase("URL2URI"));
        assertArrayEquals(new String[]{"log4j"}, decomposeCamelCase("log4j"));
        assertArrayEquals(new String[]{"LOG4J"}, decomposeCamelCase("LOG4J"));
        assertArrayEquals(new String[]{"i18n"}, decomposeCamelCase("i18n"));
        assertArrayEquals(new String[]{"i18ns"}, decomposeCamelCase("i18ns"));
        assertArrayEquals(new String[]{"I18NS"}, decomposeCamelCase("I18NS"));
        assertArrayEquals(new String[]{"url转uri"}, decomposeCamelCase("url转uri"));
        assertArrayEquals(new String[]{"URL转URI"}, decomposeCamelCase("URL转URI"));
        assertArrayEquals(new String[]{"url转为uri"}, decomposeCamelCase("url转为uri"));
        assertArrayEquals(new String[]{"URL转为URI"}, decomposeCamelCase("URL转为URI"));
    }

    /**
     * 英文单词命名驼峰成分分解方法测试
     */
    void englishWordsDecomposeCamelCaseCase() {
        assertArrayEquals(new String[]{"a", "Url", "Name"}, decomposeCamelCase("aUrlName"));
        assertArrayEquals(new String[]{"a", "URL", "Name"}, decomposeCamelCase("aURLName"));
        assertArrayEquals(new String[]{"a", "Url", "Name", "Description"}, decomposeCamelCase("aUrlNameDescription"));
        assertArrayEquals(new String[]{"a", "URL", "Name", "Description"}, decomposeCamelCase("aURLNameDescription"));
        assertArrayEquals(new String[]{"this", "Is", "A", "Url", "Name", "Description"}, decomposeCamelCase("thisIsAUrlNameDescription"));
        assertArrayEquals(new String[]{"this", "Is", "AURL", "Name", "Description"}, decomposeCamelCase("thisIsAURLNameDescription"));
        assertArrayEquals(new String[]{"a", "Url", "A"}, decomposeCamelCase("aUrlA"));
        assertArrayEquals(new String[]{"a", "Url", "B"}, decomposeCamelCase("aUrlB"));
    }

    /**
     * 数字和英文混合命名驼峰成分分解方法测试
     */
    void mixedDigitAndEnglishDecomposeCamelCaseCase() {
        assertArrayEquals(new String[]{"url", "2", "Uri"}, decomposeCamelCase("url2Uri"));
        assertArrayEquals(new String[]{"url", "2", "URL"}, decomposeCamelCase("url2URL"));
        assertArrayEquals(new String[]{"Url", "2", "uri"}, decomposeCamelCase("Url2uri"));
        assertArrayEquals(new String[]{"Url", "2", "Uri"}, decomposeCamelCase("Url2Uri"));
        assertArrayEquals(new String[]{"Url", "2", "URI"}, decomposeCamelCase("Url2URI"));
        assertArrayEquals(new String[]{"URL", "2", "uri"}, decomposeCamelCase("URL2uri"));
        assertArrayEquals(new String[]{"URL", "2", "Uri"}, decomposeCamelCase("URL2Uri"));
        assertArrayEquals(new String[]{"I", "18", "n"}, decomposeCamelCase("I18n"));
        assertArrayEquals(new String[]{"i", "18", "N"}, decomposeCamelCase("i18N"));
        assertArrayEquals(new String[]{"i", "18", "Ns"}, decomposeCamelCase("i18Ns"));
        assertArrayEquals(new String[]{"i", "18", "NS"}, decomposeCamelCase("i18NS"));
        assertArrayEquals(new String[]{"I", "18", "ns"}, decomposeCamelCase("I18ns"));
        assertArrayEquals(new String[]{"I", "18", "Ns"}, decomposeCamelCase("I18Ns"));
    }

    /**
     * 中英文混合命名驼峰成分分解方法测试
     */
    void mixedChineseAndEnglishDecomposeCamelCaseCase() {
        assertArrayEquals(new String[]{"my", "Url", "值"}, decomposeCamelCase("myUrl值"));
        assertArrayEquals(new String[]{"my", "Url", "地址"}, decomposeCamelCase("myUrl地址"));
        assertArrayEquals(new String[]{"my", "URL", "值"}, decomposeCamelCase("myURL值"));
        assertArrayEquals(new String[]{"url", "转", "Uri"}, decomposeCamelCase("url转Uri"));
        assertArrayEquals(new String[]{"url", "转", "URL"}, decomposeCamelCase("url转URL"));
        assertArrayEquals(new String[]{"Url", "转", "uri"}, decomposeCamelCase("Url转uri"));
        assertArrayEquals(new String[]{"Url", "转", "Uri"}, decomposeCamelCase("Url转Uri"));
        assertArrayEquals(new String[]{"Url", "转", "URI"}, decomposeCamelCase("Url转URI"));
        assertArrayEquals(new String[]{"URL", "转", "uri"}, decomposeCamelCase("URL转uri"));
        assertArrayEquals(new String[]{"URL", "转", "Uri"}, decomposeCamelCase("URL转Uri"));
        assertArrayEquals(new String[]{"url", "转为", "Uri"}, decomposeCamelCase("url转为Uri"));
        assertArrayEquals(new String[]{"url", "转为", "URL"}, decomposeCamelCase("url转为URL"));
        assertArrayEquals(new String[]{"Url", "转为", "uri"}, decomposeCamelCase("Url转为uri"));
        assertArrayEquals(new String[]{"Url", "转为", "Uri"}, decomposeCamelCase("Url转为Uri"));
        assertArrayEquals(new String[]{"Url", "转为", "URI"}, decomposeCamelCase("Url转为URI"));
        assertArrayEquals(new String[]{"URL", "转为", "uri"}, decomposeCamelCase("URL转为uri"));
        assertArrayEquals(new String[]{"URL", "转为", "Uri"}, decomposeCamelCase("URL转为Uri"));
    }

    /**
     * Start Case转换方法测试
     */
    @Test
    void convertToStartCaseCase() {
        assertEquals("", convertToStartCase(new String[]{"", "", ""}));
        assertEquals("Hello World", convertToStartCase(new String[]{"hello", "world"}));
        assertEquals("Hello World", convertToStartCase(new String[]{null, "hello", "", "world", ""}));
    }

    /**
     * 命名字符串成分规范化方法测试
     */
    @Test
    void normalizeComponentsCase() {
        assertArrayEquals(new String[0], normalizeComponents(new String[]{}));
        assertArrayEquals(new String[]{"hello", "world"}, normalizeComponents(new String[]{"Hello", " ", "world"}));
        assertArrayEquals(new String[]{"hello", "world"}, normalizeComponents(new String[]{null, "", "Hello", "", null, "", "world", null}));
    }

    /**
     * 命名字符串成分空白移除方法测试
     */
    @Test
    void removeBlankComponentsCase() {
        assertArrayEquals(new String[0], removeBlankComponents(new String[]{}));
        assertArrayEquals(new String[]{"Hello", "world"}, removeBlankComponents(new String[]{"Hello", " ", "world"}));
        assertArrayEquals(new String[]{"Hello", "world"}, removeBlankComponents(new String[]{null, "", "Hello", "", null, "", "world", null}));
    }

    /**
     * 命名字符串成分空格补全方法测试
     */
    @Test
    void completeComponentSpacesCase() {
        assertArrayEquals(new String[0], completeComponentSpaces(new String[]{}));
        assertArrayEquals(new String[]{"Hello", " ", "world"}, completeComponentSpaces(new String[]{"Hello", "world"}));
    }

    /**
     * 字符串是否为全大写或全小写判断方法测试
     */
    @Test
    void isAllLowerCaseOrUpperCaseCase() {
        assertFalse(isAllLowerCaseOrUpperCase("Hello"));
        assertTrue(isAllLowerCaseOrUpperCase("hello"));
        assertTrue(isAllLowerCaseOrUpperCase("HELLO"));
    }

}
