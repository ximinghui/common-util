/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.ximinghui.common.util.SystemProperty;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.ximinghui.common.util.SystemProperty.*;

/**
 * 系统信息工具类测试用例
 */
class TestSystemPropertyCase {

    /**
     * 当前主机名获取方法测试
     */
    @Test
    void getHostNameCase() {
        Optional<String> maybeHostName = SystemProperty.getHostName();
        assertNotNull(maybeHostName);
        maybeHostName.ifPresent(Assertions::assertNotNull);
    }

    /**
     * 当前操作系统用户名获取方法测试
     */
    @Test
    void getCurrentUserCase() {
        assertNotNull(getCurrentUser());
    }

    /**
     * 当前操作系统名获取方法测试
     */
    @Test
    void getOperatingSystemNameCase() {
        assertNotNull(getOperatingSystemName());
    }

    /**
     * 当前Java版本获取方法测试
     */
    @Test
    void getJavaVersionCase() {
        assertNotNull(getJavaVersion());
    }

    /**
     * 当前Java家目录获取方法测试
     */
    @Test
    void getJavaHomeCase() {
        assertNotNull(getJavaHome());
    }

    /**
     * 当前JVM名获取方法测试
     */
    @Test
    void getJVMNameCase() {
        assertNotNull(getJVMName());
    }

    /**
     * 当前JDK版本获取方法测试
     */
    @Test
    void getJDKVersionCase() {
        Optional<String> maybeVersion = getJDKVendorVersion();
        assertNotNull(maybeVersion);
        maybeVersion.ifPresent(Assertions::assertNotNull);
    }

}
