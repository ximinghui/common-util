/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util.test;

import org.junit.jupiter.api.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.jupiter.api.Assertions.*;
import static org.ximinghui.common.util.Characters.CharacterSet.US_ASCII;
import static org.ximinghui.common.util.Characters.*;

/**
 * 字符工具类测试用例
 */
class TestCharactersCase {

    /**
     * 字符常量测试
     */
    @Test
    void constantsCase() {
        assertEquals(' ', SPACE);
        assertEquals('_', UNDERSCORE);
        assertEquals('-', HYPHEN);
    }

    /**
     * 数字字符判断方法测试
     */
    @Test
    void isDigitCase() {
        assertTrue(isDigit('0'));
        assertTrue(isDigit('1'));
        assertTrue(isDigit('2'));
        assertTrue(isDigit('3'));
        assertTrue(isDigit('4'));
        assertTrue(isDigit('5'));
        assertTrue(isDigit('6'));
        assertTrue(isDigit('7'));
        assertTrue(isDigit('8'));
        assertTrue(isDigit('9'));
        assertFalse(isDigit('A'));
        assertFalse(isDigit('B'));
        assertFalse(isDigit('C'));
    }

    /**
     * 英文字母判断方法测试
     */
    @Test
    void isAlphabetCase() {
        assertTrue(isAlphabet('a'));
        assertTrue(isAlphabet('b'));
        assertTrue(isAlphabet('C'));
        assertTrue(isAlphabet('D'));
        assertFalse(isAlphabet('锟'));
        assertFalse(isAlphabet('斤'));
        assertFalse(isAlphabet('拷'));
        assertFalse(isAlphabet('1'));
        assertFalse(isAlphabet('2'));
        assertFalse(isAlphabet('3'));
    }

    /**
     * 所有均为英文字母判断方法测试
     */
    @Test
    void isAllAlphabetsCase() {
        assertTrue(isAllAlphabets('a', 'b', 'c'));
        assertFalse(isAllAlphabets('a', 'b', '1', 'c'));
        assertFalse(isAllAlphabets('a', 'b', '锟', 'c'));
    }

    /**
     * 包含任意英文字母判断方法测试
     */
    @Test
    void containsAnyAlphabetCase() {
        assertTrue(containsAnyAlphabet('1', '2', 'a', '3'));
        assertFalse(containsAnyAlphabet('1', '2', '3'));
        assertTrue(containsAnyAlphabet('锟', '斤', 'a', '拷'));
        assertFalse(containsAnyAlphabet('锟', '斤', '拷'));
    }

    /**
     * 非英文字母判断方法测试
     */
    @Test
    void isNotAlphabetCase() {
        assertTrue(isNotAlphabet('1'));
        assertTrue(isNotAlphabet('2'));
        assertTrue(isNotAlphabet('3'));
        assertTrue(isNotAlphabet('锟'));
        assertTrue(isNotAlphabet('斤'));
        assertTrue(isNotAlphabet('拷'));
        assertFalse(isNotAlphabet('a'));
        assertFalse(isNotAlphabet('b'));
        assertFalse(isNotAlphabet('C'));
        assertFalse(isNotAlphabet('D'));
    }

    /**
     * 所有均非英文字母判断方法测试
     */
    @Test
    void isAllNotAlphabetsCase() {
        assertTrue(isAllNotAlphabets('1', '2', '3'));
        assertFalse(isAllNotAlphabets('1', '2', 'a', '3'));
        assertTrue(isAllNotAlphabets('锟', '斤', '拷'));
        assertTrue(isAllNotAlphabets('1', '2', '锟', '3'));
    }

    /**
     * 包括任意非英文字母判断方法测试
     */
    @Test
    void containsAnyNotAlphabetCase() {
        assertFalse(containsAnyNotAlphabet('a', 'b', 'c'));
        assertTrue(containsAnyNotAlphabet('a', 'b', '1', 'c'));
        assertTrue(containsAnyNotAlphabet('a', 'b', '锟', 'c'));
    }

    /**
     * 大写英文字母判断方法测试
     */
    @Test
    void isUpperAlphabetCase() {
        assertFalse(isUpperAlphabet('a'));
        assertFalse(isUpperAlphabet('b'));
        assertTrue(isUpperAlphabet('C'));
        assertTrue(isUpperAlphabet('D'));
        assertFalse(isUpperAlphabet('1'));
        assertFalse(isUpperAlphabet('2'));
        assertFalse(isUpperAlphabet('3'));
        assertFalse(isUpperAlphabet('锟'));
        assertFalse(isUpperAlphabet('斤'));
        assertFalse(isUpperAlphabet('拷'));
    }

    /**
     * 所有均为大写英文字母判断方法测试
     */
    @Test
    void isAllUpperAlphabetsCase() {
        assertFalse(isAllUpperAlphabets('a', 'b', 'C', 'D'));
        assertFalse(isAllUpperAlphabets('A', 'B', '1', 'D'));
        assertFalse(isAllUpperAlphabets('A', 'B', '锟', 'D'));
        assertTrue(isAllUpperAlphabets('A', 'B', 'C', 'D'));
    }

    /**
     * 包含任意大写英文字母判断方法测试
     */
    @Test
    void containsAnyUpperAlphabetCase() {
        assertFalse(containsAnyUpperAlphabet('a', 'b', 'c', 'd'));
        assertFalse(containsAnyUpperAlphabet('a', 'b', '1', 'd'));
        assertFalse(containsAnyUpperAlphabet('a', 'b', '锟', 'd'));
        assertTrue(containsAnyUpperAlphabet('a', 'b', 'C', 'd'));
    }

    /**
     * 非大写英文字母判断方法测试
     */
    @Test
    void isNotUpperAlphabetCase() {
        assertTrue(isNotUpperAlphabet('a'));
        assertTrue(isNotUpperAlphabet('b'));
        assertFalse(isNotUpperAlphabet('C'));
        assertFalse(isNotUpperAlphabet('D'));
        assertTrue(isNotUpperAlphabet('1'));
        assertTrue(isNotUpperAlphabet('2'));
        assertTrue(isNotUpperAlphabet('3'));
        assertTrue(isNotUpperAlphabet('锟'));
        assertTrue(isNotUpperAlphabet('斤'));
        assertTrue(isNotUpperAlphabet('拷'));
    }

    /**
     * 所有均为非大写英文字母判断方法测试
     */
    @Test
    void isAllNotUpperAlphabetsCase() {
        assertTrue(isAllNotUpperAlphabets('a', 'b', 'c', 'd'));
        assertTrue(isAllNotUpperAlphabets('a', 'b', '1', 'd'));
        assertTrue(isAllNotUpperAlphabets('a', 'b', '锟', 'd'));
        assertFalse(isAllNotUpperAlphabets('a', 'b', 'C', 'd'));
    }

    /**
     * 包含任意非大写英文字母判断方法测试
     */
    @Test
    void containsAnyNotUpperAlphabetCase() {
        assertTrue(containsAnyNotUpperAlphabet('a', 'b', 'C', 'D'));
        assertTrue(containsAnyNotUpperAlphabet('A', 'B', '1', 'D'));
        assertTrue(containsAnyNotUpperAlphabet('A', 'B', '锟', 'D'));
        assertFalse(containsAnyNotUpperAlphabet('A', 'B', 'C', 'D'));
    }

    /**
     * 小写英文字母判断方法测试
     */
    @Test
    void isLowerAlphabetCase() {
        assertTrue(isLowerAlphabet('a'));
        assertTrue(isLowerAlphabet('b'));
        assertFalse(isLowerAlphabet('C'));
        assertFalse(isLowerAlphabet('D'));
        assertFalse(isLowerAlphabet('1'));
        assertFalse(isLowerAlphabet('2'));
        assertFalse(isLowerAlphabet('3'));
        assertFalse(isLowerAlphabet('锟'));
        assertFalse(isLowerAlphabet('斤'));
        assertFalse(isLowerAlphabet('拷'));
    }

    /**
     * 所有均为小写英文字母判断方法测试
     */
    @Test
    void isAllLowerAlphabetsCase() {
        assertTrue(isAllLowerAlphabets('a', 'b', 'c', 'd'));
        assertFalse(isAllLowerAlphabets('a', 'b', '1', 'd'));
        assertFalse(isAllLowerAlphabets('a', 'b', '锟', 'd'));
        assertFalse(isAllLowerAlphabets('a', 'b', 'C', 'd'));
    }

    /**
     * 包含任意小写英文字母判断方法测试
     */
    @Test
    void containsAnyLowerAlphabetCase() {
        assertFalse(containsAnyLowerAlphabet('1', '2', '3'));
        assertTrue(containsAnyLowerAlphabet('1', '2', 'a', '3'));
        assertFalse(containsAnyLowerAlphabet('锟', '斤', '拷'));
        assertFalse(containsAnyLowerAlphabet('1', '2', '锟', '3'));
    }

    /**
     * 非小写英文字母判断方法测试
     */
    @Test
    void isNotLowerAlphabetCase() {
        assertFalse(isNotLowerAlphabet('a'));
        assertFalse(isNotLowerAlphabet('b'));
        assertTrue(isNotLowerAlphabet('C'));
        assertTrue(isNotLowerAlphabet('D'));
        assertTrue(isNotLowerAlphabet('1'));
        assertTrue(isNotLowerAlphabet('2'));
        assertTrue(isNotLowerAlphabet('3'));
        assertTrue(isNotLowerAlphabet('锟'));
        assertTrue(isNotLowerAlphabet('斤'));
        assertTrue(isNotLowerAlphabet('拷'));
    }

    /**
     * 所有均为非小写英文字母判断方法测试
     */
    @Test
    void isAllNotLowerAlphabetsCase() {
        assertTrue(isAllNotLowerAlphabets('1', '2', '3'));
        assertFalse(isAllNotLowerAlphabets('1', '2', 'a', '3'));
        assertTrue(isAllNotLowerAlphabets('锟', '斤', '拷'));
        assertTrue(isAllNotLowerAlphabets('1', '2', '锟', '3'));
    }

    /**
     * 包含任意非小写英文字母判断方法测试
     */
    @Test
    void containsAnyNotLowerAlphabetCase() {
        assertFalse(containsAnyNotLowerAlphabet('a', 'b', 'c', 'd'));
        assertTrue(containsAnyNotLowerAlphabet('a', 'b', '1', 'd'));
        assertTrue(containsAnyNotLowerAlphabet('a', 'b', '锟', 'd'));
        assertTrue(containsAnyNotLowerAlphabet('a', 'b', 'C', 'd'));
    }

    /**
     * 英文字母判断方法测试
     */
    @Test
    void isEnglishAlphabetCase() {
        assertTrue(isEnglishAlphabet('a'));
        assertTrue(isEnglishAlphabet('b'));
        assertTrue(isEnglishAlphabet('C'));
        assertTrue(isEnglishAlphabet('D'));
        assertFalse(isEnglishAlphabet('锟'));
        assertFalse(isEnglishAlphabet('斤'));
        assertFalse(isEnglishAlphabet('拷'));
        assertFalse(isEnglishAlphabet('1'));
        assertFalse(isEnglishAlphabet('2'));
        assertFalse(isEnglishAlphabet('3'));
    }

    /**
     * 所有均为英文字母判断方法测试
     */
    @Test
    void isAllEnglishAlphabetsCase() {
        assertTrue(isAllEnglishAlphabets('a', 'b', 'c'));
        assertFalse(isAllEnglishAlphabets('a', 'b', '1', 'c'));
        assertFalse(isAllEnglishAlphabets('a', 'b', '锟', 'c'));
    }

    /**
     * 包含任意英文字母判断方法测试
     */
    @Test
    void containsAnyEnglishAlphabetCase() {
        assertTrue(containsAnyEnglishAlphabet('1', '2', 'a', '3'));
        assertFalse(containsAnyEnglishAlphabet('1', '2', '3'));
        assertTrue(containsAnyEnglishAlphabet('锟', '斤', 'a', '拷'));
        assertFalse(containsAnyEnglishAlphabet('锟', '斤', '拷'));
    }

    /**
     * 非英文字母判断方法测试
     */
    @Test
    void isNotEnglishAlphabetCase() {
        assertTrue(isNotEnglishAlphabet('1'));
        assertTrue(isNotEnglishAlphabet('2'));
        assertTrue(isNotEnglishAlphabet('3'));
        assertTrue(isNotEnglishAlphabet('锟'));
        assertTrue(isNotEnglishAlphabet('斤'));
        assertTrue(isNotEnglishAlphabet('拷'));
        assertFalse(isNotEnglishAlphabet('a'));
        assertFalse(isNotEnglishAlphabet('b'));
        assertFalse(isNotEnglishAlphabet('C'));
        assertFalse(isNotEnglishAlphabet('D'));
    }

    /**
     * 所有均非英文字母判断方法测试
     */
    @Test
    void isAllNotEnglishAlphabetsCase() {
        assertTrue(isAllNotEnglishAlphabets('1', '2', '3'));
        assertFalse(isAllNotEnglishAlphabets('1', '2', 'a', '3'));
        assertTrue(isAllNotEnglishAlphabets('锟', '斤', '拷'));
        assertTrue(isAllNotEnglishAlphabets('1', '2', '锟', '3'));
    }

    /**
     * 包括任意非英文字母判断方法测试
     */
    @Test
    void containsAnyNotEnglishAlphabetCase() {
        assertFalse(containsAnyNotEnglishAlphabet('a', 'b', 'c'));
        assertTrue(containsAnyNotEnglishAlphabet('a', 'b', '1', 'c'));
        assertTrue(containsAnyNotEnglishAlphabet('a', 'b', '锟', 'c'));
    }

    /**
     * 大写英文字母判断方法测试
     */
    @Test
    void isUpperEnglishAlphabetCase() {
        assertFalse(isUpperEnglishAlphabet('a'));
        assertFalse(isUpperEnglishAlphabet('b'));
        assertTrue(isUpperEnglishAlphabet('C'));
        assertTrue(isUpperEnglishAlphabet('D'));
        assertFalse(isUpperEnglishAlphabet('1'));
        assertFalse(isUpperEnglishAlphabet('2'));
        assertFalse(isUpperEnglishAlphabet('3'));
        assertFalse(isUpperEnglishAlphabet('锟'));
        assertFalse(isUpperEnglishAlphabet('斤'));
        assertFalse(isUpperEnglishAlphabet('拷'));
    }

    /**
     * 所有均为大写英文字母判断方法测试
     */
    @Test
    void isAllUpperEnglishAlphabetsCase() {
        assertFalse(isAllUpperEnglishAlphabets('a', 'b', 'C', 'D'));
        assertFalse(isAllUpperEnglishAlphabets('A', 'B', '1', 'D'));
        assertFalse(isAllUpperEnglishAlphabets('A', 'B', '锟', 'D'));
        assertTrue(isAllUpperEnglishAlphabets('A', 'B', 'C', 'D'));
    }

    /**
     * 包含任意大写英文字母判断方法测试
     */
    @Test
    void containsAnyUpperEnglishAlphabetCase() {
        assertFalse(containsAnyUpperEnglishAlphabet('a', 'b', 'c', 'd'));
        assertFalse(containsAnyUpperEnglishAlphabet('a', 'b', '1', 'd'));
        assertFalse(containsAnyUpperEnglishAlphabet('a', 'b', '锟', 'd'));
        assertTrue(containsAnyUpperEnglishAlphabet('a', 'b', 'C', 'd'));
    }

    /**
     * 非大写英文字母判断方法测试
     */
    @Test
    void isNotUpperEnglishAlphabetCase() {
        assertTrue(isNotUpperEnglishAlphabet('a'));
        assertTrue(isNotUpperEnglishAlphabet('b'));
        assertFalse(isNotUpperEnglishAlphabet('C'));
        assertFalse(isNotUpperEnglishAlphabet('D'));
        assertTrue(isNotUpperEnglishAlphabet('1'));
        assertTrue(isNotUpperEnglishAlphabet('2'));
        assertTrue(isNotUpperEnglishAlphabet('3'));
        assertTrue(isNotUpperEnglishAlphabet('锟'));
        assertTrue(isNotUpperEnglishAlphabet('斤'));
        assertTrue(isNotUpperEnglishAlphabet('拷'));
    }

    /**
     * 所有均为非大写英文字母判断方法测试
     */
    @Test
    void isAllNotUpperEnglishAlphabetsCase() {
        assertTrue(isAllNotUpperEnglishAlphabets('a', 'b', 'c', 'd'));
        assertTrue(isAllNotUpperEnglishAlphabets('a', 'b', '1', 'd'));
        assertTrue(isAllNotUpperEnglishAlphabets('a', 'b', '锟', 'd'));
        assertFalse(isAllNotUpperEnglishAlphabets('a', 'b', 'C', 'd'));
    }

    /**
     * 包含任意非大写英文字母判断方法测试
     */
    @Test
    void containsAnyNotUpperEnglishAlphabetCase() {
        assertTrue(containsAnyNotUpperEnglishAlphabet('a', 'b', 'C', 'D'));
        assertTrue(containsAnyNotUpperEnglishAlphabet('A', 'B', '1', 'D'));
        assertTrue(containsAnyNotUpperEnglishAlphabet('A', 'B', '锟', 'D'));
        assertFalse(containsAnyNotUpperEnglishAlphabet('A', 'B', 'C', 'D'));
    }

    /**
     * 小写英文字母判断方法测试
     */
    @Test
    void isLowerEnglishAlphabetCase() {
        assertTrue(isLowerEnglishAlphabet('a'));
        assertTrue(isLowerEnglishAlphabet('b'));
        assertFalse(isLowerEnglishAlphabet('C'));
        assertFalse(isLowerEnglishAlphabet('D'));
        assertFalse(isLowerEnglishAlphabet('1'));
        assertFalse(isLowerEnglishAlphabet('2'));
        assertFalse(isLowerEnglishAlphabet('3'));
        assertFalse(isLowerEnglishAlphabet('锟'));
        assertFalse(isLowerEnglishAlphabet('斤'));
        assertFalse(isLowerEnglishAlphabet('拷'));
    }

    /**
     * 所有均为小写英文字母判断方法测试
     */
    @Test
    void isAllLowerEnglishAlphabetsCase() {
        assertTrue(isAllLowerEnglishAlphabets('a', 'b', 'c', 'd'));
        assertFalse(isAllLowerEnglishAlphabets('a', 'b', '1', 'd'));
        assertFalse(isAllLowerEnglishAlphabets('a', 'b', '锟', 'd'));
        assertFalse(isAllLowerEnglishAlphabets('a', 'b', 'C', 'd'));
    }

    /**
     * 包含任意小写英文字母判断方法测试
     */
    @Test
    void containsAnyLowerEnglishAlphabetCase() {
        assertFalse(containsAnyLowerEnglishAlphabet('1', '2', '3'));
        assertTrue(containsAnyLowerEnglishAlphabet('1', '2', 'a', '3'));
        assertFalse(containsAnyLowerEnglishAlphabet('锟', '斤', '拷'));
        assertFalse(containsAnyLowerEnglishAlphabet('1', '2', '锟', '3'));
    }

    /**
     * 非小写英文字母判断方法测试
     */
    @Test
    void isNotLowerEnglishAlphabetCase() {
        assertFalse(isNotLowerEnglishAlphabet('a'));
        assertFalse(isNotLowerEnglishAlphabet('b'));
        assertTrue(isNotLowerEnglishAlphabet('C'));
        assertTrue(isNotLowerEnglishAlphabet('D'));
        assertTrue(isNotLowerEnglishAlphabet('1'));
        assertTrue(isNotLowerEnglishAlphabet('2'));
        assertTrue(isNotLowerEnglishAlphabet('3'));
        assertTrue(isNotLowerEnglishAlphabet('锟'));
        assertTrue(isNotLowerEnglishAlphabet('斤'));
        assertTrue(isNotLowerEnglishAlphabet('拷'));
    }

    /**
     * 所有均为非小写英文字母判断方法测试
     */
    @Test
    void isAllNotLowerEnglishAlphabetsCase() {
        assertTrue(isAllNotLowerEnglishAlphabets('1', '2', '3'));
        assertFalse(isAllNotLowerEnglishAlphabets('1', '2', 'a', '3'));
        assertTrue(isAllNotLowerEnglishAlphabets('锟', '斤', '拷'));
        assertTrue(isAllNotLowerEnglishAlphabets('1', '2', '锟', '3'));
    }

    /**
     * 包含任意非小写英文字母判断方法测试
     */
    @Test
    void containsAnyNotLowerEnglishAlphabetCase() {
        assertFalse(containsAnyNotLowerEnglishAlphabet('a', 'b', 'c', 'd'));
        assertTrue(containsAnyNotLowerEnglishAlphabet('a', 'b', '1', 'd'));
        assertTrue(containsAnyNotLowerEnglishAlphabet('a', 'b', '锟', 'd'));
        assertTrue(containsAnyNotLowerEnglishAlphabet('a', 'b', 'C', 'd'));
    }

    /**
     * 非控制字符随机生成方法测试
     */
    @Test
    void randomNonControlCharCase() {
        assertNotNull(randomNonControlChar(US_ASCII));
    }

    /**
     * 可视字符随机生成方法测试
     */
    @Test
    void randomVisibleCharCase() {
        assertNotNull(randomVisibleChar(US_ASCII));
        assertNotEquals(SPACE, randomVisibleChar(US_ASCII));
    }

    /**
     * 数字字符随机生成方法测试
     */
    @Test
    void randomDigitCharCase() {
        Set<Character> chars = new HashSet<>();
        for (int i = 0; i < 1000; i++) {
            Character c = randomDigitChar();
            assertNotNull(c);
            assertTrue(c >= '0' && c <= '9');
            chars.add(c);
        }
        assertTrue(chars.contains('0'));
        assertTrue(chars.contains('1'));
        assertTrue(chars.contains('2'));
        assertTrue(chars.contains('3'));
        assertTrue(chars.contains('4'));
        assertTrue(chars.contains('5'));
        assertTrue(chars.contains('6'));
        assertTrue(chars.contains('7'));
        assertTrue(chars.contains('8'));
        assertTrue(chars.contains('9'));
    }

}
