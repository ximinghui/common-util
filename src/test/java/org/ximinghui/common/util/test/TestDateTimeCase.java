/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util.test;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.ximinghui.common.util.DateTime;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.UnsupportedTemporalTypeException;
import java.util.Date;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.ximinghui.common.util.DateTime.*;

/**
 * 日期时间工具类测试用例
 */
class TestDateTimeCase {

    /**
     * 日期时间格式化方法测试
     */
    @Test
    void formatCase() {
        assertNotNull(format(LocalDateTime.now(), "yyyy-MM-dd HH:mm:ss"));
        assertEquals(19, format(LocalDateTime.now(), "yyyy-MM-dd HH:mm:ss").length());
        assertNotNull(format(LocalDate.now(), "yyyy-MM-dd"));
        assertEquals(10, format(LocalDate.now(), "yyyy-MM-dd").length());
        LocalDate localDate = LocalDate.now();
        Assertions.assertThrows(UnsupportedTemporalTypeException.class, () -> format(localDate, "yyyy-MM-dd HH"));
    }

    /**
     * 日期时间解析方法测试
     */
    @Test
    void parseCase() {
        assertNotNull(parse("2022-10-24 14:36:50", "yyyy-MM-dd HH:mm:ss"));
        assertNotNull(parse("2022年10月24日14时36分50秒", "yyyy年MM月dd日HH时mm分ss秒"));
    }

    /**
     * 获取某天第一个时间点方法测试
     */
    @Test
    void getFirstTimeOfDayCase() {
        assertNotNull(getFirstTimeOfDay(DateTime.getPresentTime()));
    }

    /**
     * 获取某天最后一个时间点方法测试
     */
    @Test
    void getLastTimeOfDayCase() {
        assertNotNull(getLastTimeOfDay(DateTime.getPresentTime()));
    }

    /**
     * 获取某天中特定时间点方法测试
     */
    @Test
    void getSpecifiedTimeOfDayCase() {
        assertNotNull(getSpecifiedTimeOfDay(DateTime.getPresentTime(), 20, 30, 0));
    }

    /**
     * 获取当前时间方法测试
     */
    @Test
    void getPresentTimeCase() {
        assertNotNull(getPresentTime());
        assertNotNull(getPresentTime("yyyyMMddHHmmss"));
        assertNotNull(getPresentBeijingTime());
        assertNotNull(getPresentBeijingTime("yyyyMMddHHmmss"));
    }

    /**
     * 获取昨日日期方法测试
     */
    @Test
    void getYesterdayDateCase() {
        assertNotNull(getYesterdayDate());
        assertNotNull(getYesterdayBeijingDate());
    }

    /**
     * 获取当月第一天方法测试
     */
    @Test
    void getFirstDayOfMonthCase() {
        assertNotNull(getFirstDayOfMonth());
        assertNotNull(getBeijingTimeFirstDayOfMonth());
    }

    /**
     * Date对象转换方法测试
     */
    @Test
    void toDateCase() {
        assertNotNull(toDate(LocalDateTime.now()));
        assertNotNull(toDate(LocalDate.now()));
        assertNotNull(toDateAtBeijingTimeZone(LocalDateTime.now()));
        assertNotNull(toDateAtBeijingTimeZone(LocalDate.now()));
    }

    /**
     * LocalDateTime对象转换方法测试
     */
    @Test
    void toLocalDateTimeCase() {
        assertNotNull(toLocalDateTime(new Date()));
        assertNotNull(toLocalDateTimeAtBeijingTimeZone(new Date()));
    }

    /**
     * 获取分钟前时间方法测试
     */
    @Test
    void getMinutesAgoTimeCase() {
        assertNotNull(getMinutesAgoTime(4));
        assertNotNull(getOneMinuteAgoTime());
        assertNotNull(getTwoMinutesAgoTime());
        assertNotNull(getThreeMinutesAgoTime());
        assertNotNull(getFiveMinutesAgoTime());
        assertNotNull(getTenMinutesAgoTime());
        assertNotNull(getFifteenMinutesAgoTime());
        assertNotNull(getTwentyMinutesAgoTime());
        assertNotNull(getThirtyMinutesAgoTime());
        assertNotNull(getSixtyMinutesAgoTime());
        assertNotNull(getMinutesAgoBeijingTime(4));
        assertNotNull(getOneMinuteAgoBeijingTime());
        assertNotNull(getTwoMinutesAgoBeijingTime());
        assertNotNull(getThreeMinutesAgoBeijingTime());
        assertNotNull(getFiveMinutesAgoBeijingTime());
        assertNotNull(getTenMinutesAgoBeijingTime());
        assertNotNull(getFifteenMinutesAgoBeijingTime());
        assertNotNull(getTwentyMinutesAgoBeijingTime());
        assertNotNull(getThirtyMinutesAgoBeijingTime());
        assertNotNull(getSixtyMinutesAgoBeijingTime());
    }

    /**
     * 获取分钟后时间方法测试
     */
    @Test
    void getMinutesLaterTimeCase() {
        assertNotNull(getMinutesLaterTime(4));
        assertNotNull(getOneMinuteLaterTime());
        assertNotNull(getTwoMinutesLaterTime());
        assertNotNull(getThreeMinutesLaterTime());
        assertNotNull(getFiveMinutesLaterTime());
        assertNotNull(getTenMinutesLaterTime());
        assertNotNull(getFifteenMinutesLaterTime());
        assertNotNull(getTwentyMinutesLaterTime());
        assertNotNull(getThirtyMinutesLaterTime());
        assertNotNull(getSixtyMinutesLaterTime());
        assertNotNull(getMinutesLaterBeijingTime(4));
        assertNotNull(getOneMinuteLaterBeijingTime());
        assertNotNull(getTwoMinutesLaterBeijingTime());
        assertNotNull(getThreeMinutesLaterBeijingTime());
        assertNotNull(getFiveMinutesLaterBeijingTime());
        assertNotNull(getTenMinutesLaterBeijingTime());
        assertNotNull(getFifteenMinutesLaterBeijingTime());
        assertNotNull(getTwentyMinutesLaterBeijingTime());
        assertNotNull(getThirtyMinutesLaterBeijingTime());
        assertNotNull(getSixtyMinutesLaterBeijingTime());
    }

}
