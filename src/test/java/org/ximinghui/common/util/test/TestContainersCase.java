/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util.test;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.junit.jupiter.api.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.ximinghui.common.util.Containers.*;

/**
 * 容器工具类测试用例
 */
class TestContainersCase {

    // 初始化测试数据
    static final Person[] PEOPLE_ARRAY = {
            new Person("James", 28),
            new Person("Mary", 27),
            new Person("Robert", 55),
            new Person("Linda", 18),
            new Person("John", 16),
            new Person("Lisa", 58),
            new Person("David", 13),
            new Person("Anna", 79),
            new Person("Charles", 32),
            new Person("Alice", 68),
            new Person("Jack", 31),
            new Person("Helen", 84),
            new Person("Michael", 37),
            new Person("Maria", 21)
    };

    static final List<Person> PERSON_LIST = Arrays.asList(PEOPLE_ARRAY);

    /**
     * 最大元素获取方法测试
     */
    @Test
    void maxElementCase() {
        // 断言人群中年龄最大的人是Helen（位于索引11处）
        assertEquals(PEOPLE_ARRAY[11], maxElement(PEOPLE_ARRAY, Person::getAge));
        assertEquals(PERSON_LIST.get(11), maxElement(PERSON_LIST, Person::getAge));
        // 断言人群中名字最长的人是Charles（位于索引8处）
        assertEquals(PEOPLE_ARRAY[8], maxElement(PEOPLE_ARRAY, person -> person.getName().length()));
        assertEquals(PERSON_LIST.get(8), maxElement(PERSON_LIST, person -> person.getName().length()));
    }

    /**
     * 最大值获取方法测试
     */
    @Test
    void maxValueCase() {
        // 断言人群中的最大年龄为84（位于索引11处）
        int maxAge1 = maxValue(PEOPLE_ARRAY, Person::getAge);
        int maxAge2 = maxValue(PERSON_LIST, Person::getAge);
        assertEquals(84, maxAge1, maxAge2);
        // 断言人群中的最大名字长度为7（位于索引8处）
        Integer maxNameLength1 = maxValue(PEOPLE_ARRAY, person -> person.getName().length());
        Integer maxNameLength2 = maxValue(PERSON_LIST, person -> person.getName().length());
        assertEquals(7, maxNameLength1, maxNameLength2);
    }

    /**
     * 最小元素获取方法测试
     */
    @Test
    void minElementCase() {
        // 断言人群中年龄最小的人是David（位于索引6处）
        assertEquals(PEOPLE_ARRAY[6], minElement(PEOPLE_ARRAY, Person::getAge));
        assertEquals(PERSON_LIST.get(6), minElement(PERSON_LIST, Person::getAge));
        // 断言人群中名字最短的人是Mary（位于索引1处，并列最小取第一个）
        assertEquals(PEOPLE_ARRAY[1], minElement(PEOPLE_ARRAY, person -> person.getName().length()));
        assertEquals(PERSON_LIST.get(1), minElement(PERSON_LIST, person -> person.getName().length()));
    }

    /**
     * 最小值获取方法测试
     */
    @Test
    void minValueCase() {
        // 断言人群中的最小年龄为13（位于索引6处）
        int minAge1 = minValue(PEOPLE_ARRAY, Person::getAge);
        int minAge2 = minValue(PERSON_LIST, Person::getAge);
        assertEquals(13, minAge1, minAge2);
        // 断言人群中的最小名字长度为4（位于索引1处，并列最小取第一个）
        Integer minNameLength1 = minValue(PEOPLE_ARRAY, person -> person.getName().length());
        Integer minNameLength2 = minValue(PERSON_LIST, person -> person.getName().length());
        assertEquals(4, minNameLength1, minNameLength2);
    }

    /**
     * 安全获取元素方法测试
     */
    @Test
    void safeObtainElementCase() {
        // 断言获取超出边界的索引（100）处元素返回值不为null
        assertNotNull(safeObtainElement(PEOPLE_ARRAY, 100));
        // 断言超出边界的索引（100）处元素不存在
        assertFalse(safeObtainElement(PEOPLE_ARRAY, 100).isPresent());
        // 断言获取未超出边界的索引（1）处元素返回值不为null
        assertNotNull(safeObtainElement(PEOPLE_ARRAY, 1));
        // 断言未超出边界的索引（1）处元素存在
        assertTrue(safeObtainElement(PEOPLE_ARRAY, 1).isPresent());
        // 定义默认值
        Person defaultIfNotExists = PEOPLE_ARRAY[0];
        // 断言获取超出边界的索引（100）处元素时因元素不存在而返回默认值
        assertEquals(defaultIfNotExists, safeObtainElement(PEOPLE_ARRAY, 100, defaultIfNotExists));
        // 断言获取未超出边界的索引（1）处元素时因元素存在而不返回默认值
        assertNotEquals(defaultIfNotExists, safeObtainElement(PEOPLE_ARRAY, 1, defaultIfNotExists));
        // 测试序列数据类型
        assertNotNull(safeObtainElement(PERSON_LIST, 100));
        assertFalse(safeObtainElement(PERSON_LIST, 100).isPresent());
        assertNotNull(safeObtainElement(PERSON_LIST, 1));
        assertTrue(safeObtainElement(PERSON_LIST, 1).isPresent());
        assertEquals(defaultIfNotExists, safeObtainElement(PERSON_LIST, 100, defaultIfNotExists));
        assertNotEquals(defaultIfNotExists, safeObtainElement(PERSON_LIST, 1, defaultIfNotExists));
    }

    @Data
    @NoArgsConstructor
    @AllArgsConstructor
    private static class Person {
        private String name;
        private int age;
    }

}
