/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util.test;

import org.apache.commons.lang3.RandomUtils;
import org.junit.jupiter.api.Test;
import org.ximinghui.common.util.Strings;

import java.util.Arrays;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.ximinghui.common.util.Characters.CharacterSet.US_ASCII;
import static org.ximinghui.common.util.Strings.*;

/**
 * 字符串工具类测试用例
 */
class TestStringsCase {

    /**
     * 非控制字符组成的字符串随机生成方法测试
     */
    @Test
    void randomNonControlCharCase() {
        assertNotNull(randomNonControlChar(US_ASCII));
        assertEquals(1, randomNonControlChar(US_ASCII).length());
        assertNotNull(randomNonControlChars(US_ASCII, 5));
        assertEquals(5, randomNonControlChars(US_ASCII, 5).length());
    }

    /**
     * 可视字符组成的字符串随机生成方法测试
     */
    @Test
    void randomVisibleCharCase() {
        assertNotNull(randomVisibleChar(US_ASCII));
        assertNotEquals(Strings.SPACE, randomVisibleChar(US_ASCII));
        assertEquals(1, randomVisibleChar(US_ASCII).length());
        assertNotNull(randomVisibleChars(US_ASCII, 5));
        assertFalse(randomVisibleChars(US_ASCII, 5).contains(Strings.SPACE));
        assertEquals(5, randomVisibleChars(US_ASCII, 5).length());
    }

    /**
     * 首个空格替换方法测试
     */
    @Test
    void replaceFirstSpaceCase() {
        assertEquals("First-space has been replaced with hyphen", replaceFirstSpace("First space has been replaced with hyphen", "-"));
        assertEquals("我们看得见的星星，绝大多数是恒星", replaceFirstSpace("我们看得见的星星 绝大多数是恒星", '，'));
    }

    /**
     * 所有空格替换方法测试
     */
    @Test
    void replaceAllSpacesCase() {
        assertEquals("All_spaces_are_replaced_with_underscores", replaceAllSpaces("All spaces are replaced with underscores", "_"));
        assertEquals("熊猫、海豚、霸王龙都是食肉动物", replaceAllSpaces("熊猫 海豚 霸王龙都是食肉动物", '、'));
    }

    /**
     * 按行切分字符串为数组方法测试
     */
    @Test
    void splitLineAsArrayCase() {
        String[] a1 = {"What's the Duke?", "Duke is the Java mascot."};
        String[] a2 = splitLineAsArray("What's the Duke?\nDuke is the Java mascot.");
        assertArrayEquals(a1, a2);
        String[] b1 = {"山有木兮木有枝", "心悦君兮君不知"};
        String[] b2 = splitLineAsArray("山有木兮木有枝\n心悦君兮君不知");
        assertArrayEquals(b1, b2);
    }

    /**
     * 按行切分字符串为序列方法测试
     */
    @Test
    void splitLineAsListCase() {
        List<String> a1 = Arrays.asList("There is only one heroism in the world:", "to see the world as it is and to love it.");
        List<String> a2 = splitLineAsList("There is only one heroism in the world:\nto see the world as it is and to love it.");
        assertEquals(a1, a2);
        List<String> b1 = Arrays.asList("如今最好", "别说来日方长", "时光难留", "只有一去不返", "——张嘉佳");
        List<String> b2 = splitLineAsList("如今最好\n别说来日方长\n时光难留\n只有一去不返\n——张嘉佳");
        assertEquals(b1, b2);
    }

    /**
     * 数字字符串随机生成方法测试
     */
    @Test
    void randomDigitStringCase() {
        assertNotNull(randomDigitString());
        assertEquals(1, randomDigitString().length());
        assertNotNull(randomDigitString(5));
        assertEquals(5, randomDigitString(5).length());
        assertNotNull(randomDigitString(5, true));
        assertEquals(5, randomDigitString(5, true).length());
        for (int i = 0; i < 1000; i++) {
            int length = RandomUtils.nextInt(1, 10);
            String str = randomDigitString(length, false);
            assertNotNull(str);
            assertEquals(length, str.length());
            assertFalse(str.startsWith("0"));
        }
    }

    /**
     * 对象toString字符串右边指定位数获取方法测试
     */
    @Test
    void rightCase() {
        Object stringObjectA1 = "PHP is the best language in the world";
        assertEquals("the world", right(stringObjectA1, 9));
        Object stringObjectB1 = "PHP是世界上最好的语言";
        assertEquals("最好的语言", right(stringObjectB1, 5));
    }

}
