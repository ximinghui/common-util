/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.util.Optional;

/**
 * 系统信息工具类
 *
 * @author Xi Minghui
 * @since Common Util 3.12.0.1
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class SystemProperty {

    /**
     * 获取主机名
     *
     * @return 主机名
     */
    public static Optional<String> getHostName() {
        try {
            return Optional.of(InetAddress.getLocalHost().getHostName());
        } catch (UnknownHostException e) {
            return Optional.empty();
        }
    }

    /**
     * 获取当前进程运行所使用的操作系统用户名
     *
     * @return 用户名
     */
    public static String getCurrentUser() {
        return System.getProperty("user.name");
    }

    /**
     * 获取操作系统名字
     *
     * @return 操作系统名字
     */
    public static String getOperatingSystemName() {
        return System.getProperty("os.name");
    }

    /**
     * 获取Java版本号
     *
     * @return Java版本号
     */
    public static String getJavaVersion() {
        return System.getProperty("java.version");
    }

    /**
     * 获取Java Home路径
     *
     * @return Java Home 路径
     */
    public static String getJavaHome() {
        return System.getProperty("java.home");
    }

    /**
     * 获取JVM名字
     *
     * @return JVM名字
     */
    public static String getJVMName() {
        return System.getProperty("java.vm.name");
    }

    /**
     * 获取JDK厂商版本号
     *
     * @return JDK厂商版本号
     */
    public static Optional<String> getJDKVendorVersion() {
        String vendorVersion = System.getProperty("java.vendor.version");
        return vendorVersion == null ? Optional.empty() : Optional.of(vendorVersion);
    }

}
