/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util.naming;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;
import org.ximinghui.common.util.exception.UnrecognizedNamingPatternException;

import java.util.LinkedList;
import java.util.List;
import java.util.Locale;

import static org.ximinghui.common.util.Characters.*;

/**
 * 命名转换器
 *
 * @author Xi Minghui
 * @since Common Util 3.12.0.1
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Converter {

    /**
     * 空字符串
     */
    protected static final String EMPTY_STRING = "";

    /**
     * 空格
     */
    protected static final String SPACE = " ";

    /**
     * 下划线
     */
    protected static final String UNDERSCORE = "_";

    /**
     * 连字符
     */
    protected static final String HYPHEN = "-";

    /**
     * 空字符串数组
     */
    protected static final String[] EMPTY_STRING_ARRAY = new String[0];

    /**
     * 转换命名字符串为给定的命名格式
     *
     * @param naming 命名字符串
     * @param format 命名规则
     * @return 转换后的字符串
     * @since Common Util 3.12.0.1
     */
    public static String convert(String naming, Convention format) {
        // 分解命名字符串
        String[] components = decompose(naming);
        // 检查命名字符串成分
        if (components.length == 0) return EMPTY_STRING;
        // 转换为Start Case命名方式，如：This Is An Example Name
        String text = convertToStartCase(components);
        // 匹配命名公约并进行转换
        switch (format) {
            case KEBAB_CASE:
                return text.replace(SPACE, HYPHEN).toLowerCase(Locale.ROOT);
            case SCREAMING_KEBAB_CASE:
                return text.replace(SPACE, HYPHEN).toUpperCase(Locale.ROOT);
            case CAMEL_CASE:
                return StringUtils.uncapitalize(text.replace(SPACE, EMPTY_STRING));
            case PASCAL_CASE:
                return text.replace(SPACE, EMPTY_STRING);
            case HTTP_HEADER_CASE:
                return text.replace(SPACE, HYPHEN);
            case SNAKE_CASE:
                return text.replace(SPACE, UNDERSCORE).toLowerCase(Locale.ROOT);
            case SCREAMING_SNAKE_CASE:
                return text.replace(SPACE, UNDERSCORE).toUpperCase(Locale.ROOT);
            case CAMEL_SNAKE_CASE:
                return StringUtils.uncapitalize(text.replace(SPACE, UNDERSCORE));
            case PASCAL_SNAKE_CASE:
                return text.replace(SPACE, UNDERSCORE);
            case FLAT_CASE:
                return text.replace(SPACE, EMPTY_STRING).toLowerCase(Locale.ROOT);
            case UPPER_FLAT_CASE:
                return text.replace(SPACE, EMPTY_STRING).toUpperCase(Locale.ROOT);
            case SENTENCE_CASE:
                throw new UnrecognizedNamingPatternException("Sentence Case暂未实现");
            case TITLE_CASE:
                throw new UnrecognizedNamingPatternException("Title Case暂未实现");
            case START_CASE:
                return text;
            default:
                throw new UnrecognizedNamingPatternException("暂不支持的命名规则");
        }
    }

    /**
     * 分解命名字符串
     * <p>
     * 分解命名字符串成分的算法流程：
     * <ol>
     *   <li>命名公约可简单分为自然语言使用和编程语言使用两类。两者的显著特点是自然语言使用空格分隔多个单词，而编程语言的标识符名字不包含空格。故第一步判断字符串是否包含空格；
     *   <li>若字符串包含空格，则按空格切分单词，流程结束。否则，进入下一步；
     *   <li>自然语言句子或单词间不会出现下划线字符，所以含下划线的字符串命名一定是类似蛇形命名、驼峰蛇形命名这样的以下划线为单词分隔符的命名公约。故该步判断字符串是否包含下划线；
     *   <li>若字符串包含下划线，则按下划线切分单词，流程结束。否则，进入下一步；
     *   <li>含连字符的字符串可能是类似烤串命名、HTTP头命名这样的以中划线为单词分隔符的命名公约，也可能是自然语言中类似“close-up”这样带连字符的单词（排除句子，含空格的句子已在前面匹配）。由于无法区分两种情况且后者出现概率很低，所以这里按前者处理。故该步判断字符串是否包含连字符；
     *   <li>若字符串包含连字符，则按连字符切分单词，流程结束。否则，进入下一步；
     *   <li>到这一步说明字符串为单个单元（或称单词），即字符串不包含空格、下划线、连字符。单个单元的字符串可能是驼峰命名、帕斯卡命名、大小写平坦命名（无法分解）或自然语言类命名（无法分辨）。故该步将命名判定为驼峰命名或帕斯卡命名；
     *   <li>做驼峰命名或帕斯卡命名分解，流程结束。
     * </ol>
     *
     * @param naming 命名字符串
     * @return 命名字符串成分
     * @since Common Util 3.12.0.10
     */
    public static String[] decompose(String naming) {
        String[] components;
        if (StringUtils.isBlank(naming)) components = EMPTY_STRING_ARRAY;
        else if (naming.contains(SPACE)) components = naming.split(SPACE);
        else if (naming.contains(UNDERSCORE)) components = naming.split(UNDERSCORE);
        else if (naming.contains(HYPHEN)) components = naming.split(HYPHEN);
        else components = decomposeCamelCase(naming);
        return removeBlankComponents(components);
    }

    /**
     * 分解驼峰（包括大小驼峰）命名字符串
     * <table class="striped">
     * <caption>一些示例</caption>
     * <thead>
     *     <tr><th scope="col">分解前</th><th scope="col">分解后</th></tr>
     * </thead>
     * <tbody>
     *     <tr><td>a</td><td>{a}</td></tr>
     *     <tr><td>A</td><td>{A}</td></tr>
     *     <tr><td>io</td><td>{io}</td></tr>
     *     <tr><td>Io</td><td>{Io}</td></tr>
     *     <tr><td>IO</td><td>{IO}</td></tr>
     *     <tr><td>url</td><td>{url}</td></tr>
     *     <tr><td>Url</td><td>{Url}</td></tr>
     *     <tr><td>URL</td><td>{URL}</td></tr>
     *     <tr><td>aUrlName</td><td>{a, Url, Name}</td></tr>
     *     <tr><td>aURLName</td><td>{a, URL, Name}</td></tr>
     *     <tr><td>urlName</td><td>{url, Name}</td></tr>
     *     <tr><td>UrlName</td><td>{Url, Name}</td></tr>
     *     <tr><td>URLName</td><td>{URL, Name}</td></tr>
     *     <tr><td>urlA</td><td>{url, A}</td></tr>
     *     <tr><td>UrlA</td><td>{Url, A}</td></tr>
     *     <tr><td>aUrlNameDescription</td><td>{a, Url, Name, Description}</td></tr>
     *     <tr><td>aURLNameDescription</td><td>{a, URL, Name, Description}</td></tr>
     *     <tr><td>thisIsAUrlNameDescription</td><td>{this, Is, A, Url, Name, Description}</td></tr>
     *     <tr><td>thisIsAURLNameDescription</td><td>{this, Is, A, URL, Name, Description}</td></tr>
     *     <tr><td>aUrlA</td><td>{a, Url, A}</td></tr>
     *     <tr><td>aUrlB</td><td>{a, Url, B}</td></tr>
     *     <tr><td>url2uri</td><td>{url2uri}</td></tr>
     *     <tr><td>url2Uri</td><td>{url, 2, Uri}</td></tr>
     *     <tr><td>url2URL</td><td>{url, 2, URL}</td></tr>
     *     <tr><td>Url2uri</td><td>{Url, 2, uri}</td></tr>
     *     <tr><td>Url2Uri</td><td>{Url, 2, Uri}</td></tr>
     *     <tr><td>Url2URI</td><td>{Url, 2, URI}</td></tr>
     *     <tr><td>URL2uri</td><td>{URL, 2, uri}</td></tr>
     *     <tr><td>URL2Uri</td><td>{URL, 2, Uri}</td></tr>
     *     <tr><td>URL2URI</td><td>{URL2URI}</td></tr>
     *     <tr><td>log4j</td><td>{log4j}</td></tr>
     *     <tr><td>LOG4J</td><td>{LOG4J}</td></tr>
     *     <tr><td>i18n</td><td>{i18n}</td></tr>
     *     <tr><td>I18n</td><td>{I, 18, n}</td></tr>
     *     <tr><td>i18N</td><td>{i, 18, N}</td></tr>
     *     <tr><td>i18ns</td><td>{i18ns}</td></tr>
     *     <tr><td>i18Ns</td><td>{i, 18, Ns}</td></tr>
     *     <tr><td>i18NS</td><td>{i, 18, NS}</td></tr>
     *     <tr><td>I18ns</td><td>{I, 18, ns}</td></tr>
     *     <tr><td>I18Ns</td><td>{I, 18, Ns}</td></tr>
     *     <tr><td>I18NS</td><td>{I18NS}</td></tr>
     *     <tr><td>myUrl值</td><td>{my, Url, 值}</td></tr>
     *     <tr><td>myUrl地址</td><td>{my, Url, 地址}</td></tr>
     *     <tr><td>myURL值</td><td>{my, URL, 值}</td></tr>
     *     <tr><td>url转uri</td><td>{url转uri}</td></tr>
     *     <tr><td>url转Uri</td><td>{url, 转, Uri}</td></tr>
     *     <tr><td>url转URL</td><td>{url, 转, URL}</td></tr>
     *     <tr><td>Url转uri</td><td>{Url, 转, uri}</td></tr>
     *     <tr><td>Url转Uri</td><td>{Url, 转, Uri}</td></tr>
     *     <tr><td>Url转URI</td><td>{Url, 转, URI}</td></tr>
     *     <tr><td>URL转uri</td><td>{URL, 转, uri}</td></tr>
     *     <tr><td>URL转Uri</td><td>{URL, 转, Uri}</td></tr>
     *     <tr><td>URL转URI</td><td>{URL转URI}</td></tr>
     *     <tr><td>url转为uri</td><td>{url转为uri}</td></tr>
     *     <tr><td>url转为Uri</td><td>{url, 转为, Uri}</td></tr>
     *     <tr><td>url转为URL</td><td>{url, 转为, URL}</td></tr>
     *     <tr><td>Url转为uri</td><td>{Url, 转为, uri}</td></tr>
     *     <tr><td>Url转为Uri</td><td>{Url, 转为, Uri}</td></tr>
     *     <tr><td>Url转为URI</td><td>{Url, 转为, URI}</td></tr>
     *     <tr><td>URL转为uri</td><td>{URL, 转为, uri}</td></tr>
     *     <tr><td>URL转为Uri</td><td>{URL, 转为, Uri}</td></tr>
     *     <tr><td>URL转为URI</td><td>{URL转为URI}</td></tr>
     * </tbody>
     * </table>
     *
     * @param naming 命名字符串
     * @return 命名字符串成分
     * @since Common Util 3.12.0.1
     */
    protected static String[] decomposeCamelCase(String naming) {
        if (isAllLowerCaseOrUpperCase(naming)) return new String[]{naming};
        List<String> components = new LinkedList<>();
        char[] chars = naming.toCharArray();
        int pos = 0;
        for (int i = 0; i <= chars.length; i++) {
            // 不应切分的情况直接进入下一轮循环
            if (!detectSplit(chars, i)) continue;
            // 若应切分则截取前面子字符串添加到序列中并更新pos
            components.add(naming.substring(pos, i));
            pos = i;
        }
        return components.toArray(new String[0]);
    }

    /**
     * 将命名字符串转换为{@link Convention#START_CASE Start Case}格式
     * <p>
     * 对于该工具类，Start Case格式被设计为各命名格式转换的中间态
     *
     * @param components 命名字符串成分
     * @return Capitalize Words命名格式
     * @since Common Util 3.12.0.1
     */
    protected static String convertToStartCase(String[] components) {
        // 规范化命名字符串成分
        components = normalizeComponents(components);
        // 检查命名字符串成分
        if (components.length == 0) return EMPTY_STRING;
        // 补全空格
        components = completeComponentSpaces(components);
        // 转换为Start Case
        StringBuilder sb = new StringBuilder();
        for (String c : components) sb.append(StringUtils.capitalize(c));
        // 返回结果
        return sb.toString();
    }

    /**
     * 规范化命名字符串成分
     * <p>
     * 移除所有命名成分中的空白元素，并将所有字母转为小写
     *
     * @param components 命名字符串成分
     * @return 规范化处理过的命名字符串成分
     * @since Common Util 3.12.0.10
     */
    protected static String[] normalizeComponents(String[] components) {
        LinkedList<String> list = new LinkedList<>();
        for (String c : removeBlankComponents(components)) list.add(c.toLowerCase(Locale.ROOT));
        return list.toArray(new String[0]);
    }

    /**
     * 移除空白的命名字符串成分
     *
     * @param components 命名字符串成分
     * @return 移除空白后的命名字符串成分
     * @since Common Util 3.12.0.10
     */
    protected static String[] removeBlankComponents(String[] components) {
        LinkedList<String> list = new LinkedList<>();
        for (String c : components) if (StringUtils.isNotBlank(c)) list.add(c.trim());
        return list.toArray(new String[0]);
    }

    /**
     * 补全命名字符串成分空格
     *
     * @param components 命名字符串成分
     * @return 补全空格后的命名字符串成分
     * @since Common Util 3.12.0.10
     */
    protected static String[] completeComponentSpaces(String[] components) {
        if (components.length == 0) return EMPTY_STRING_ARRAY;
        LinkedList<String> list = new LinkedList<>();
        for (String c : components) {
            list.add(c);
            list.add(SPACE);
        }
        if (SPACE.equals(list.getLast())) list.removeLast();
        return list.toArray(new String[0]);
    }

    /**
     * 判断命名字符串是否为全部大写或全部小写
     *
     * @param naming 命名字符串
     * @return 当命名字符串为全部大写或全部小写时返回true，否则返回false
     * @since Common Util 3.12.0.10
     */
    protected static boolean isAllLowerCaseOrUpperCase(String naming) {
        final Locale locale = Locale.ROOT;
        return naming.equals(naming.toLowerCase(locale)) || naming.equals(naming.toUpperCase(locale));
    }

    /**
     * 判断指定索引处字符按驼峰命名公约是否应该切分
     *
     * @param chars 字符数组
     * @param i 字符索引
     * @return 当应在指定索引处切分时返回true，否则返回false
     * @since Common Util 3.12.0.1
     */
    protected static boolean detectSplit(char[] chars, int i) {
        // 索引0前不存在字符，故无需切分位于0索引前的字符
        if (i == 0) return false;
        // 若索引等于字符数组长度则表示已到末尾，应做最后部分切分
        if (i == chars.length) return true;
        // 是否为最后一个字符标识
        final boolean isLastChar = i >= chars.length - 1;
        // 获取上一个字符
        final Character p = i > 0 ? chars[i - 1] : null;
        // 获取当前字符
        final char c = chars[i];
        // 获取下一个字符
        final Character n = isLastChar ? null : chars[i + 1];
        // 判断是否为最后一个字符
        if (isLastChar) {
            // 若当前字符和前字符有任意一个字符非英文字母或当前字符为大写英文字母且前字符为小写英文字母则切分
            return containsAnyAlphabet(p, c) && containsAnyNotAlphabet(p, c) || isUpperAlphabet(c) && isLowerAlphabet(p);
        }
        return
                // 若前字符非大写英文字母且当前字符为大写英文字母则切分
                isNotUpperAlphabet(p) && isUpperAlphabet(c) ||
                        // 若前字符为非英文字母字符且当前字符为小写英文字母则切分
                        isNotAlphabet(p) && isLowerAlphabet(c) ||
                        // 若当前字符为英文大写字母且后字符（若存在）为小写英文字母则切分
                        isUpperAlphabet(c) && isLowerAlphabet(n) ||
                        // 若当前字符非英文字母且前字符为英文字母则切分
                        isNotAlphabet(c) && isAlphabet(p) ||
                        // 若当前字符为英文字母且前字符非英文字母则切分
                        isAlphabet(c) && isNotAlphabet(p);
    }

}
