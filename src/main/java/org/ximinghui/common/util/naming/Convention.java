/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util.naming;

/**
 * 命名公约枚举类
 * <p>
 * 该类定义了一些常见的命名公约（或称为“命名规范”、“命名标准”、“命名风格”）。命名公约的中文名由作者翻译，非权威
 * 命名，故仅供参考。每种命名公约枚举值的代码注释中都提供一个采用该命名公约的命名示例，如<i>this-is-an-example-name</i>、
 * <i>ThisIsAnExampleName</i>。
 * <p>
 * “匈牙利命名”和GO语言的“MixedCaps”或“mixedCaps”并非传统的命名公约，可以说它们是基于“驼峰命名”并增加代表
 * 某种含义的命名规则而形成的，本质上它们仍属于“驼峰命名”，因此该类中没有定义这些命名，且它们命名的“含义”也无法
 * 很好的被程序化定义。
 *
 * @author Xi Minghui
 * @since Common Util 3.12.0.1
 */
public enum Convention {

    /**
     * this-is-an-example-name
     * <p>
     * 烤串命名，有时也叫“Lisp Case”、“Brochette Case”、“Spinal Case”、“Param Case”、“Dash Case”、
     * “Hyphen Case”、“CSS Case”或“Caterpillar Case”。即多个小写单词间使用连字符（中划线）连接。多个单
     * 词被中划线“穿过”看起来就像一串烤串，因此而得名。这里决定采用“Kebab Case”命名是因为该叫法自2012年之后
     * 越来越流行，且当通过搜索引擎搜索时“Kebab Case”明显比其它叫法的搜索结果多很多，Google Trends也表明该
     * 叫法比其它叫法更受欢迎。
     * <p>
     * 烤串命名广泛应用于URL路径命名、HTML属性（Property和Attribute）命名、CSS选择器和KV（属性名&#38;属
     * 性值）命名，Unix命令和包也经常采用该命名方式。另外COBOL（1959年）、Forth（1970年）和Lisp（1958年）
     * 编程语言的开发者们几乎都采用该命名方式。
     *
     * @since Common Util 3.12.0.10
     */
    KEBAB_CASE,

    /**
     * THIS-IS-AN-EXAMPLE-NAME
     * <p>
     * 大写烤串命名（或“尖叫烤串命名”），有时也叫“Cobol Case”、“Train Case（注意：‘{@link #HTTP_HEADER_CASE HTTP Header Case}’
     * 也被称为‘Train Case’）”。除了所有字母为大写外和“{@link Convention#KEBAB_CASE Kebab Case}”没
     * 有不同。
     * <p>
     * 随着计算机网络的出现，在社交媒体上完全用大写字母输入消息通常被认为是大喊大叫或其他不礼貌、争论行
     * 为，因此所有字母大写有时也被称为“尖叫”或“大喊”，所以我们称这种命名公约为“尖叫烤串命名”。
     *
     * @see #KEBAB_CASE
     * @since Common Util 3.12.0.10
     */
    SCREAMING_KEBAB_CASE,

    /**
     * thisIsAnExampleName
     * <p>
     * 驼峰命名，更精确的叫法为“小驼峰命名（Lower Camel Case / Dromedary Case）”，因其突出的大写字母像骆驼的驼
     * 峰而得名。常见的“iPhone”、“eBay”即为小驼峰命名例子，它也是Java编程语言中主要使用的命名方式。关于第一个单词
     * 的首字母是否大写定义并不明确，为了明确两种情况，人们起了一些别名以示区分，其中微软公司的开发者命名指南提供了一
     * 个不错的方案，即用“{@link Convention#PASCAL_CASE Pascal Case}”表示“大驼峰命名”，而“Camel Case”专指
     * “小驼峰命名”，本命名公约类也采用方案。
     * <p>
     * 使用驼峰命名（包含大驼峰、小驼峰）表示网站URL时可以避免地址太长和可读性差，如空格在URL中会被编码为“%20”，
     * 由于驼峰命名中没有空格，故不存在该问题。当首字母缩略词（Acronym）出现时，驼峰命名会存在一些弊端，如“HTTP SID”、
     * “DVD Player”和“parse DBM XML”采用驼峰命名将分别变为“HTTPSID”、“DVD Player”和“parseDBMXML”，
     * 这将使得读者难以甚至无法分辨单词边界，就像“HTTPSID”可能是“HTTP SID”也可能是“HTTPS ID”。出于这个原因，
     * 将缩略词当作普通的小写单词来处理的方式很受欢迎，如“HttpSid”、“DvdPlayer”、“parseDbmXml”，然而这样也
     * 带来了难以识别是普通单词还是首字母缩略词这样的新问题。
     * <p>
     * 目前主流的驼峰命名规范可以参考SUN公司于1997年发布的<a href="https://www.oracle.com/technetwork/java/codeconventions-150003.pdf">Java代码公约</a>
     * （已多年未更新）、<a href="https://google.github.io/styleguide/javaguide.html#s5.3-camel-case">谷歌Java风格指南</a>
     * （最常用样式指南之一，但其2个空格缩进的约定令许多开发者反对）、<a href="https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/naming-guidelines">微软.NET命名指南</a>
     * 和<a href="https://source.android.com/docs/setup/contribute/code-style#java-style-rules">安卓Java风格规则</a>。
     * <p>
     * Java中并没有一个关于缩写词（Abbreviation）和首字母缩略词该如何命名的约定，由于Java和.NET的命名方式非
     * 常相似，因此可借鉴<a href="https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/naming-guidelines">微软.NET命名指南</a>。
     * 简单说，<a href="https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/naming-guidelines">微软.NET命名指南</a>
     * 指出，当首字母缩略词的字母长度超过2时将其作为普通单词对待，即不超过2个字母的首字母缩略词（如“IO”）在使用
     * 时应全部大写（如“getIOStream”），而超过2个字母的首字母缩略词（如“HTML”）在使用时应当作为普通单词来对
     * 待（如“myHtmlTag”）。
     * 另外<a href="https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/naming-guidelines">微软.NET命名指南</a>
     * 也规范了复合词（Compound Word，也称“合成词”）的命名约定。复合词根据“封闭复合词”（Closed-form Compound Word）
     * 和“非封闭复合词”两种类型分别做不同的处理。对于封闭复合词（如“callback”、“endpoint”、“metadata”、
     * “namespace”）应将它们视为单个单词处理（即“Callback”、“Endpoint”、“Metadata”、“Namespace”），
     * 请勿将复合词中的每个单词首字母大写（错误演示“CallBack”、“EndPoint”、“MetaData”、“NameSpace”）。
     * 一些非封闭复合词的示例单词是：“fileName”、“logOff”、“signIn”、“userName”、“whiteSpace”。关于
     * 如何确认符合词是否以闭合形式书写，微软的给出的建议是查阅英文词典，同时文档中也给出了一些常见的<a href="https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/capitalization-conventions#capitalizing-compound-words-and-common-terms">复合词清单</a>
     * 以供参考。
     * <p>
     * 虽然不同厂商制定的命名规范有所不同，但有些概念似乎达成了共识。例如，建议尽可能在命名中使用完整的单词而不是
     * 使用首字母缩略词或缩写词，极其广泛地使用的首字母缩略词或缩写词（例如“URL”、“HTML”、“XML”，它们的全称反
     * 而使人更难以识别）除外。
     * <p>
     * 尽管采用一致的命名约定可以获得许多好处，但在PHP、.NET、Java等主流语言的官方库中仍不乏出现命名混乱的现象。
     * 常见的如.NET平台中位于{@code System.Data.Common}命名空间的{@code DbConnection}类和位于{@code System}
     * 命名空间的{@code DBNull}类，两者分别采用了“Db”和“DB”来命名类名；Visual Studio SDK中位于{@code MSXML}
     * 命名空间的{@code IXMLHttpRequest}接口采用两种方式命名“XML”和“Http”；在PHP库中存在的{@code DOMDocument}
     * 和{@code DomXPath}两个类也分别采用“DOM”和“Dom”两种不一致的形式来命名类名；Java中的驼峰大小写也十分
     * 不一致，位于{@code java.net}包下的{@code HttpURLConnection}类呈现了“Http”和“URL”两种命名方式，
     * 其<a href="https://docs.oracle.com/javase/6/docs/api/index-files/index-7.html">类库</a>
     * 中也存在大量命名不一的如“getID”、“getId”这样的混乱现象。值得一提的是，.NET从3.0/3.5版本开始出现由“ID”
     * 变为“Id”的命名趋势，在Java 8版本中也越来越多的使用“getId”命名，最新的Java API似乎已经决定只大写第一
     * 个字母（如“DosFileAttributes”、“IsoChronology”），可见微软的PascalCase约定将成为首选。在Java标
     * 准库中，4个字母的首字母缩略词按照普通单词来对待，这点与.NET保持一致，对于2个字母的首字母缩略词也同.NET保
     * 持一致采用全部大写方式命名。对于2个字母的缩写词Java并没有实际的标准，对于3个字母的首字母缩略词，可以像.NET
     * 那样首字母大写，也可以像Java类库那样全部大写（“Jar（全称Java archive）”是一个混乱的特例），无论哪种命
     * 名方式，在项目中保持一致性将会是一个良好的约定。
     * <p>
     * 众说纷纭的“ID”是一个典型的特殊情况。虽然<a href="https://learn.microsoft.com/en-us/dotnet/standard/design-guidelines/naming-guidelines">微软.NET命名指南</a>
     * 不建议使用缩写词，但唯独允许了两个特例，即“ID（identity或identifier的缩写）”和“OK（okay的缩写）”，其
     * 大驼峰应写作“Id”、“Ok”，小驼峰应写作“id”、“ok”，这也与JavaScript中的{@code getElementById}函数、
     * JPA（Java Persistence API）中的{@code jakarta.persistence.Id}（原{@code javax.persistence.Id}）
     * 注解命名一致。具有争议的是，“ID”同时也是“identity document”的首字母缩略词，很难说某一个“ID”具体指缩写
     * 词还是首字母缩略词。
     * <p>
     * 该工具提供非英文命名的驼峰支持，常见不区分大小写的有中文汉字、阿拉伯字母、阿拉伯数字等，区分大小写的有英文
     * 字母、俄语字母、法语字母、西班牙字母等。这里不区分大小写的字符按照一同（若有）出现的区分大小写的字符做决定，
     * 以不区分大小写的中文汉字和区分大小写的英文字母为例：
     * <ul>
     * <li>若所有英文字母均为大写，则中文汉字按大写处理，如“URL转URI”中的汉字视为大写，即整个字符串为一个整体；</li>
     * <li>若所有英文字母均为小写，则中文汉字按小写处理，如“url转uri”中的汉字视为小写，即整个字符串为一个整体；</li>
     * <li>若英文字母既存在大写又存在小写，则中文汉字按大写处理，如“myUrl地址”被视为由“my”、“Url”、“地址”三个部分组成。</li>
     * </ul>
     *
     * @since Common Util 3.12.0.1
     */
    CAMEL_CASE,

    /**
     * ThisIsAnExampleName
     * <p>
     * 帕斯卡命名，同属“驼峰命名”，也称为“大驼峰命名（Upper Camel Case / Capital Camel Case）”，或称
     * “Bumpy Case”、“Studly Case”、“Studly Caps”。除第一个单词的首字母为大写外和“{@link #CAMEL_CASE Camel Case}”
     * 没有不同。
     *
     * @see #CAMEL_CASE
     * @since Common Util 3.12.0.1
     */
    PASCAL_CASE,

    /**
     * This-Is-An-Example-Name
     * <p>
     * HTTP头命名，有时也叫“Train Case（注意：‘{@link #SCREAMING_KEBAB_CASE Screaming Kebab Case}’
     * 也被称为‘Train Case’）”，除了每个单词的首字母为大写外和“{@link #KEBAB_CASE Kebab Case}”没有不同。
     * 正如其名，该命名方式主要用在HTTP协议头的键（Key）命名。
     * <p>
     * “HTTP Header Case”名字的起源可能来自一个叫<a href="https://clj-commons.org/camel-snake-kebab/">camel-snake-kebab</a>
     * 的Clojure库，该库的作者使用该名字称呼这种命名方式。
     *
     * @see #KEBAB_CASE
     * @since Common Util 3.12.0.10
     */
    HTTP_HEADER_CASE,

    /**
     * this_is_an_example_name
     * <p>
     * 蛇形命名，因为它很像蛇的长身体而得名，有时称“Pothole Case”。由于该命名方式经常在C语言中使用，所以也有
     * 人称之为“C Case”。<a href="https://peps.python.org/pep-0008/#naming-conventions">Python代码风格指南</a>
     * 称其为“Lower Case With Underscores”。这种命名方式最早可追溯至1960年代晚期，它与C语言有特别的关系，
     * 与著名的“驼峰命名”形成明显对比。尽管该命名方式很流行，然而它并没有一个较为官方的名字。
     * <p>
     * 根据网络记载，该词最早出现是2004年于Ruby社群网站上由一名为“Gavin Kistner”的人提到，他写道：“BTW...
     * what *do* you call that naming style? snake_case? That's what I'll call it until someone
     * corrects me（顺便说一下，你们怎么称呼这种命名方式？snake_case吗？在没人纠正我之前，我将会这么称呼它）”。
     * 不确定的消息称，前英特尔（也许是微软）工程师“Jack Dahlgren”表示他们在2002就已经使用这种叫法了。这个名
     * 字可能是在多个社区中独立衍生出来的。
     *
     * @since Common Util 3.12.0.10
     */
    SNAKE_CASE,

    /**
     * THIS_IS_AN_EXAMPLE_NAME
     * <p>
     * 大写蛇形命名（或“尖叫蛇形命名”），有时也叫“Capitalized With Underscores”、“Upper Case（注意：‘{@link #UPPER_FLAT_CASE Upper Flat Case}’
     * 也被称为‘Upper Case’）”。除了所有字母为大写外和“{@link Convention#SNAKE_CASE Snake Case}”没
     * 有不同。
     * <p>
     * 随着计算机网络的出现，在社交媒体上完全用大写字母输入消息通常被认为是大喊大叫或其他不礼貌、争论行为，因此
     * 所有字母大写有时也被称为“尖叫”或“大喊”，所以我们称这种命名公约为“尖叫蛇形命名”。
     * <p>
     * 由于该命名方式常见于对常量进行命名或在C语言预处理器里的宏命名，故有时也被称为“Constant Case”或“Macro Case”。
     *
     * @see #SNAKE_CASE
     * @since Common Util 3.12.0.10
     */
    SCREAMING_SNAKE_CASE,

    /**
     * this_Is_An_Example_Name
     * <p>
     * 驼峰蛇形命名。小驼峰和蛇形命名的混合方式，即可视为驼峰命名每个单词间添加下划线或蛇形命名除首单词外每个单词
     * 首字母大写。
     *
     * @see #CAMEL_CASE
     * @see #SNAKE_CASE
     * @since Common Util 3.12.0.10
     */
    CAMEL_SNAKE_CASE,

    /**
     * This_Is_An_Example_Name
     * <p>
     * 帕斯卡蛇形命名，有时也叫“Mixed Case With Underscores”。除第一个单词的首字母为大写外和
     * “{@link #CAMEL_SNAKE_CASE Camel Snake Case}”没有不同。
     * <p>
     * 该命名方式典型的用例是Ada语言标识符命名，也因此有人称之为“Ada Case”。
     * <p>
     * 注意：有人也能称这种命名方式为“Title Case”，但在这里“{@link #TITLE_CASE Title Case}”为另一种命
     * 名公约。
     *
     * @see #CAMEL_SNAKE_CASE
     * @see #TITLE_CASE
     * @since Common Util 3.12.0.10
     */
    PASCAL_SNAKE_CASE,

    /**
     * thisisanexamplename
     * <p>
     * 平坦命名，有人称之为“Mumble Case”。该命名方式只是将单词以全小写形式直接拼接在一起，看起来十分平坦，故
     * 因此得名，它也被形象地称为“Lazy Case（懒惰命名）”。
     * <p>
     * 该命名方式可读性较差，Java的包命名是该命名方式的一个典型用例。
     *
     * @since Common Util 3.12.0.10
     */
    FLAT_CASE,

    /**
     * THISISANEXAMPLENAME
     * <p>
     * 大写平坦命名，有时也叫“Upper Case（注意：‘{@link #SCREAMING_SNAKE_CASE Screaming Snake Case}’
     * 也被称为‘Upper Case’）”。除所有字母大写外和“{@link #FLAT_CASE Flat Case}”没有不同。
     *
     * @since Common Util 3.12.0.10
     */
    UPPER_FLAT_CASE,

    /**
     * This is an example name
     * <p>
     * 标准英文句子
     *
     * @since Common Util 3.12.0.10
     */
    SENTENCE_CASE,

    /**
     * This Is an Example Name
     * <p>
     * 一种混合大小写的风格，所有单词都大写，但某些子集（冠词、短介词和连词）由未普遍标准化的规则定义。
     * <p>
     * 主流的规则：AP 风格手册、芝加哥风格手册、现代语言协会 (MLA) 手册、APA 风格、美国医学会 (AMA) 风格大写规则手册、蓝皮书
     * <p>
     * 参考<a href="https://en.wikipedia.org/wiki/Title_case">Title case - Wikipedia</a>
     *
     * @since Common Util 3.12.0.10
     */
    TITLE_CASE,

    /**
     * This Is An Example Name
     * <p>
     * 与“{@link #TITLE_CASE Title Case}”相似，但不考虑词性（即所有单词首字母均大写）。
     *
     * @since Common Util 3.12.0.10
     */
    START_CASE

}
