/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * 字符串工具类
 *
 * @author Xi Minghui
 * @since Common Util 3.12.0.1
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Strings extends StringUtils {

    /**
     * 下划线
     */
    public static final String UNDERSCORE = "_";

    /**
     * 连字符
     */
    public static final String HYPHEN = "-";

    /**
     * 数字或（不区分大小写）字母匹配模式
     */
    public static final String DIGIT_OR_ALPHABET_PATTERN = "[a-zA-Z\\d]+";

    /**
     * 换行符匹配模式
     */
    public static final String LINE_SEPARATOR_PATTERN = "\\n";

    /**
     * 从给定字符集中随机获取非控制字符的字符串
     *
     * @param charSet 字符集
     * @return 随机非控制字符的字符串
     */
    public static String randomNonControlChar(Characters.CharacterSet charSet) {
        return randomNonControlChars(charSet, 1);
    }

    /**
     * 从给定字符集中随机获取非控制字符的字符串
     *
     * @param charSet 字符集
     * @param length  字符串长度
     * @return 随机非控制字符的字符串
     */
    public static String randomNonControlChars(Characters.CharacterSet charSet, int length) {
        if (length <= 0) throw new IllegalArgumentException("参数length必须大于等于1：" + length);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) builder.append(Characters.randomNonControlChar(charSet));
        return builder.toString();
    }

    /**
     * 从给定字符集中随机获取字符可见的字符串
     * <p>
     * 与{@link #randomNonControlChar(Characters.CharacterSet charSet) 随机非控制字符的字符串}方法不同的是，
     * 该方法不会生成包含空白符（如空格）的字符串。
     *
     * @param charSet 字符集
     * @return 随机字符可见的字符串
     */
    public static String randomVisibleChar(Characters.CharacterSet charSet) {
        return randomVisibleChars(charSet, 1);
    }

    /**
     * 从给定字符集中随机获取字符可见的字符串
     *
     * @param charSet 字符集
     * @param length  字符串长度
     * @return 随机字符可见的字符串
     */
    public static String randomVisibleChars(Characters.CharacterSet charSet, int length) {
        if (length <= 0) throw new IllegalArgumentException("参数length必须大于等于1：" + length);
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < length; i++) builder.append(Characters.randomVisibleChar(charSet));
        return builder.toString();
    }

    /**
     * 替换字符串中出现的首个空格为指定字符串
     *
     * @param string      待处理字符串
     * @param replacement 替换字符串
     * @return 替换后的字符串
     */
    public static String replaceFirstSpace(String string, String replacement) {
        return string.replaceFirst(SPACE, replacement);
    }

    /**
     * 替换字符串中出现的首个空格为指定字符
     *
     * @param string      待处理字符串
     * @param replacement 替换字符
     * @return 替换后的字符串
     */
    public static String replaceFirstSpace(String string, Character replacement) {
        return replaceFirstSpace(string, String.valueOf(replacement));
    }

    /**
     * 替换字符串中出现的所有空格为指定字符串
     *
     * @param string      待处理字符串
     * @param replacement 替换字符串
     * @return 替换后的字符串
     */
    public static String replaceAllSpaces(String string, String replacement) {
        return string.replace(SPACE, replacement);
    }

    /**
     * 替换字符串中出现的所有空格为指定字符
     *
     * @param string      待处理字符串
     * @param replacement 替换字符串
     * @return 替换后的字符串
     */
    public static String replaceAllSpaces(String string, Character replacement) {
        return replaceAllSpaces(string, String.valueOf(replacement));
    }

    /**
     * 将字符串按行切分为数组
     *
     * @param string 字符串
     * @return 字符串数组
     */
    public static String[] splitLineAsArray(String string) {
        return string.split(LINE_SEPARATOR_PATTERN);
    }

    /**
     * 将字符串按行切分为List序列
     *
     * @param string 字符串
     * @return 字符串序列
     */
    public static List<String> splitLineAsList(String string) {
        return new ArrayList<>(Arrays.asList(splitLineAsArray(string)));
    }

    /**
     * 生成一个由随机数字字符组成的字符串
     *
     * @return 单随机数字字符组成的字符串
     */
    public static String randomDigitString() {
        return randomDigitString(1, true);
    }

    /**
     * 生成一个由随机数字字符组成的指定长度字符串
     *
     * @param length 生成字符串长度
     * @return 随机数字字符组成的指定长度字符串
     */
    public static String randomDigitString(int length) {
        return randomDigitString(length, true);
    }

    /**
     * 生成一个由随机数字字符组成且指定是否允许以0开头的指定长度字符串
     *
     * @param length         生成字符串长度
     * @param startsWithZero 是否允许以0开头
     * @return 随机数字字符组成且指定是否允许以0开头的指定长度字符串
     */
    public static String randomDigitString(int length, boolean startsWithZero) {
        if (length < 1) return EMPTY;
        StringBuilder sb = new StringBuilder();
        for (int i = sb.length(); i < length; i = sb.length()) {
            Character character = Characters.randomDigitChar();
            if (!startsWithZero && character.equals('0')) continue;
            sb.append(character);
        }
        return sb.toString();
    }

    /**
     * 获取对象toString字符串右边指定位数的子字符串
     *
     * @param object 转换为字符串的对象
     * @param length 指定长度
     * @return 自右指定长度的子字符串
     */
    public static String right(Object object, int length) {
        if (object == null) return null;
        return right(object.toString(), length);
    }

}
