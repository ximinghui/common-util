/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.time.*;
import java.time.format.DateTimeFormatter;
import java.util.Date;

/**
 * 时间工具类
 * <p>
 * 提供时间处理相关的操作
 */
@SuppressWarnings("unused")
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class DateTime {

    /**
     * 格林尼治标准时区
     */
    public static final ZoneId GREENWICH_MEAN_TIME_ZONE = ZoneOffset.UTC;

    /**
     * 中国标准时区
     */
    public static final ZoneId CHINA_TIME_ZONE = ZoneId.of("Asia/Shanghai");

    /**
     * 格式化时间
     *
     * @param localDateTime 时间对象
     * @param formatPattern 时间格式化模式
     * @return 格式化后的时间
     */
    public static String format(LocalDateTime localDateTime, String formatPattern) {
        return localDateTime.format(DateTimeFormatter.ofPattern(formatPattern));
    }

    /**
     * 格式化日期
     *
     * @param localDate     日期对象
     * @param formatPattern 日期格式化模式（不支持时间格式化模式）
     * @return 格式化后的日期
     */
    public static String format(LocalDate localDate, String formatPattern) {
        return localDate.format(DateTimeFormatter.ofPattern(formatPattern));
    }

    /**
     * 解析时间
     *
     * @param time          时间文本
     * @param formatPattern 时间格式化模式
     * @return 不带时区的日期时间对象
     */
    public static LocalDateTime parse(String time, String formatPattern) {
        return LocalDateTime.parse(time, DateTimeFormatter.ofPattern(formatPattern));
    }

    /**
     * 获取某天中的第一个时间点
     *
     * @param day 代表某一天的时间对象
     * @return 某天中的第一个时间点（即00:00:00）
     */
    public static LocalDateTime getFirstTimeOfDay(LocalDateTime day) {
        return getSpecifiedTimeOfDay(day, 0, 0, 0);
    }

    /**
     * 获取某天中的最后一个时间点
     *
     * @param day 代表某一天的时间对象
     * @return 某天中的最后一个时间点（即23:59:59）
     */
    public static LocalDateTime getLastTimeOfDay(LocalDateTime day) {
        return getSpecifiedTimeOfDay(day, 23, 59, 59);
    }

    /**
     * 获取某一天中的指定时间点
     *
     * @param day    代表某一天的时间对象
     * @param hour   指定小时
     * @param minute 指定分钟
     * @param second 指定秒
     * @return 指定时间点
     */
    public static LocalDateTime getSpecifiedTimeOfDay(LocalDateTime day, int hour, int minute, int second) {
        int year = day.getYear();
        int month = day.getMonth().getValue();
        int dayOfMonth = day.getDayOfMonth();
        return LocalDateTime.of(year, month, dayOfMonth, hour, minute, second);
    }

    /**
     * 获取格林尼治标准时间
     *
     * @return 格林尼治标准时间
     */
    public static LocalDateTime getPresentTime() {
        return LocalDateTime.now(GREENWICH_MEAN_TIME_ZONE);
    }

    /**
     * 获取格林尼治标准时间
     *
     * @param formatPattern 时间格式化模式
     * @return 格林尼治标准时间
     */
    public static String getPresentTime(String formatPattern) {
        return format(getPresentTime(), formatPattern);
    }

    /**
     * 获取格林尼治标准时间昨日日期
     *
     * @return 昨日日期
     */
    public static LocalDate getYesterdayDate() {
        return LocalDate.now(GREENWICH_MEAN_TIME_ZONE).minusDays(1);
    }

    /**
     * 获取格林尼治标准时间当前月第一天日期
     *
     * @return 本月第一天日期
     */
    public static LocalDate getFirstDayOfMonth() {
        return YearMonth.now(GREENWICH_MEAN_TIME_ZONE).atDay(1);
    }

    /**
     * 将LocalDateTime以格林尼治标准时间转为Date对象
     *
     * @param localDateTime LocalDateTime时间对象
     * @return 格林尼治标准时间Date对象
     */
    public static Date toDate(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(GREENWICH_MEAN_TIME_ZONE).toInstant());
    }

    /**
     * 将LocalDate以格林尼治标准时间转为Date对象
     *
     * @param localDate LocalDate时间对象
     * @return 格林尼治标准时间Date对象
     */
    public static Date toDate(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(GREENWICH_MEAN_TIME_ZONE).toInstant());
    }

    /**
     * 将Date以格林尼治标准时间转为LocalDateTime对象
     *
     * @param date 格林尼治标准时间Date对象
     * @return LocalDateTime时间对象
     */
    public static LocalDateTime toLocalDateTime(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(GREENWICH_MEAN_TIME_ZONE).toLocalDateTime();
    }

    /**
     * 获取指定分钟前的格林尼治标准时间
     *
     * @param minutes 指定分钟前
     * @return 指定分钟前的格林尼治标准时间
     */
    public static LocalDateTime getMinutesAgoTime(long minutes) {
        return getPresentTime().minusMinutes(minutes);
    }

    /**
     * 获取1分钟前的格林尼治标准时间
     *
     * @return 1分钟前的格林尼治标准时间
     */
    public static LocalDateTime getOneMinuteAgoTime() {
        return getPresentTime().minusMinutes(1);
    }

    /**
     * 获取2分钟前的格林尼治标准时间
     *
     * @return 2分钟前的格林尼治标准时间
     */
    public static LocalDateTime getTwoMinutesAgoTime() {
        return getPresentTime().minusMinutes(2);
    }

    /**
     * 获取3分钟前的格林尼治标准时间
     *
     * @return 3分钟前的格林尼治标准时间
     */
    public static LocalDateTime getThreeMinutesAgoTime() {
        return getPresentTime().minusMinutes(3);
    }

    /**
     * 获取5分钟前的格林尼治标准时间
     *
     * @return 5分钟前的格林尼治标准时间
     */
    public static LocalDateTime getFiveMinutesAgoTime() {
        return getPresentTime().minusMinutes(5);
    }

    /**
     * 获取10分钟前的格林尼治标准时间
     *
     * @return 10分钟前的格林尼治标准时间
     */
    public static LocalDateTime getTenMinutesAgoTime() {
        return getPresentTime().minusMinutes(10);
    }

    /**
     * 获取15分钟前的格林尼治标准时间
     *
     * @return 15分钟前的格林尼治标准时间
     */
    public static LocalDateTime getFifteenMinutesAgoTime() {
        return getPresentTime().minusMinutes(15);
    }

    /**
     * 获取20分钟前的格林尼治标准时间
     *
     * @return 20分钟前的格林尼治标准时间
     */
    public static LocalDateTime getTwentyMinutesAgoTime() {
        return getPresentTime().minusMinutes(20);
    }

    /**
     * 获取30分钟前的格林尼治标准时间
     *
     * @return 30分钟前的格林尼治标准时间
     */
    public static LocalDateTime getThirtyMinutesAgoTime() {
        return getPresentTime().minusMinutes(30);
    }

    /**
     * 获取60分钟前的格林尼治标准时间
     *
     * @return 60分钟前的格林尼治标准时间
     */
    public static LocalDateTime getSixtyMinutesAgoTime() {
        return getPresentTime().minusMinutes(60);
    }

    /**
     * 获取指定分钟后的格林尼治标准时间
     *
     * @param minutes 指定分钟后
     * @return 指定分钟后的格林尼治标准时间
     */
    public static LocalDateTime getMinutesLaterTime(long minutes) {
        return getPresentTime().plusMinutes(minutes);
    }

    /**
     * 获取1分钟后的格林尼治标准时间
     *
     * @return 1分钟后的格林尼治标准时间
     */
    public static LocalDateTime getOneMinuteLaterTime() {
        return getPresentTime().plusMinutes(1);
    }

    /**
     * 获取2分钟后的格林尼治标准时间
     *
     * @return 2分钟后的格林尼治标准时间
     */
    public static LocalDateTime getTwoMinutesLaterTime() {
        return getPresentTime().plusMinutes(2);
    }

    /**
     * 获取3分钟后的格林尼治标准时间
     *
     * @return 3分钟后的格林尼治标准时间
     */
    public static LocalDateTime getThreeMinutesLaterTime() {
        return getPresentTime().plusMinutes(3);
    }

    /**
     * 获取5分钟后的格林尼治标准时间
     *
     * @return 5分钟后的格林尼治标准时间
     */
    public static LocalDateTime getFiveMinutesLaterTime() {
        return getPresentTime().plusMinutes(5);
    }

    /**
     * 获取10分钟后的格林尼治标准时间
     *
     * @return 10分钟后的格林尼治标准时间
     */
    public static LocalDateTime getTenMinutesLaterTime() {
        return getPresentTime().plusMinutes(10);
    }

    /**
     * 获取15分钟后的格林尼治标准时间
     *
     * @return 15分钟后的格林尼治标准时间
     */
    public static LocalDateTime getFifteenMinutesLaterTime() {
        return getPresentTime().plusMinutes(15);
    }

    /**
     * 获取20分钟后的格林尼治标准时间
     *
     * @return 20分钟后的格林尼治标准时间
     */
    public static LocalDateTime getTwentyMinutesLaterTime() {
        return getPresentTime().plusMinutes(20);
    }

    /**
     * 获取30分钟后的格林尼治标准时间
     *
     * @return 30分钟后的格林尼治标准时间
     */
    public static LocalDateTime getThirtyMinutesLaterTime() {
        return getPresentTime().plusMinutes(30);
    }

    /**
     * 获取60分钟后的格林尼治标准时间
     *
     * @return 60分钟后的格林尼治标准时间
     */
    public static LocalDateTime getSixtyMinutesLaterTime() {
        return getPresentTime().plusMinutes(60);
    }

    /**
     * 获取当前中国标准时间
     *
     * @return 中国标准时间
     */
    public static LocalDateTime getPresentBeijingTime() {
        return LocalDateTime.now(CHINA_TIME_ZONE);
    }

    /**
     * 获取当前中国标准时间
     *
     * @param formatPattern 时间格式化模式
     * @return 中国标准时间
     */
    public static String getPresentBeijingTime(String formatPattern) {
        return format(getPresentBeijingTime(), formatPattern);
    }

    /**
     * 获取中国标准时间昨日日期
     *
     * @return 中国标准时间昨日日期
     */
    public static LocalDate getYesterdayBeijingDate() {
        return LocalDate.now(CHINA_TIME_ZONE).minusDays(1);
    }

    /**
     * 获取中国标准时间当前月第一天日期
     *
     * @return 本月第一天日期
     */
    public static LocalDate getBeijingTimeFirstDayOfMonth() {
        return YearMonth.now(CHINA_TIME_ZONE).atDay(1);
    }

    /**
     * 将LocalDateTime以中国标准时间转为Date对象
     *
     * @param localDateTime LocalDateTime时间对象
     * @return 中国标准时间Date对象
     */
    public static Date toDateAtBeijingTimeZone(LocalDateTime localDateTime) {
        return Date.from(localDateTime.atZone(CHINA_TIME_ZONE).toInstant());
    }

    /**
     * 将LocalDate以中国标准时间转为Date对象
     *
     * @param localDate LocalDate时间对象
     * @return 中国标准时间Date对象
     */
    public static Date toDateAtBeijingTimeZone(LocalDate localDate) {
        return Date.from(localDate.atStartOfDay(CHINA_TIME_ZONE).toInstant());
    }

    /**
     * 将Date以中国标准时间转为LocalDateTime对象
     *
     * @param date 中国标准时间Date对象
     * @return LocalDateTime时间对象
     */
    public static LocalDateTime toLocalDateTimeAtBeijingTimeZone(Date date) {
        return Instant.ofEpochMilli(date.getTime()).atZone(CHINA_TIME_ZONE).toLocalDateTime();
    }

    /**
     * 获取指定分钟前的中国标准时间
     *
     * @param minutes 指定分钟前
     * @return 指定分钟前的中国标准时间
     */
    public static LocalDateTime getMinutesAgoBeijingTime(long minutes) {
        return getPresentBeijingTime().minusMinutes(minutes);
    }

    /**
     * 获取1分钟前的中国标准时间
     *
     * @return 1分钟前的中国标准时间
     */
    public static LocalDateTime getOneMinuteAgoBeijingTime() {
        return getPresentBeijingTime().minusMinutes(1);
    }

    /**
     * 获取2分钟前的中国标准时间
     *
     * @return 2分钟前的中国标准时间
     */
    public static LocalDateTime getTwoMinutesAgoBeijingTime() {
        return getPresentBeijingTime().minusMinutes(2);
    }

    /**
     * 获取3分钟前的中国标准时间
     *
     * @return 3分钟前的中国标准时间
     */
    public static LocalDateTime getThreeMinutesAgoBeijingTime() {
        return getPresentBeijingTime().minusMinutes(3);
    }

    /**
     * 获取5分钟前的中国标准时间
     *
     * @return 5分钟前的中国标准时间
     */
    public static LocalDateTime getFiveMinutesAgoBeijingTime() {
        return getPresentBeijingTime().minusMinutes(5);
    }

    /**
     * 获取10分钟前的中国标准时间
     *
     * @return 10分钟前中国治标准时间
     */
    public static LocalDateTime getTenMinutesAgoBeijingTime() {
        return getPresentBeijingTime().minusMinutes(10);
    }

    /**
     * 获取15分钟前的中国标准时间
     *
     * @return 15分钟前中国治标准时间
     */
    public static LocalDateTime getFifteenMinutesAgoBeijingTime() {
        return getPresentBeijingTime().minusMinutes(15);
    }

    /**
     * 获取20分钟前的中国标准时间
     *
     * @return 20分钟前中国治标准时间
     */
    public static LocalDateTime getTwentyMinutesAgoBeijingTime() {
        return getPresentBeijingTime().minusMinutes(20);
    }

    /**
     * 获取30分钟前的中国标准时间
     *
     * @return 30分钟前中国治标准时间
     */
    public static LocalDateTime getThirtyMinutesAgoBeijingTime() {
        return getPresentBeijingTime().minusMinutes(30);
    }

    /**
     * 获取60分钟前的中国标准时间
     *
     * @return 60分钟前中国治标准时间
     */
    public static LocalDateTime getSixtyMinutesAgoBeijingTime() {
        return getPresentBeijingTime().minusMinutes(60);
    }

    /**
     * 获取指定分钟后的中国标准时间
     *
     * @param minutes 指定分钟后
     * @return 指定分钟后的中国标准时间
     */
    public static LocalDateTime getMinutesLaterBeijingTime(long minutes) {
        return getPresentBeijingTime().plusMinutes(minutes);
    }

    /**
     * 获取1分钟后的中国标准时间
     *
     * @return 1分钟后的中国标准时间
     */
    public static LocalDateTime getOneMinuteLaterBeijingTime() {
        return getPresentBeijingTime().plusMinutes(1);
    }

    /**
     * 获取2分钟后的中国标准时间
     *
     * @return 2分钟后的中国标准时间
     */
    public static LocalDateTime getTwoMinutesLaterBeijingTime() {
        return getPresentBeijingTime().plusMinutes(2);
    }

    /**
     * 获取3分钟后的中国标准时间
     *
     * @return 3分钟后的中国标准时间
     */
    public static LocalDateTime getThreeMinutesLaterBeijingTime() {
        return getPresentBeijingTime().plusMinutes(3);
    }

    /**
     * 获取5分钟后的中国标准时间
     *
     * @return 5分钟后的中国标准时间
     */
    public static LocalDateTime getFiveMinutesLaterBeijingTime() {
        return getPresentBeijingTime().plusMinutes(5);
    }

    /**
     * 获取10分钟后的中国标准时间
     *
     * @return 10分钟后的中国标准时间
     */
    public static LocalDateTime getTenMinutesLaterBeijingTime() {
        return getPresentBeijingTime().plusMinutes(10);
    }

    /**
     * 获取15分钟后的中国标准时间
     *
     * @return 15分钟后的中国标准时间
     */
    public static LocalDateTime getFifteenMinutesLaterBeijingTime() {
        return getPresentBeijingTime().plusMinutes(15);
    }

    /**
     * 获取20分钟后的中国标准时间
     *
     * @return 20分钟后中国治标准时间
     */
    public static LocalDateTime getTwentyMinutesLaterBeijingTime() {
        return getPresentBeijingTime().plusMinutes(20);
    }

    /**
     * 获取30分钟后的中国标准时间
     *
     * @return 30分钟后中国治标准时间
     */
    public static LocalDateTime getThirtyMinutesLaterBeijingTime() {
        return getPresentBeijingTime().plusMinutes(30);
    }

    /**
     * 获取60分钟后的中国标准时间
     *
     * @return 60分钟后中国治标准时间
     */
    public static LocalDateTime getSixtyMinutesLaterBeijingTime() {
        return getPresentBeijingTime().plusMinutes(60);
    }

}
