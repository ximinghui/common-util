/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import org.apache.commons.lang3.CharUtils;
import org.apache.commons.lang3.RandomUtils;
import org.ximinghui.common.util.exception.UnsupportedCharacterSetException;

/**
 * 字符工具类
 *
 * @author Xi Minghui
 * @since Common Util 3.12.0.1
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Characters extends CharUtils {

    /**
     * 空格
     */
    public static final Character SPACE = ' ';

    /**
     * 下划线
     */
    public static final Character UNDERSCORE = '_';

    /**
     * 连字符
     */
    public static final Character HYPHEN = '-';

    /**
     * 判断给定字符是否为数字
     *
     * @param character 待判断字符
     * @return 当给定字符为0~9的数字字符时返回true，否则返回false
     * @since Common Util 3.12.0.8
     */
    public static boolean isDigit(Character character) {
        if (character == null) return false;
        return character >= '0' && character <= '9';
    }

    /**
     * 判断字符是否为英文字母
     * <p>
     * 同{@link #isEnglishAlphabet(char)}方法
     *
     * @param c 待判断字符
     * @return 当待判断字符为26个英文字母之一时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAlphabet(char c) {
        return isEnglishAlphabet(c);
    }

    /**
     * 判断字符是否都为英文字母
     * <p>
     * 同{@link #isAllEnglishAlphabets(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都为英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isAllEnglishAlphabets(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllAlphabets(char... chars) {
        return isAllEnglishAlphabets(chars);
    }

    /**
     * 判断字符是否存在任意英文字母
     * <p>
     * 同{@link #containsAnyEnglishAlphabet(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#containsAnyEnglishAlphabet(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyAlphabet(char... chars) {
        return containsAnyEnglishAlphabet(chars);
    }

    /**
     * 判断字符是否英文字母
     * <p>
     * 同{@link #isNotEnglishAlphabet(char)}方法
     *
     * @param c 待判断字符
     * @return 当待判断字符非26个英文字母之一时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isNotEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isNotAlphabet(char c) {
        return isNotEnglishAlphabet(c);
    }

    /**
     * 判断字符是否都非英文字母
     * <p>
     * 同{@link #isAllNotEnglishAlphabets(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都非英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isAllNotEnglishAlphabets(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllNotAlphabets(char... chars) {
        return isAllNotEnglishAlphabets(chars);
    }

    /**
     * 判断字符是否存在任意非英文字母
     * <p>
     * 同{@link #containsAnyNotEnglishAlphabet(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何非英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#containsAnyNotEnglishAlphabet(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyNotAlphabet(char... chars) {
        return containsAnyNotEnglishAlphabet(chars);
    }

    /**
     * 判断字符是否为大写英文字母
     * <p>
     * 同{@link #isUpperEnglishAlphabet(char)}方法
     *
     * @param c 待判断字符
     * @return 当待判断字符为大写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isUpperEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isUpperAlphabet(char c) {
        return isUpperEnglishAlphabet(c);
    }

    /**
     * 判断字符是否都为大写英文字母
     * <p>
     * 同{@link #isAllUpperEnglishAlphabets(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都为大写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isAllUpperEnglishAlphabets(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllUpperAlphabets(char... chars) {
        return isAllUpperEnglishAlphabets(chars);
    }

    /**
     * 判断字符是否存在任意大写英文字母
     * <p>
     * 同{@link #containsAnyUpperEnglishAlphabet(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何大写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#containsAnyUpperEnglishAlphabet(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyUpperAlphabet(char... chars) {
        return containsAnyUpperEnglishAlphabet(chars);
    }

    /**
     * 判断字符是否非大写英文字母
     * <p>
     * 同{@link #isNotUpperEnglishAlphabet(char)}方法
     *
     * @param c 待判断字符
     * @return 当待判断字符非大写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isNotUpperEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isNotUpperAlphabet(char c) {
        return isNotUpperEnglishAlphabet(c);
    }

    /**
     * 判断字符是否都非大写英文字母
     * <p>
     * 同{@link #isAllNotUpperEnglishAlphabets(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都非大写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isAllNotUpperEnglishAlphabets(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllNotUpperAlphabets(char... chars) {
        return isAllNotUpperEnglishAlphabets(chars);
    }

    /**
     * 判断字符是否存在任意非大写英文字母
     * <p>
     * 同{@link #containsAnyNotUpperEnglishAlphabet(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何非大写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#containsAnyNotUpperEnglishAlphabet(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyNotUpperAlphabet(char... chars) {
        return containsAnyNotUpperEnglishAlphabet(chars);
    }

    /**
     * 判断字符是否为小写英文字母
     * <p>
     * 同{@link #isLowerEnglishAlphabet(char)}方法
     *
     * @param c 待判断字符
     * @return 当待判断字符为小写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isLowerEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isLowerAlphabet(char c) {
        return isLowerEnglishAlphabet(c);
    }

    /**
     * 判断字符是否都为小写英文字母
     * <p>
     * 同{@link #isAllLowerEnglishAlphabets(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都为小写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isAllLowerEnglishAlphabets(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllLowerAlphabets(char... chars) {
        return isAllLowerEnglishAlphabets(chars);
    }

    /**
     * 判断字符是否存在任意小写英文字母
     * <p>
     * 同{@link #containsAnyLowerEnglishAlphabet(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符在存在任何小写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#containsAnyLowerEnglishAlphabet(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyLowerAlphabet(char... chars) {
        return containsAnyLowerEnglishAlphabet(chars);
    }

    /**
     * 判断字符是否非小写英文字母
     * <p>
     * 同{@link #isNotLowerEnglishAlphabet(char)}方法
     *
     * @param c 待判断字符
     * @return 当待判断字符非小写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isNotLowerEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isNotLowerAlphabet(char c) {
        return isNotLowerEnglishAlphabet(c);
    }

    /**
     * 判断字符是否都非小写英文字母
     * <p>
     * 同{@link #isAllNotLowerEnglishAlphabets(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都非小写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isAllNotLowerEnglishAlphabets(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllNotLowerAlphabets(char... chars) {
        return isAllNotLowerEnglishAlphabets(chars);
    }

    /**
     * 判断字符是否存在任意非小写英文字母
     * <p>
     * 同{@link #containsAnyNotLowerEnglishAlphabet(char...)}方法
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何非小写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#containsAnyNotLowerEnglishAlphabet(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyNotLowerAlphabet(char... chars) {
        return containsAnyNotLowerEnglishAlphabet(chars);
    }

    /**
     * 判断字符是否为英文字母
     * <table class="striped">
     * <caption>一些示例</caption>
     * <thead>
     *     <tr><th scope="col">待判断字符</th><th scope="col">判断结果</th></tr>
     * </thead>
     * <tbody>
     *     <tr><td>a</td><td>true</td></tr>
     *     <tr><td>b</td><td>true</td></tr>
     *     <tr><td>C</td><td>true</td></tr>
     *     <tr><td>D</td><td>true</td></tr>
     *     <tr><td>锟</td><td>false</td></tr>
     *     <tr><td>斤</td><td>false</td></tr>
     *     <tr><td>拷</td><td>false</td></tr>
     *     <tr><td>1</td><td>false</td></tr>
     *     <tr><td>2</td><td>false</td></tr>
     *     <tr><td>3</td><td>false</td></tr>
     * </tbody>
     * </table>
     *
     * @param c 待判断字符
     * @return 当待判断字符为26个英文字母之一时返回true，否则返回false
     * @since Common Util 3.12.0.9
     */
    public static boolean isEnglishAlphabet(char c) {
        return isAsciiAlpha(c);
    }

    /**
     * 判断字符是否都为英文字母
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都为英文字母时返回true，否则有任意一个字符非英文字母都将返回false
     * @see org.ximinghui.common.util.Characters#isNotEnglishAlphabet(char)
     * @see org.ximinghui.common.util.Characters#isEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllEnglishAlphabets(char... chars) {
        for (char c : chars) if (isNotEnglishAlphabet(c)) return false;
        return true;
    }

    /**
     * 判断字符是否存在任意英文字母
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyEnglishAlphabet(char... chars) {
        for (char c : chars) if (isEnglishAlphabet(c)) return true;
        return false;
    }

    /**
     * 判断字符是否非英文字母
     * <p>
     * isEnglishAlphabet(char)方法的取反
     *
     * @param c 待判断字符
     * @return 当待判断字符非26个英文字母之一时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isNotEnglishAlphabet(char c) {
        return !isEnglishAlphabet(c);
    }

    /**
     * 判断字符是否都非英文字母
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都非英文字母时返回true，否则有任意一个字符为英文字母都将返回false
     * @see org.ximinghui.common.util.Characters#isEnglishAlphabet(char)
     * @see org.ximinghui.common.util.Characters#isNotEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllNotEnglishAlphabets(char... chars) {
        for (char c : chars) if (isEnglishAlphabet(c)) return false;
        return true;
    }

    /**
     * 判断字符是否存在任意非英文字母
     * <p>
     * {@link #isAllEnglishAlphabets(char...)}方法取反
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何非英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isAllEnglishAlphabets(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyNotEnglishAlphabet(char... chars) {
        return !isAllEnglishAlphabets(chars);
    }

    /**
     * 判断字符是否为大写英文字母
     * <table class="striped">
     * <caption>一些示例</caption>
     * <thead>
     *     <tr><th scope="col">待判断字符</th><th scope="col">判断结果</th></tr>
     * </thead>
     * <tbody>
     *     <tr><td>a</td><td>false</td></tr>
     *     <tr><td>b</td><td>false</td></tr>
     *     <tr><td>C</td><td>true</td></tr>
     *     <tr><td>D</td><td>true</td></tr>
     *     <tr><td>锟</td><td>false</td></tr>
     *     <tr><td>斤</td><td>false</td></tr>
     *     <tr><td>拷</td><td>false</td></tr>
     *     <tr><td>1</td><td>false</td></tr>
     *     <tr><td>2</td><td>false</td></tr>
     *     <tr><td>3</td><td>false</td></tr>
     * </tbody>
     * </table>
     *
     * @param c 待判断字符
     * @return 当待判断字符为大写英文字母时返回true，否则返回false
     * @since Common Util 3.12.0.9
     */
    public static boolean isUpperEnglishAlphabet(char c) {
        return isAsciiAlphaUpper(c);
    }

    /**
     * 判断字符是否都为大写英文字母
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都为大写英文字母时返回true，否则有任意一个字符非大写英文字母都将返回false
     * @see org.ximinghui.common.util.Characters#isNotUpperEnglishAlphabet(char)
     * @see org.ximinghui.common.util.Characters#isUpperEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllUpperEnglishAlphabets(char... chars) {
        for (char c : chars) if (isNotUpperEnglishAlphabet(c)) return false;
        return true;
    }

    /**
     * 判断字符是否存在任意大写英文字母
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何大写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isUpperEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyUpperEnglishAlphabet(char... chars) {
        for (char c : chars) if (isUpperEnglishAlphabet(c)) return true;
        return false;
    }

    /**
     * 判断字符是否非大写英文字母
     * <p>
     * isUpperAlphabet(char)方法的取反
     *
     * @param c 待判断字符
     * @return 当待判断字符非大写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isUpperEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isNotUpperEnglishAlphabet(char c) {
        return !isUpperEnglishAlphabet(c);
    }

    /**
     * 判断字符是否都非大写英文字母
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都非大写英文字母时返回true，否则有任意一个字符为大写英文字母都将返回false
     * @see org.ximinghui.common.util.Characters#isUpperEnglishAlphabet(char)
     * @see org.ximinghui.common.util.Characters#isNotUpperEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllNotUpperEnglishAlphabets(char... chars) {
        for (char c : chars) if (isUpperEnglishAlphabet(c)) return false;
        return true;
    }

    /**
     * 判断字符是否存在任意非大写英文字母
     * <p>
     * {@link #isAllUpperEnglishAlphabets(char...)}方法取反
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何非大写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isAllUpperEnglishAlphabets(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyNotUpperEnglishAlphabet(char... chars) {
        return !isAllUpperEnglishAlphabets(chars);
    }

    /**
     * 判断字符是否为小写英文字母
     * <table class="striped">
     * <caption>一些示例</caption>
     * <thead>
     *     <tr><th scope="col">待判断字符</th><th scope="col">判断结果</th></tr>
     * </thead>
     * <tbody>
     *     <tr><td>a</td><td>true</td></tr>
     *     <tr><td>b</td><td>true</td></tr>
     *     <tr><td>C</td><td>false</td></tr>
     *     <tr><td>D</td><td>false</td></tr>
     *     <tr><td>锟</td><td>false</td></tr>
     *     <tr><td>斤</td><td>false</td></tr>
     *     <tr><td>拷</td><td>false</td></tr>
     *     <tr><td>1</td><td>false</td></tr>
     *     <tr><td>2</td><td>false</td></tr>
     *     <tr><td>3</td><td>false</td></tr>
     * </tbody>
     * </table>
     *
     * @param c 待判断字符
     * @return 当待判断字符为小写英文字母时返回true，否则返回false
     * @since Common Util 3.12.0.9
     */
    public static boolean isLowerEnglishAlphabet(char c) {
        return isAsciiAlphaLower(c);
    }

    /**
     * 判断字符是否都为小写英文字母
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都为小写英文字母时返回true，否则有任意一个字符非小写英文字母都将返回false
     * @see org.ximinghui.common.util.Characters#isNotLowerEnglishAlphabet(char)
     * @see org.ximinghui.common.util.Characters#isLowerEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllLowerEnglishAlphabets(char... chars) {
        for (char c : chars) if (isNotLowerEnglishAlphabet(c)) return false;
        return true;
    }

    /**
     * 判断字符是否存在任意小写英文字母
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何小写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isLowerEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyLowerEnglishAlphabet(char... chars) {
        for (char c : chars) if (isLowerEnglishAlphabet(c)) return true;
        return false;
    }

    /**
     * 判断字符是否非小写英文字母
     * <p>
     * isLowerAlphabet(char)方法的取反
     *
     * @param c 待判断字符
     * @return 当待判断字符非小写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isLowerEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isNotLowerEnglishAlphabet(char c) {
        return !isLowerEnglishAlphabet(c);
    }

    /**
     * 判断字符是否都非小写英文字母
     * <p>
     * {@link #containsAnyLowerEnglishAlphabet(char...)}方法取反
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符都非小写英文字母时返回true，否则有任意一个字符为小写英文字母都将返回false
     * @see org.ximinghui.common.util.Characters#isLowerEnglishAlphabet(char)
     * @see org.ximinghui.common.util.Characters#isNotLowerEnglishAlphabet(char)
     * @since Common Util 3.12.0.9
     */
    public static boolean isAllNotLowerEnglishAlphabets(char... chars) {
        return !containsAnyLowerEnglishAlphabet(chars);
    }

    /**
     * 判断字符是否存在任意非小写英文字母
     * <p>
     * {@link #isAllLowerEnglishAlphabets(char...)}方法取反
     *
     * @param chars 待判断字符数组
     * @return 当待判断字符中存在任何非小写英文字母时返回true，否则返回false
     * @see org.ximinghui.common.util.Characters#isAllLowerEnglishAlphabets(char...)
     * @since Common Util 3.12.0.9
     */
    public static boolean containsAnyNotLowerEnglishAlphabet(char... chars) {
        return !isAllLowerEnglishAlphabets(chars);
    }

    /**
     * 从给定字符集中随机获取一个非控制字符
     *
     * @param charSet 字符集
     * @return 随机非控制字符
     * @since Common Util 3.12.0.1
     */
    public static Character randomNonControlChar(CharacterSet charSet) {
        switch (charSet) {
            case US_ASCII:
                return (char) RandomUtils.nextInt(32, 127);
            case ISO_8859_1:
                return (char) (RandomUtils.nextBoolean() ? RandomUtils.nextInt(32, 127) : RandomUtils.nextInt(160, 256));
            default:
                throw new UnsupportedCharacterSetException("不支持的字符集");
        }
    }

    /**
     * 从给定字符集中随机获取一个可见字符
     * <p>
     * 与{@link #randomNonControlChar(Characters.CharacterSet)}方法不同的是，该方法不
     * 会生成包含空白字符（如空格）。
     *
     * @param charSet 字符集
     * @return 随机可见字符
     * @since Common Util 3.12.0.1
     */
    public static Character randomVisibleChar(CharacterSet charSet) {
        switch (charSet) {
            case US_ASCII:
                return (char) RandomUtils.nextInt(33, 127);
            case ISO_8859_1:
                return (char) (RandomUtils.nextBoolean() ? RandomUtils.nextInt(33, 127) : RandomUtils.nextInt(161, 256));
            default:
                throw new UnsupportedCharacterSetException("不支持的字符集");
        }
    }

    /**
     * 生成一个随机数字字符
     *
     * @return 随机数字字符
     * @since Common Util 3.12.0.5
     */
    public static Character randomDigitChar() {
        return String.valueOf(RandomUtils.nextInt(0, 10)).charAt(0);
    }

    /**
     * 字符集定义
     *
     * @since Common Util 3.12.0.1
     */
    public enum CharacterSet {
        /**
         * ASCII
         */
        US_ASCII,

        /**
         * ISO
         */
        ISO_8859_1
    }

}
