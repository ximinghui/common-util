/*
 * MIT License
 *
 * Copyright (c) 2022 Xi Minghui
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 */
package org.ximinghui.common.util;

import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.util.*;
import java.util.function.Function;

/**
 * 容器工具类
 *
 * @author Xi Minghui
 * @since Common Util 3.12.0.1
 */
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class Containers {

    /**
     * 获取容器中函数对象返回值最大的元素
     *
     * @param elements 一组元素
     * @param function 函数对象
     * @param <E> 元素泛型
     * @param <V> 函数对象返回值泛型
     * @return 最大元素
     */
    public static <E, V extends Comparable<V>> E maxElement(Collection<E> elements, Function<E, V> function) {
        if (elements == null || elements.isEmpty()) throw new IllegalArgumentException("容器中不存在任意元素");
        Iterator<E> iterator = elements.iterator();
        E max = iterator.next();
        for (E e = max; iterator.hasNext(); e = iterator.next()) if (isFormerBigger(e, max, function)) max = e;
        return max;
    }

    /**
     * 获取容器中函数对象返回值最大的元素
     *
     * @param elements 一组元素
     * @param function 函数对象
     * @param <E> 元素泛型
     * @param <V> 函数对象返回值泛型
     * @return 最大元素
     */
    public static <E, V extends Comparable<V>> E maxElement(E[] elements, Function<E, V> function) {
        return maxElement(Arrays.asList(elements), function);
    }

    /**
     * 获取容器中函数对象返回值最大的元素的值
     *
     * @param elements 一组元素
     * @param function 函数对象
     * @param <E> 元素泛型
     * @param <V> 函数对象返回值泛型
     * @return 最大元素的值
     */
    public static <E, V extends Comparable<V>> V maxValue(Collection<E> elements, Function<E, V> function) {
        return function.apply(maxElement(elements, function));
    }

    /**
     * 获取容器中函数对象返回值最大的元素的值
     *
     * @param elements 一组元素
     * @param function 函数对象
     * @param <E> 元素泛型
     * @param <V> 函数对象返回值泛型
     * @return 最大元素的值
     */
    public static <E, V extends Comparable<V>> V maxValue(E[] elements, Function<E, V> function) {
        return function.apply(maxElement(elements, function));
    }

    /**
     * 获取容器中函数对象返回值最小的元素
     *
     * @param elements 一组元素
     * @param function 函数对象
     * @param <E> 元素泛型
     * @param <V> 函数对象返回值泛型
     * @return 最小元素
     */
    public static <E, V extends Comparable<V>> E minElement(Collection<E> elements, Function<E, V> function) {
        if (elements == null || elements.isEmpty()) throw new IllegalArgumentException("容器中不存在任意元素");
        Iterator<E> iterator = elements.iterator();
        E min = iterator.next();
        for (E e = min; iterator.hasNext(); e = iterator.next()) if (isFormerBigger(min, e, function)) min = e;
        return min;
    }

    /**
     * 获取容器中函数对象返回值最小的元素
     *
     * @param elements 一组元素
     * @param function 函数对象
     * @param <E> 元素泛型
     * @param <V> 函数对象返回值泛型
     * @return 最小元素
     */
    public static <E, V extends Comparable<V>> E minElement(E[] elements, Function<E, V> function) {
        return minElement(Arrays.asList(elements), function);
    }

    /**
     * 获取容器中函数对象返回值最小的元素的值
     *
     * @param elements 一组元素
     * @param function 函数对象
     * @param <E> 元素泛型
     * @param <V> 函数对象返回值泛型
     * @return 最小元素的值
     */
    public static <E, V extends Comparable<V>> V minValue(Collection<E> elements, Function<E, V> function) {
        return function.apply(minElement(elements, function));
    }

    /**
     * 获取容器中函数对象返回值最小的元素的值
     *
     * @param elements 一组元素
     * @param function 函数对象
     * @param <E> 元素泛型
     * @param <V> 函数对象返回值泛型
     * @return 最小元素的值
     */
    public static <E, V extends Comparable<V>> V minValue(E[] elements, Function<E, V> function) {
        return function.apply(minElement(elements, function));
    }

    /**
     * 判断是否前元素更大
     *
     * @param e1 元素1
     * @param e2 元素2
     * @param function 函数对象
     * @param <E> 元素泛型
     * @param <V> 函数对象返回值泛型
     * @return 当前元素更大是返回true，否则返回false
     */
    protected static <E, V extends Comparable<V>> boolean isFormerBigger(E e1, E e2, Function<E, V> function) {
        V v1 = function.apply(e1);
        V v2 = function.apply(e2);
        return v1.compareTo(v2) > 0;
    }

    /**
     * 从容器中安全的获取元素
     * <p>
     * 改方法确保获取元素时不会触发异常，若给定索引出存在元素则返回该元素，否则返回空Optional对象
     *
     * @param elements 容器
     * @param index 索引
     * @param <E> 元素类型
     * @return 可选元素
     */
    public static <E> Optional<E> safeObtainElement(List<E> elements, int index) {
        if (index < 0 || index >= elements.size()) return Optional.empty();
        return Optional.of(elements.get(index));
    }

    /**
     * 从容器中安全的获取元素，若获取失败则返回默认元素
     *
     * @param elements 容器
     * @param index 索引
     * @param defaultElement 默认元素
     * @param <E> 元素类型
     * @return 指定元素或默认元素
     */
    public static <E> E safeObtainElement(List<E> elements, int index, E defaultElement) {
        return safeObtainElement(elements, index).orElse(defaultElement);
    }

    /**
     * 从容器中安全的获取元素
     * <p>
     * 改方法确保获取元素时不会触发异常，若给定索引出存在元素则返回该元素，否则返回空Optional对象
     *
     * @param elements 容器
     * @param index 索引
     * @param <E> 元素类型
     * @return 可选元素
     */
    public static <E> Optional<E> safeObtainElement(E[] elements, int index) {
        if (index < 0 || index >= elements.length) return Optional.empty();
        return Optional.of(elements[index]);
    }

    /**
     * 从容器中安全的获取元素，若获取失败则返回默认元素
     *
     * @param elements 容器
     * @param index 索引
     * @param defaultElement 默认元素
     * @param <E> 元素类型
     * @return 指定元素或默认元素
     */
    public static <E> E safeObtainElement(E[] elements, int index, E defaultElement) {
        return safeObtainElement(elements, index).orElse(defaultElement);
    }

}
